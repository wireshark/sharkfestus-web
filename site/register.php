<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="reg-background-image">
</div>
<div class="row grid-row reg-row">
	<div class="col-lg-7 col-centered index-text">
	    <h3>Don't miss out on this opportunity to learn with the best and network with your peers!<br></h3>
		<p>SharkFest'22 US will be a Hybrid event. While we look forward to seeing many of you in person, we realize that some of you may not be able to travel to the conference, and are happy to offer you the chance to attend virtually via Zoom. As all presentations will be live, you will be able to participate fully in the pre-conference classes and/or all conference sessions, as well as interact with presenters, core developers, sponsors and other attendees via the conference Discord channel.</p>
		<p><i>Please note: Pre-recorded sessions will only be available to conference registrants for 2 months after the conference.</i></p>
		<p><i>Please note: Attendees can change their registration from in-person to virtual and vice versa, by sending us an email to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a></i></p>
	    <div class="">
	        <!-- <a class="front-button" href="https://sharkfestregistration.wireshark.org"><h4>Register</h4></a> -->
			<!-- <button id="front-modal-button" class="front-button">Interest Form</button> -->
	    </div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-10 col-centered reg-table ">
			<table class="table table-striped">
			  <tbody>
			    <tr class="thead-default">
			      <th scope="row">Registration Fees</th>
			      <th>Standard [IN PERSON]</th>
			      <th>Standard [VIRTUAL]</th>
			      <th></th>     
			    </tr>
			    <tr>
			      <th>SharkFest Conference Only (July 12-14)</th>
			      <td class="td-centered">$1295</td>
				  <td class="td-centered">$695</td>
			      <td class="td-centered"></td>			      
			    </tr>
			    <tr>
			      <th>Pre-Conference Class I: Introduction to Packets - How to capture &amp; analyze them using Wireshark (July 9-10)</th>
			      <td class="td-centered">$895</td>
			      <td class="td-centered">$895</td>
			      <td class="td-centered"></td> 
			    </tr>
			    <tr>
			      <th>Pre-Conference Class II: Cybersecurity Threat Hunting - Go Deep with Wireshark (July 11)</th>
			      <td class="td-centered">$795</td>
			      <td class="td-centered">$795</td>
			      <td class="td-centered"></td> 
			    </tr>
				<tr>
			      <th>SharkFest + Pre-Conference Class I</th>
			      <td class="td-centered">$2190</td>
			      <td class="td-centered">$1590</td>
				  <td class="td-centered"></td> 
				</tr>
				<tr>
			      <th>SharkFest + Pre-Conference Class II</th>
			      <td class="td-centered">$2090</td>
			      <td class="td-centered">$1490</td>
				  <td class="td-centered"></td> 
				</tr>
				<tr>
			      <th>All Pre-Conference Classes</th>
			      <td class="td-centered">$1690</td>
			      <td class="td-centered">$1690</td>
				  <td class="td-centered"></td> 
				</tr>
				<tr>
			      <th>SharkFest + All Pre-Conference Classes</th>
			      <td class="td-centered">$2985</td>
			      <td class="td-centered">$2385</td>
				  <td class="td-centered"></td> 
				</tr>
			  </tbody>
			</table>
		</div>
		<div class="col-lg-10 col-centered">
			<div id="accordion" role="tablist" aria-multiselectable="true" class="reg-accordion">
			  <div class="card">
			    <div class="card-header" role="tab" id="headingTwo">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			           SHARKFEST CONFERENCE DETAILS
			        </a>
			      </h5>
			    </div>
			    <div id="collapseTwo" class="collapse in" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>			    
					    <tr>
					      <th scope="row">What's the Schedule?</th>
					      <th>
								July 12-14: Keynotes, Sessions, Developer Den, CTF challenge
						  </th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment for SharkFest is by credit card.</th>
					    </tr>
					    </tr>
					  </tbody>
					</table>					
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">SHARKFEST CONFERENCE CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the SharkFest’22 Virtual US Conference start date</th>
					      <td>Full Refund minus $100 Administration Fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the SharkFest’22 Virtual US Conference start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a>
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingThree">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			           PRE-CONFERENCE CLASS I: Introduction to Packets - How to capture &amp; analyze them using Wireshark
			        </a>
			      </h5>
			    </div>
			    <div id="collapseThree" class="collapse in" role="tabpanel" aria-labelledby="headingThree">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					  	<tr>
					  		<th scope="row">Course Description</th>
					  		<th>You've just downloaded Wireshark, now what?<br /><br />
							
							Whether you are:<br /><br />
							<ul>
								<li>• Net-Ops troubleshooting response time or connection issues</li>
								<li>• Sec-Ops investigating a SIEM alert, performing forensic analysis, or investigating
malware/ransomware infiltration </li>
								<li>• Dev-Ops validating a new application or optimizing old ones</li>
							</ul><br />

							Learning the art of packet analysis will save you time, and your company money. Don't
							be that person who waits until an incident happens to learn how to capture and analyze
							packets. Now is the time, and this is the class.<br /><br />

							We'll use troubleshooting and security case studies to teach core Wireshark techniques.
							This will be a hands-on class. Be ready with the latest version of Wireshark installed so
							you can hone your analyzing skills. There will be a plethora of "Follow Along with Me"
							demos and labs for you to practice the #ArtOfPacketAnalysis.<br /><br />

							There will be plenty of tips and tricks in this 2 day class. It will set the foundation for all the
							#geekygoodness you'll experience during the rest of the Wireshark Conference.<br /><br />

							A share site will be available one week in advance so that all pcaps and supplemental
							materials can be downloaded. Login information will be sent after your registration.
							</th>
						</tr>
						<tr>
							<th scope="row">Course Outline<br>Day 1
							</th>
							<th><strong>Wireshark Fundamentals</strong><br>
								<ul>
									<li>• How and where to capture</li>
									<li>• Customizing Wireshark with GeoIP and profiles </li>
									<li>• Adding color rules to highlight the essential packets</li>
									<li>• Utilizing statistics and graphs to visualize trends </li>
									<li>• Filtering down to just the essential packets</li>
									<li>• Using the command line tools tshark and dumpcap</li>
								</ul>
							</th>
						</tr>
						<tr>
							<th scope="row">
							Day 2</th>
							<th><strong>TCP Flows</strong><br>
								<ul>
									<li>• Taking advantage of filters, filters &amp; more filters</li>
									<li>• Analyzing the Handshake, including the SACK, Window Scaling and Timestamps
option</li>
									<li>• Identifying scanning attempts both simple SYN scans to more concealed attempts</li>
									<li>• Isolating fault for latency issues</li>
									<li>• Examining the various congestion algorithms </li>
									<li>• Troubleshooting retransmissions</li>
									<li>• Visualizing the flows with TCP Stream Graphs</li>
								</ul>
								<strong>TLS Decrypting</strong><br>
								<ul>
									<li>• Locating vulnerabilities in the TLS handshake</li>
									<li>• Setting up environment to decrypt TLS</li>
									<li>• Interpreting TLS errors</li>
								</ul>
							</th>
						</tr>
					  	<tr>
					      <th scope="row">Who Should Attend:</th>
					      <th>This class is for those new to packet analysis and Wireshark. After this course, you will
							become more efficient and confident analyzing pcaps with Wireshark. Network engineers,
							network analysts, application developers, cybersecurity analysts and security engineers
							would benefit the most from this class.</th>
					    </tr>
					    <tr>
					      <th scope="row">Dates & Time</th>
					      <th>July 9-10, 2022, 9am - 5pm</th>
					    </tr>
					    <tr>
					      <th scope="row">Instructor</th>
					      <th>Betty DuBois</th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card.</th>
					    </tr>
					  </tbody>
					</table>
					
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the class start date</th>
					      <td>Full Refund minus $100 administrative fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the class start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a></p>

			      </div>
			    </div>
			    <div class="card">
			    <div class="card">
			    <div class="card-header" role="tab" id="headingFour">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
			           PRE-CONFERENCE CLASS II: Cybersecurity Threat Hunting - Go Deep with Wireshark
			        </a>
			      </h5>
			    </div>
			    <div id="collapseFour" class="collapse in" role="tabpanel" aria-labelledby="headingFour">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
						<tr>
					      <th scope="row">Class Title</th>
					      <th>Cybersecurity Threat Hunting - Go Deep with Wireshark</th>
					    </tr>
					    <tr>
					      <th scope="row">Description</th>
					      <th>The field of Cybersecurity has grown tremendously in the past few years. With every new breach, we realize just how important analysis skills have become in identifying, mitigating, and protecting networks. Wireshark is one of the most important tools in the toolbox for identifying threats, spotting unusual behavior, and analyzing malware behavior, we just need to know how to use it. In this class, we will dive deep into traffic flows to learn how Wireshark can be used to analyze different steps in the Cyber Kill Chain. This is a lab-driven course, with plenty of hands-on, to learn:
							  <ul>
								  <li>• Creating a Security profile</li>
								  <li>• Filters to spot abnormal traffic patterns</li>
								  <li>• Analyzing scan activity</li>
								  <li>• Malware Analysis</li>
								  <li>• Log4j and Emotet</li>
								  <li>• How to spot data exfiltration</li>
								  <li>• Finding Traffic from unusual sources with GeoIP</li>
								  <li>• Spotting a brute-force attack</li>
						  	  </ul>
					      </th>
						</tr>
						<tr>
							<th scope="row">Who Should Attend</th> 
							<th>Wireshark is an important skill for those entering the cybersecurity field, as well as seasoned pros who need to dig into the packets. This course is targeted toward Network Engineers with a working understanding of Wireshark who would like to use it for a cybersecurity focus, but don’t have much experience with threat hunting. Those who want to learn to spot attack patterns, analyze malware, or respond to an incident will enjoy this content!</th>
						</tr>
						<tr>
					      <th scope="row">Instructor</th>
					      <th>Chris Greer</th>
					    </tr>
					    <tr>
					      <th scope="row">Date</th>
					      <th>July 11th, 2022</th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card.</th>
					    </tr>
					  </tbody>
					</table>
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the Pre-Conference Class start date</th>
					      <td>Full Refund minus $100 administrative fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the Pre-Conference Class start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a></p>
			      </div>
			    </div>
			  </div>
			<div id="front-modal" class="front-modal-wrapper">
                <span class="close">&times;</span>
                <div class="front-modal-content">
                    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfmEjsAOTxr0luRdS5BhmII1SI1wzJy_TqbhijsdkFWb4wSvQ/viewform?embedded=true" width="100%" height="600" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                </div>
            </div>
		</div>
	</div>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>