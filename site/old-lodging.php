<?php include($_SERVER['DOCUMENT_ROOT'] . "/header-small.php"); ?>

<div class="container">
	<div class="row">
			<div class="col-sm-6 col-md-6">
			    <div class="thumbnail">
			    <a target="_blank" href="https://www.cmu.edu/housing/residence-types/apartments/margaret-morrison-apartments.html">
			      	<img src="img/lodging/margaret1.jpg" alt="...">
			    </a>
			      <div class="caption">
			        <h4>Margaret Morrison Apartments</h4>
			        <ol>
			        	<li>On-campus, 2-bedroom dorms with private bathroom</li>
			        	<li>Double Occupancy in a Double Room - $49 per person/per night*</li>
			        	<li>Single Occupancy in a Double Room - $91 per person/night*</li>
			        	<li>Kitchen and common living space</li>
			        	<li>Easy walking distance to Cohon Center</li>
			        	<li>No Air Conditioning</li>
			        	<li>WiFi access</li>
			        	<li>*Linens include: bed linens, a bath mat, 2 towels, 2 washcloths, 2 hand towels, and a bar of soap.</li>
			        </ol>
			        <p><a href="https://www.cmu.edu/housing/residence-types/apartments/margaret-morrison-apartments.html" class="btn-lg btn-primary" role="button">Learn More</a></p>
			      </div>
			    </div>
			</div>
			<div class="col-sm-6 col-md-6">
			    <div class="thumbnail">
			    <a target="_blank" href="http://www.wyndhampittsburghuniversitycenter.com/">
			      	<img src="img/lodging/wyndham.jpg" alt="...">
			    </a>
			      <div class="caption">
			        <h4>Wyndham Pittsburgh University Center Hotel</h4>
			        <ol>
			        	<li><strong>SharkFest’17 US Discounted Group Room Block Hotel</strong></li>
			        	<li><strong>Reserve under SharkFest’17 Group Block Code “06166841SF” online or by calling Chelsea (412) 682.6200 x6131 or Sam (412) 682.6259</strong></li>
			        	
			        	<li> SharkFest attendee rate - $149.00 (single, double, triple or quad occupancy) plus 14% occupancy tax per night</li>
			        	<li>SharkFest attendee discounted parking rate - $10/night</li>
			        	<li>Easy walking distance to Cohon Center</li>	
			        	<li></li>		        	
			        </ol>
			        <p><a href="http://www.wyndhampittsburghuniversitycenter.com/" class="btn-lg btn-primary" role="button">Learn More</a></p>
			      </div>
			    </div>
			</div>	
			<div class="col-sm-6 col-md-6 clear-left">
			    <div class="thumbnail">
			    <a target="_blank" href="http://hiltongardeninn3.hilton.com/en/hotels/pennsylvania/hilton-garden-inn-pittsburgh-university-place-PITUCGI/index.html">
			      	<img src="img/lodging/hilton.jpg" alt="...">
			    </a>
			      <div class="caption">
			        <h4>Hilton Garden Inn Pittsburgh University Place</h4>
			        <ol>
			        	<li>1 King Room, 2 Queen Room, 1 King Suite - $184 - $210/night + tax*</li>
			        	<li>Complimentary high-speed internet & secure printing</li>
			        	<li>Valet parking only - $18/day</li>
			        	<li>1 mile from CMU campus (20-minute walk)</li>
			        	<li>Free shuttle to CMU campus (reserved the night before)</li>	
						<li><strong>*on-line rates – quoted rate may vary</strong></li>
		        	
			        </ol>			        <p><a href="http://hiltongardeninn3.hilton.com/en/hotels/pennsylvania/hilton-garden-inn-pittsburgh-university-place-PITUCGI/index.html" class="btn-lg btn-primary" role="button">Learn More</a></p>
			      </div>
			    </div>
			</div>
			<div class="col-sm-6 col-md-6">
			    <div class="thumbnail">
			    <a target="_blank" href="http://www.shadysideinn.com/">
			      <img src="img/lodging/shadyside.jpg" alt="...">
			    </a>
			      <div class="caption">
			        <h4>Shadyside Inn</h4>
			        <ol>
			        	<li>Studio ($125/night + tax), 1-bedroom ($135/night + tax) and 2-bedroom ($145/night + tax) suites with full kitchens</li>
			        	<li>Complimentary Wifi and Parking</li>
			        	<li>.7 miles from CMU campus (10-15 minute walk)</li>
			        	<li>Free hotel shuttle to CMU campus from 7:30am – 10:30pm (weekdays) and 8:00am-10:30pm (weekends).  Reservations made at front desk.  Limit two 1-way or 1 round-trip shuttle ride/day/suite</li>			        
			        	<li><strong>Please contact <a href="mailto:reservations@shadysideinn.com">reservations@shadysideinn.com</a> or call (412) 441-444 and mention that you’re attending SharkFest’17 US in order to get these rates.</strong></li>
			        </ol>
			        <p><a href="http://www.shadysideinn.com/" class="btn-lg btn-primary" role="button">Learn More</a></p>
			      </div>
			    </div>
			</div>	
			
	</div>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>