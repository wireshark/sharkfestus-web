<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

    <div class="container grid-container" id="main-container">
        <div class="background-image">
            <h3>SharkFest'23 US details coming soon.</h3>
            <!-- <button id="front-modal-button" class="front-button">Interest Form</button> -->
        </div>
        <div class="row grid-row">
            <div class="col-xs-12 col-centered index-text">
                
                <!-- <div class="reg-button-front">
                    <a class="front-button" href="/register"><h4>Register Now</h4></a>
                </div> -->
            </div>
        </div>
        <section id="main-content">
            <div class="container col-md-12">
                <div class="col-sm-8 col-xs-12 column">
                    <div class="col-sm-6 col-xs-7 background-blue front-column1">
                        <div class="grid-cell">
                            <h2>About SharkFest</h2>
                            <p>SharkFest is an annual educational conference focused on sharing knowledge, experience and
                                best practices among the Wireshark developer and user communities.</p>
                        </div>
                        <div class="about-button">
                            <a class="" href="about">
                                <h4>Learn More</h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-5 front-agenda-image front-column1 grid-cell">
    
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 column">
                    <div class="col-xs-12 background-blue front-column1">
                        <div class="reg-homepage">
                            <div class="grid-cell">
                                <h2>Registration is closed.</h2>
                                <p>Check back soon for details about SharkFest'23 US!</p>
                            </div>
                            <div class="retro-image">
    
                            </div>
                        </div>
                        <div class="about-button">
                            <a class="" href="register">
                                <h4>Register Now</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container col-md-12" style="margin-bottom: 40px;">
                <div class="col-sm-4 col-xs-12 column">
                    <div class="col-xs-12 background-blue front-column1">
                        <div class="">
                            <div class="grid-cell">
                                <h2>SharkFest Retrospective</h2>
                                <p>Travel back in time through the entire history of SharkFest US conferences.</p>
                                <div class="retro-homepage">
    
                                </div>
                            </div>
                            <div class="retro-image">
    
                            </div>
                        </div>
                        <div class="about-button">
                            <a class="" href="retrospective">
                                <h4>Travel Back Now</h4>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-xs-12 column">
                    <div class="col-sm-6 col-xs-7 background-blue front-column1">
                        <div class="grid-cell">
                            <h2>Become a Sponsor</h2>
                            <p>There are many options for supporting the Wireshark community through a SharkFest
                                sponsorship. To read about this, please click the button below.</p>
                        </div>
                        <div class="about-button">
                            <a class="" href="sponsors">
                                <h4>Learn More</h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-6 front-sponsor-image front-column1 grid-cell">
    
                    </div>
                </div>
            </div>
            <div id="front-modal" class="front-modal-wrapper">
                <span class="close">&times;</span>
                <div class="front-modal-content">
                    <iframe
                        src="https://docs.google.com/forms/d/e/1FAIpQLSfmEjsAOTxr0luRdS5BhmII1SI1wzJy_TqbhijsdkFWb4wSvQ/viewform?embedded=true"
                        width="100%" height="600" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                </div>
            </div>
        </section>
    </div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
