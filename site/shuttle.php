<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container">
	<div class="row lodging-page">

			<div class="col-sm-12 col-md-12">
			    <div class="thumbnail">
			    
			      	<img src="img/corinthian.jpg" alt="...">
			    </div>
			      <div class="caption">
			      	<div>
				        <h4>SharkFest Shuttle Schedule</h4>
				        <p class="lodging-page-p">Shuttle Service begins on Monday afternoon, June 25th and ends Thursday evening, June 28th.<strong> NO SHUTTLE SERVICE FOR TROUBLESHOOTING CLASS OR MALICIOUS TRAFFIC ANALYSIS WORKSHOP</strong></p><br>
				        <table class="table">
				        	<tr>
				        		<th>Date</th>
				        		<th>Time</th>
				        		<th>Location</th>
				        	</tr>
				        	<tr>
				        		<th>Monday, June 25, 2018</th>
				        		<td>4:30pm</td>
				        		<td>Pickup at Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>4:35pm-4:40pm</td>
				        		<td>Pickup at Wild Palms</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>4:40pm-4:45pm</td>
				        		<td>Pickup at Maple Tree Inn; drop off at CHM</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>5:30pm</td>
				        		<td>Pickup at Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>5:35pm-5:40pm</td>
				        		<td>Pickup at Wild Palms</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>5:40pm-5:45pm</td>
				        		<td>Pickup at Maple Tree Inn; drop off at CHM</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>9:00pm</td>
				        		<td>Pickup at CHM and drop off at Maple Tree Inn, Wild Palms and Domain Hotel</td>
				        	</tr>
				        </table>
				        <table class="table">
				        	<tr>
				        		<th>Date</th>
				        		<th>Time</th>
				        		<th>Location</th>
				        	</tr>
				        	<tr>
				        		<th>Tuesday, June 26, 2018</th>
				        		<td>7:15am</td>
				        		<td>Pickup at Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>7:20am-7:25am</td>
				        		<td>Pickup at Wild Palms</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>7:25am-7:30am</td>
				        		<td>Pickup at Maple Tree Inn; drop off at CHM</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:15am</td>
				        		<td>Pickup at Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:20am-8:25am</td>
				        		<td>Pickup at Wild Palms</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:25am-8:30am</td>
				        		<td>Pickup at Maple Tree Inn; drop off at CHM</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:00pm</td>
				        		<td>Pickup at CHM and drop off at Maple Tree Inn, Wild Palms and Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>9:00pm</td>
				        		<td>Pickup at CHM and drop off at Maple Tree Inn, Wild Palms and Domain Hotel</td>
				        	</tr>
				        </table>
				        <table class="table">
				        	<tr>
				        		<th>Date</th>
				        		<th>Time</th>
				        		<th>Location</th>
				        	</tr>
				        	<tr>
				        		<th><span style="letter-spacing: -.5px;">Wednesday, June 27, 2018</span></th>
				        		<td>7:15am</td>
				        		<td>Pickup at Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>7:20am-7:25am</td>
				        		<td>Pickup at Wild Palms</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>7:25am-7:30am</td>
				        		<td>Pickup at Maple Tree Inn; drop off at CHM</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:15am</td>
				        		<td>Pickup at Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:20am-8:25am</td>
				        		<td>Pickup at Wild Palms</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:25am-8:30am</td>
				        		<td>Pickup at Maple Tree Inn; drop off at CHM</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:00pm</td>
				        		<td>Pickup at CHM and drop off at Maple Tree Inn, Wild Palms and Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>9:00pm</td>
				        		<td>Pickup at CHM and drop off at Maple Tree Inn, Wild Palms and Domain Hotel</td>
				        	</tr>
				        </table>
				        <table class="table">
				        	<tr>
				        		<th>Date</th>
				        		<th>Time</th>
				        		<th>Location</th>
				        	</tr>
				        	<tr>
				        		<th>Thursday, June 28, 2018</th>
				        		<td>7:15am</td>
				        		<td>Pickup at Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>7:20am-7:25am</td>
				        		<td>Pickup at Wild Palms</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>7:25am-7:30am</td>
				        		<td>Pickup at Maple Tree Inn; drop off at CHM</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:15am</td>
				        		<td>Pickup at Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:20am-8:25am</td>
				        		<td>Pickup at Wild Palms</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:25am-8:30am</td>
				        		<td>Pickup at Maple Tree Inn; drop off at CHM</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>6:00pm</td>
				        		<td>Pickup at CHM and drop off at Maple Tree Inn, Wild Palms and Domain Hotel</td>
				        	</tr>
				        	<tr>
				        		<th></th>
				        		<td>8:00pm</td>
				        		<td>Pickup at CHM and drop off at Maple Tree Inn, Wild Palms and Domain Hotel</td>
				        	</tr>
				        </table>
				       	<!--<ol>
				        	<li>Monday, 6/25   4:30-6:00pm (3 Pickup runs from Domain and Maple Tree, DO at CHM)</li>
				        	<li>Monday, 6/25   8:00-9:00pm (2 PU runs from CHM with DO at Maple Tree and Domain)</li>
				        </ol>
				        <ol>
				        	<li>Tuesday, 6/26   7:30-8:15am (2 PU runs from Domain and Maple Tree, DO at CHM)</li>
				        	<li>Tuesday, 6/26   8:00-9:00pm (2 PU runs from CHM with DO at Maple Tree and Domain)</li>
				        </ol>
				        <ol>
				        	<li>Wednesday, 6/27   7:30-8:15am (2 PU runs from Domain and Maple Tree, DO at CHM)</li>
				        	<li>Wednesday, 6/27   8:00-9:00pm (2 PU runs from CHM with DO at Maple Tree and Domain)</li>
				        </ol>
				        <ol>
				        	<li>Thursday, 6/28  7:30-8:15am (2 PU runs from Domain and Maple Tree, DO at CHM)</li>
				        	<li>Thursday, 6/28   6:00-8:00pm (3 PU runs from CHM with DO at Maple Tree and Domain)</li>
				        </ol>-->
			      </div>
			</div>
			
	</div>
</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>