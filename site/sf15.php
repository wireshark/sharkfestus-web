<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf15-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'15 Retrospective</h1>
            <p>June 22nd - 25th, 2015<br>Computer History Museum | Mountain View, California</p>
        </div>
    </div>
    <section id="main-content">
    <!-- Content area -->
    <div class="content-area retrospective-page">

        <!-- PAGE BLOG -->
        <section class="page-section with-sidebar sidebar-right">
        <div class="container">
        <div class="row post-excerpt">

        <!-- Content -->
        <section id="content" class="content col-sm-7 col-md-8">

            <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                <div class="post-media">
                </div>
                <div class="post-header">
                    
                    <div class="post-meta">                       
                    </div>
                </div>
                <div class="post-body">
                    <div class="post-excerpt">

                        <!--
                        <h2 class="post-title2"><strong>Blogs</strong></h2>
                        <p><a href="http://blog.packet-foo.com/2014/06/sharkfest-2014-recap/">SharkFest 2015 Recap</a> by Jasper Bongertz</p> -->

                        <h3 class="post-title2"><strong><a href="assets/presentations15/packetchallenge.pdf">Packet Challenge</a></strong></h3>
                        <p>Click <a href="assets/presentations15/packetchallenge.zip">HERE</a> to download the Packet Challenge questions/answers and the pcap files!  
                        </p>

                        <h3 class="post-title2"><strong>Keynote Presentations</strong></h3>
                        <div class="retro-vid-wrapper">
                            <div class="responsive-iframe">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/YJCsgh6OIaY" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <h4><strong>High Impact: How Computer Geeks, Geniuses and Renegades are Changing the World</strong><br/>
                            John C Hollar</h4>
                        </div>
                        

                        <h3 class="post-title2"><strong>Blogs</strong></h3>
                        <div class="retro-vid-wrapper">
                            <h4><a href="https://blog.packet-foo.com/2015/07/sharkfest-2015-recap/">SharkFest'15 Recap</a> by Jasper Bongertz <br>
                            <a href="http://www.unadulteratednerdery.com/2015/06/26/sharkfest-2015/">SharkFest'15 Recap</a> by John Schreiner
                            </h4>
                        </div>

                        <h3 class="post-title2"><strong>SharkBytes</strong></h3>
                        <div class="retro-vid-wrapper">
                            <div class="responsive-iframe">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/Q5gDOozxnY0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        
                        
                        <h3 class="post-title2"><strong>Tuesday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li>01: <a href="assets/presentations15/01.pptx">Troubleshooting Fundamentals: Time-Tested Technique to Finding Root Cause - How I Approach Every Packet-Level Troubleshooting Exercise <!--[<a href="presentations/B1.pdf">PDF</a> ||<a href="assets/presentations/B1-Wireshark for Beginners-Hansang_Bae.pptx"> PPT</a>] --></a> by Hansang Bae</li>
                        
                        <li>02: <a href="assets/presentations15/02.pptx">Introduction to Wireshark: Making Sense of the Matrix</a> by Chris Greer</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/635snt797bQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:00)</li>
                        </ul>
                        <li>03: <a href="assets/presentations15/03.pptx">Writing a Wireshark Dissector Using WSGD, Lua and C</a> by Graham Bloice</li>
                        <ul>
                        <li><a href="assets/presentations15/grahambloice.zip">Presentation Materials</a></li>
                        </ul>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/bwqv_OzCZC8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:39)</li>
                        </ul>
                        <li>04: <a href="assets/presentations15/04.pdf"> Tracewrangling: Preparing Food for the Shark</a> by Jasper Bongertz</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/_-iMd-UAhtY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:11:00)</li>
                        </ul>
          
                        <li>05: <a href="assets/presentations15/05.pdf">Visualizations & Correlations in Troubleshooting</a> by Kevin Burns</li>
                        <li>06: <a href="assets/presentations15/06.pdf">Integrating L2/L3 Diagnostics: Deducing Network Configuration</a> by Nalini Elkins</li>
                        <li>07: <a href="assets/presentations15/07.pptx"> TCP Analysis: Tips & Case Studies</a> by Chris Greer</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/MIPS0RPn-aw"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:05:00)</li>
                        </ul>
                        <li>08: <a href="assets/presentations15/08.pdf"> IPv6 in Wireshark</a> by Jeff Carrell</li>
                        
                        <li>09: <a href="assets/presentations15/09.pptx"> The Wireless Side of Wireshark </a>by Thomas d'Otreppe de Bouvette</li>
                        <li>10: <a href="assets/presentations15/10.pdf"> Analyzing Huge Data for Suspicious Traffic </a>by Christian Landström</li>
                        <li>11: <a href="assets/presentations15/11.pptx"> Changing Wireshark with Lua: Writing a Lua Plug-in to Create a Custom Decoder </a>by Hadriel Kaplan</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/HTtVHxIh6ww" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:19:03)</li>
                        </ul>

                        <li>12: <a href="assets/presentations15/12.pdf"> Matching Packets in Multiple Network Traces </a>by Paul Offord</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/xlJXr3dpnWs" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:41)</li>
                        </ul>
                        </ul>

                        <h3 class="post-title2"><strong>Wednesday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li> <a href="assets/presentations15/13.pdf"> 13: Advanced TCP Stuff: We're not in RFC 793 Anymore!</a> by Jasper Bongertz</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/L5cbn2iF91s" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:06:13)</li>
                        </ul>
                        <li>14: Simple Wireless Packet Capture on Android by Mike Kershaw</li>
                        <li>15: <a href="assets/presentations15/15.pdf">How To Get the Packets: Packet Capture Techniques for the Enterprise by Paul Offord</a></li> 
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/pb1yb1eUlgY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:00)</li>
                        </ul>
                        <li>16: Network Troubleshooting Using ntop [<a href="assets/presentations15/16.pdf">PDF</a> ||<a href="assets/presentations15/16.pptx"> PPT</a>] by Luca Deri</li> 
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/RWg1mNYRkmc" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:22)</li>
                        </ul>
                        <li>17:  Troubleshooting Routing Protocols with Wireshark by John Schreiner</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/Mx7K1wPOm2Q" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:31)</li>
                        </ul>
                       
                        <li>18:<a href="assets/presentations15/18.pptx"> De-duping Packet Capture Files at Layer 3: Using Wiretap to Create a Custom De-duping App by Rober Bullen</a></li>
                        <li>19: <a href="assets/presentations15/19.pdf" title="Presentation slides" target="_blank">SSL Does Not Mean SOL</a> by Scott Haugdahl</li>

                        <li>20: <a href="assets/presentations15/20.pdf" title="Presentation slides" target="_blank">Understanding Wireshark's Reassembly Features </a> by Christian Landström</li>
                        <li>21: <a href="assets/presentations15/07.pptx">TCP Analysis: Tips & Case Studies</a> by Chris Greer</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/MIPS0RPn-aw"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:05:00)</li>
                        </ul>
                        <li>22: <a href="assets/presentations15/22.pdf" title="Presentation slides" target="_blank">Network Forensics 101: What You Don't See Will Hurt You!</a> by Phill Shade</li>
                        <li>23: <a href="assets/presentations15/23.pdf">Discover WLANs with Wireshark, AirPcap & WiSpy: Troubleshooting WLANs using 802.11 Management & Control Frames</a> by Rolf Leutert</li>
                        <li>24: Capturing Mobile Device Traffic with Wireshark by Tom Updegrove</li>
                        </ul>
                  
                        <h3 class="post-title2"><strong>Thursday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li>25: <a href="assets/presentations15/25.pdf">Troubleshooting the TCP Handshake</a> by Betty DuBois</li> 
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/HGcbhCVZ8MU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:27)</li>
                        </ul>
                        <li>26: <a href="assets/presentations15/26.pdf">Root Cause in Complex Networks: Tips & Lessons from Fast-Paced & Enterprise Financials by</a> Chris Bidwell</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/CaIAb462a3w" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:36)</li>
                        </ul>
                        <li>27: Network Troubleshooting Using ntop [<a href="assets/presentations15/16.pdf">PDF</a> ||<a href="assets/presentations15/16.pptx"> PPT</a>] by Luca Deri</li>
                        <li>28: <a href="assets/presentations15/28.pdf">T-Shark for the Win!</a> by Christian Landström</li>
                        <li>29: <a href="assets/presentations15/29.pdf">Wireless Troubleshooting Tips using AirPcaps: DFS & Module Debugging</a> by Megumi Takeshita</li>
                        <li>30: <a href="assets/presentations15/30.pdf" title="Presentation slides" target="_blank">How Did That Happen? Practical Techniques for Analyzing Suspicious Traffic</a> by Phill Shade</li>
                        <li>31: <a href="assets/presentations15/31.pdf" title="Presentation slides" target="_blank">Zen & The Art of Packet Capturing: Packet Capture Techniques in Different Scenarios</a> by Sake Blok</li>
                        <li>32: <a href="assets/presentations15/32.pdf">Why is FTP So Slow? Which End is Holding it Up? </a>by Nalini Elkins</li>
                        <li>33: <a href="assets/presentations15/33.pdf">Using the TRANSUM Wireshark Plugin to Measure Application Performance</a> by Paul Offord</li>
                        <ul>
                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=7PO-9HxHGdo&t=7m48s" title="Presentation video on YouTube" target="_blank">Presentation Video Part 1</a> (45:25)</li>
                        </ul>
                        <li>34: <a href="assets/presentations15/34.pdf">Wireshark vs. the Cloud: Capturing Packets in Virtual Environments</a> by Jasper Bongertz</li>
                        <li>35: <a href="assets/presentations15/35.pdf">The Anatomy of a Vulnerability</a> by Ron Bowes</li>
                        <li>36: <a href="assets/presentations15/36.pptx">Wireshark and the Art of Packet Analysis</a> by Hansang Bae</li>
                        </ul>  
                        <!--
                        <h2 class="post-title2">Shark Bytes</h2>
                        <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/c119qZnKFsw" frameborder="0" allowfullscreen></iframe>
                        <p>Shark Bytes consist of "little crunchy bits of wisdom". Like the immensely popular TED talks, Shark Bytes aim to inform, inspire, surprise, and delight as presenters share their personal perspective on a topic in under 5 minutes.</p>
                        -->
                    </div>
                </div>
                
            </article>

            <!-- About the author -->
            
            <!-- /About the author -->

            <!-- Comments -->
            
            <!-- /Comments -->

            <!-- Leave a Comment -->
            
            <!-- /Leave a Comment -->

        </section>
        <!-- Content -->

        <hr class="page-divider transparent visible-xs"/>

        <!-- Sidebar -->
        <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <h4 class="widget-title">A Word of Thanks</h4>
                        <p>SharkFest’15, in our new home at the Computer History Museum in Mountain View, proved to be a rousing success, with over 300 IT Professionals from 25 countries in attendance.  A good time was had by all, thanks to the dedication of our very capable SharkFest coordinating crew, sponsors, volunteers, instructors, and Wireshark core developers.  Thanks for giving us a reason to come together again, Gerald!</p>
                    </div>
                </div>
            <div class="widget categories">
            </div>

            <div class="widget flickr-feed">
                          
        <div class="post-media">
        <a style="font-size: 25px;" data-lightbox="sf15" alt="Click Here to View Pictures from Sharkfest'15!" href='img/sharkfest15gallery/1.jpg'><img src="img/sf15gallery.jpg" ></a></div>
            <a href='img/sharkfest15gallery/2.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/3.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/4.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/5.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/6.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/7.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/8.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/22.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/9.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/10.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/11.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/12.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/12.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/13.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/14.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/15.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/16.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/17.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/18.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/19.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/20.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/21.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/23.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/24.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/25.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/26.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/27.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/28.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/29.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/30.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/31.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/32.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/33.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/34.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/35.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/36.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/37.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/38.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/39.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/40.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/41.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/42.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/43.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/44.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/45.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/46.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/47.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/48.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/49.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/50.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/51.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/52.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/53.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/54.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/55.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/56.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/57.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/58.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/59.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/60.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/61.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/62.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/63.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/64.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/65.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/66.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/67.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/68.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/69.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/70.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/71.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/72.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/73.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/74.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/75.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/76.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/77.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/78.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/79.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/80.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/81.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/82.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/83.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/84.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/85.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/86.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/87.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/88.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/89.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/90.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/91.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/92.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/93.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/94.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/95.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/96.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/97.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/98.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/99.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/100.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/101.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/102.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/103.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/104.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/105.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/106.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/107.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/108.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/109.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/110.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/111.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/112.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/114.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/115.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/116.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/117.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/118.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/119.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/120.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/121.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/122.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/123.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/124.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/125.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/126.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/127.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/128.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/129.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/130.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/131.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/132.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/133.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/134.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/135.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/136.jpg' data-lightbox="sf15"></a>
            <a href='img/sharkfest15gallery/137.jpg' data-lightbox="sf15"></a>

            </div>
             <script>
            jQuery('a.gallery').colorbox();
        </script>
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <div class="sponsor-list">
                            <div class="sponsor-imgs">
                                        <h4><strong>Reef Sponsors</strong></h4>
                                        <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>
                                        <a href="http://www.cpacket.com/index.php" target="_blank"><img class="sponsors" src="img/sponsors/cpacket-white.png"></a>
                                        <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                                        <a href="http://www.wiresharktraining.com/" target="_blank"><img class="sponsors" src="img/sponsors/wiresharku-white.png"></a>


                                        <h4><strong>Angel Shark Sponsors</strong></h4>
                                        <a href="http://accoladetechnology.com/" target="_blank"><img class="sponsors" src="img/sponsors15/accolade.png"></a>
                                        <a href="http://www.bigswitch.com/" target="_blank"><img class="sponsors" src="img/sponsors/bigswitch-white.png"></a>
                                        <a href="http://datacomsystems.com/" target="_blank"><img class="sponsors" src="img/sponsors/datacom-white.png"></a>
                                        <a href="http://www.dual-comm.com/" target="_blank"><img class="sponsors" src="img/sponsors/dualcomm-white.png"></a>
                                        <a href="http://www.garlandtechnology.com/" target="_blank"><img class="sponsors" src="img/sponsors/garland-white.png"></a>
                                        <a href="http://www.gigamon.com/" target="_blank"><img class="sponsors" src="img/sponsors/gigamon_logo.png"></a>
                                        <a href="http://www.insidethestack.com/" target="_blank"><img class="sponsors" src="img/sponsors/inside-white.png"></a>
                                        <a href="http://www.ixiacom.com/" target="_blank"><img class="sponsors" src="img/sponsors/ixia-white.png"></a>
                                        <a href="http://www.napatech.com/" target="_blank"><img class="sponsors" src="img/sponsors/napatech-white.png"></a>
                                        <a href="http://www.pluribusnetworks.com/" target="_blank"><img class="sponsors" src="img/sponsors15/pluribus.png"></a>
                                        <a href="http://www.teledynelecroy.com" target="_blank"><img class="sponsors" src="img/sponsors/teledyne-white.png"></a>
                            </div>
                        </div>
                    </div>
                </div>                         
    </div> 
        </div>
        </section>
        </aside>
        <!-- /PAGE BLOG -->

    </div>
    <!-- /Content area -->
</div>
</section>
</div>
</section>
</div>

    <!-- FOOTER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
    <!-- /FOOTER -->

