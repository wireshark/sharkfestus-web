<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<head>
    <style>
        .pwd-protected-page {
            height: 75vh;
            padding: 5rem;
        }
        #main-content {max-width: 1400px; margin: auto;}
        #main-content  .container {display: flex; background-color: white;}
        .about-container {padding: 10px;}
        .about-container h2, h3 {background-color: #2b6fef; color: white; padding: 5px; font-weight: 100;}

        .front-button {text-transform: uppercase;
            padding: 14px 30px 12px;
                padding-right: 30px;
                padding-left: 30px;
            border: 2px solid #7ca7f5;
            border-radius: 0;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            margin: 10px auto 0 auto;
            display: inline-block;
            text-align: center;
            letter-spacing: 1px;
            line-height: 1.0em;
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -ms-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
            color: white;
            background: #2b6fef;
            text-decoration: none;
        }
        .front-button:hover {text-decoration: none; color:#a4d4ff;}
        .front-button:focus {text-decoration: none; color:#a4d4ff;}
        .front-button h4 {margin: 0;}
    </style>
</head>

<div class="text-centered pwd-protected-page">
    <section id="main-content">
        <div class="about-container">
            <a class="front-button" href="assets/SharkFest22Agenda-zoom.docx"><h4>Conference Agenda PDF with Zoom Links</h4></a>
        </div>
    </section>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>