<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <section id="main-content">
        <div class="col-md-10 col-sm-12 col-centered">
            <div class="retro-container">
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'21 Retrospective</h2>
                                    <p>SharkFest’21 Virtual US, held online, was populated by hundreds Wireshark enthusiasts from all over the world and was a great success in educating and inspiring the assembled Wireshark community thanks to the core developers, instructors, sponsors, volunteers, staff, and generous knowledge-sharing by attendees.</p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf21"><h4>Read More</h4></a>
                                 </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'19 Retrospective</h2>
                                    <p>SharkFest’19 US, at UC Berkeley, was populated by 300 Wireshark enthusiasts from over 20 countries and was a great success in educating and inspiring the assembled Wireshark community thanks to the core developers, instructors, sponsors, volunteers, staff, and generous knowledge-sharing by attendees.</p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf19"><h4>Read More</h4></a>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'18 Retrospective</h2>
                                    <p>SharkFest’18 US, back at the Computer History Museum, was populated by 300 Wireshark enthusiasts from over 19 countries and succeeded in its overall mission to educate and inspire the assembled Wireshark community thanks to the core developers, instructors, sponsors, volunteers, staff, and generous knowledge-sharing by attendees. </p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf18"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="">
                                    <div class="grid-cell">
                                        <h2>SharkFest'17 Retrospective</h2>
                                        <p>SharkFest’17 was the first on the east coast of the United States and was a huge success! Hundreds of IT Professionals from many countries found their way to Carnegie Mellon University to share their Wireshark wisdom and experience, and to enjoy one another's company over the course of the conference days. To view keynotes, session presentations and more, please read on.</p>
                                    </div>
                                    <div class="retro-image">
                                        
                                    </div>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf17"><h4>Read More</h4></a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'16 Retrospective</h2>
                                    <p>SharkFest’16 has been said to be the best SharkFest ever! Many IT Pros from 18 countries came to the Computer History Museum to share Wireshark wisdom, and to enjoy one another's company during the conference. </p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf16"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="">
                                    <div class="grid-cell">
                                        <h2>SharkFest'15 Retrospective</h2>
                                        <p>SharkFest’15, at the Computer History Museum, had 300+ IT Professionals from 25 countries attending. It succeeded thanks to the SharkFest coordinating crew, sponsors, instructors, and Wireshark core developers.</p>
                                    </div>
                                    <div class="retro-image">
                                        
                                    </div>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf15"><h4>Read More</h4></a>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'14 Retrospective</h2>
                                    <p>SharkFest '14 was held at the Dominican University in San Rafael, CA, and had keynote speakers Vinton Cerf, one of the "fathers of the Internet", and Tim O'Neill, Chief Contributing and Technology Editor for LoveMyTool.</p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf14"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="">
                                    <div class="grid-cell">
                                        <h2>SharkFest'13 Retrospective</h2>
                                        <p>SharkFest'13 was at UC Berkeley in California and featured keynotes by Gerald Combs, Wireshark founder; Rich Seifert, President of Networks and Comm.; and Charles Kaplan, Senior Technical Director at Riverbed.</p>
                                    </div>
                                    <div class="retro-image">
                                        
                                    </div>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf13"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'12 Retrospective</h2>
                                    <p>SharkFest'12 was held at UC Berkeley in California and had keynote addresses by astronomer/author Clifford Stoll and Steve Riley, Technical Leader for the CTO at Riverbed. </p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf12"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="">
                                    <div class="grid-cell">
                                        <h2>SharkFest'11 Retrospective</h2>
                                        <p>SharkFest '11 was held at Stanford University in Palo Alto, California and featured a keynote address by Steve McCanne, co-founder and CTO of Riverbed Technology.</p>
                                    </div>
                                    <div class="retro-image">
                                        
                                    </div>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf11"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'10 Retrospective</h2>
                                    <p>SharkFest '10 was held at Stanford University in Palo Alto, California and featured a keynote address by Van Jacobson, Creator of tcpdump and a research fellow at PARC. </p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf10"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="">
                                    <div class="grid-cell">
                                        <h2>SharkFest'09 Retrospective</h2>
                                        <p>SharkFest’09 was held at Stanford University in Palo Alto, California and featured keynotes by Harry Saal, Founder, CEO and President of Network General Corporation, and Dr. Lawrence Roberts, Creator of the ARPANET and CEO and Founder of Anagram.</p>
                                    </div>
                                    <div class="retro-image">
                                        
                                    </div>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf09"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column retro-margin-bottom">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'08 Retrospective</h2>
                                    <p>SharkFest '08 was held at Foothill College in Los Altos Hills, California and featured a keynote address by Vint Cerf, an American computer scientist, who is recognized as one of the "fathers of the Internet". </p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf08"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
