<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf11-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'11 Retrospective</h1>
            <p>June 13th - 16th, 2011<br>Stanford University | Stanford, California</p>
        </div>
    </div>
    <section id="main-content">


        <!-- Content area -->
        <div class="content-area retrospective-page">

            <!-- PAGE BLOG -->
            <section class="page-section with-sidebar sidebar-right">
            <div class="container">
            <div class="row">

            <!-- Content -->
            <section id="content" class="content col-sm-7 col-md-8">

                <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="post-media">
                    </div>
                    <div class="post-header">
                        <div class="post-meta">     
                        </div>
                    </div>
                    <div class="post-body">
                        <div class="post-excerpt">
                            <h3 class="post-title2"><strong>Blogs</strong></h3>
                            <p><a href="https://www.facebook.com/media/set/?set=a.10150222206636871.321177.51502216870">Event Photos</a></p>

                            <h3 class="post-title2"><strong>Presentation Videos</strong></h3>
                            <p><a href="https://www.lovemytool.com/blog/sharkfest/">Love My Tool</a><br>
                           </p>

                            <h3 class="post-title2"><strong>Keynote Video and Presentation</strong></h3>
                            <div class="responsive-iframe">
                                <iframe width="640" height="480" src="https://www.youtube.com/embed/XHlqIqPvKw8" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <p>SHARKFEST '11 Keynote Address by Steve McCanne with an introduction by Gerald Combs and Loris Degioanni.</p>
                            <p>Keynote Presentation:<a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/McCanne-Sharkfest'11_Keynote_Address.pdf" title="Download presentation PDF" target="_blank">The Architecture and Optimization Methodology of the libpcap Packet Capture Library </a>by Steve McCanne, co-creator tcpdump, co-founder and CTO, Riverbed.</p>
                            
                            <h3 style="" class="post-title2"><strong>Beginner Track Presentations</strong></h3>
                            <ul style="list-style:none;">
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-1_DuBois-%20I_Just_Downloaded_Wireshark-Now_What.pdf" target="_blank">B-1 I've Downloaded Wireshark...Now What? </a> by Betty DuBois</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-2_Bardwell-Mobile_App_Analysis.pdf" target="_blank">B-2 and B-9 Mobile Application Analysis with Wireshark</a> by Joe Bardwell</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-3_Leutert-Discovering_IPv6_with_Wireshark.pdf" target="_blank">B-3 Discovering IPv6 with Wireshark</a> by Rolf Leutert</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-4_Woodings-Visualizing_RF.pdf" target="_blank">B-4 Visualizing RF</a> by Ryan Woodings</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-6_DuBois-Network_Mysteries-Case_of_the_Slow_Network.pdf" target="_blank">B-6 Network Mysteries and How To Solve Them Part 1</a> by Betty DuBois</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-7_Leutert-Discovering_WLAN_802.11n_MIMO.pdf" target="_blank">B-7 Discovering WLAN 802.11n MIMO</a> by Rolf Leutert</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-8_Chappell-Wireshark_Certification.pdf" target="_blank">B-8 Wireshark Certification – What it is and How to Get it</a> by Laura Chappell</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-10_Poth-Using_Wireshark_to_Support_the_Application.pdf" target="_blank">B-10 Using Wireshark to Support the Application</a> by Tim Poth + <a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-10_Trace_Files_PCAP.7z"> Trace File</a></li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/B-11_DuBois-Network_Mysteries-Case_of_the_Missing_Download.pdf" target="_blank">B-11 Network Mysteries and How To Solve Them Part 2</a> by Betty DuBois</li>
        </ul>
              
          
                  <h3 class="post-title2"><strong>Intermediate Track Presentations</strong></h3>
                  
            <ul style="list-style:none;"><li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/I-1_Bae-Packet_Trace_Whispering.pdf" target="_blank">I-1 Packet Trace Whispering</a> by Hansang Bae + <a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/I-1_Bae-Packet_Trace_Whispering_TraceFile.zip">Trace Files</a></li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/I-2_Sanders-Increasing_Throughput_of_Network_Appliances_Through_Virtualization.pdf" target="_blank">I-2 Increasing the Throughput of Network Appliances through Virtualization</a> by Pete Sanders</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/I-3_Chappell-Customizing_Wireshark_for_Different_Use_Scenarios.pdf" target="_blank">I-3 and I-4 Hands-On Lab: Customizing Wireshark for Different Use Scenarios</a> by Laura Chappell</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/I-6_Leong-Taking_Wireshark_to_the_Future_Network.pdf" target="_blank">I-6  Taking Wireshark to the Future Network</a> by Patrick Leong</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/I-7_White-Using_NetFlow_to_Analyze_Your_Network.pdf" target="_blank">I-7 Using NetFlow to Analyze Your Network</a> by Christopher J. White</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/i-8_Canney-Troubleshooting_Application_Performance_Issues.pdf" target="_blank">I-8 Troubleshooting Application Performance Issues</a> by Mike Canney</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/I-9_Chappell-Troubleshooting_Tips_and_Tricks_for_TCP_IP_Networks.pdf" target="_blank">I-9  and I-10 Hands-On Lab: Troubleshooting Tips &amp; Tricks for TCP/IP Networks</a> by Laura Chappell</li>
        </ul>
                  <h3 class="post-title2"><strong>Mixed Bag Track Presentations</strong></h3>
                  
            <ul style="list-style:none;"><li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/A-1_Fisher_Combs-Writing_Wireshark_Dissectors_Plug-Ins.pdf" target="_blank">A-1 Writing Wireshark Dissectors and Plug-Ins</a> by Stephen Fisher and Gerald Combs</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/A-2_Blok-Using_Wireshark_Command_Line_Tools_&_Scripting.pdf" target="_blank">A-2 and A-5 Using Wireshark Command Line Tools &amp; Scripting</a> by Sake Blok</li>
          <li><a href="http://prezi.com/mw0_9qfb2d6d/packetfu-by-example/" target="_blank">A-3  PacketFu by Example</a> and <a href="http://prezi.com/290abtkiola3/metasploit-open-source-penetration-testing/" target="_blank">Metasploit: Open Source Penetration Testing</a> by Tod Beardsley</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/A-4_Bongertz-Wireshark_vs_the_Cloud.pdf" target="_blank">A-4 Wireshark vs. the Cloud </a>by Jasper Bongertz</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/A-6_Lyon-Open_Source_Security_Tools.pdf" target="_blank">A-6 Open Source Security Tools </a>by Gordon Lyon, Tod Beardsley and Thomas D'Otreppe</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/A-7_Bongertz-Have_Wireshark_Will_Travel.pdf" target="_blank">A-7 Have Wireshark – Will Travel</a> by Jasper Bongertz</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/A-8_Bae-Wireshark_in_the_Large_Enterprise.pdf" target="_blank">A-8 Wireshark in the Large Enterprise</a> by Hansang Bae + <a href="presentations/A-8_Bae-Wireshark_in_the_Large_Enterprise_TraceFile.zip">Trace Files</a></li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/A-10_Giordano_and_Degioanni-VoIP_Troubleshooting_and_Analysis.pdf" target="_blank">A-10  VoIP Troubleshooting and Analysis</a> by Pietro Giordano and Loris Degioanni</li>
          <li><a href="http://sharkfest.wireshark.org/sharkfest.11/presentations/A-11_Bongertz-Trace_File_Anonymization.pdf" target="_blank">A-11 Trace File Anonymization</a> by Jasper Bongertz</li>
        </ul>
                            

                        </div>
                    </div>
                    
                </article>

                <!-- About the author -->
                
                <!-- /About the author -->

                <!-- Comments -->
                
                <!-- /Comments -->

                <!-- Leave a Comment -->
                
                <!-- /Leave a Comment -->

            </section>
            <!-- Content -->

            <hr class="page-divider transparent visible-xs"/>

            <!-- Sidebar -->
    <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
        <div class="background-blue front-column1">
          <div class="grid-cell">
            <h4 class="widget-title">A Word of Thanks</h4>
            <p>Another Sharkfest has come and gone, and we thank each participant, sponsor, presenter, keynote speaker, coordinator, caterer, staff and general support person for making this a successful, richly educational event once again. All session presentations are available from this page, and recordings of many sessions can be found at <a href="www.lovemytool.com">www.lovemytool.com</a>.</p>
          </div>
        </div>
                
    <!--

                    <p><strong>Platinum Level</strong></p>
                    <a href="http://www.cpacket.com/index.php" target="_blank"><img class="sponsors" style="width: 127px; margin-bottom:20px; margin-top: -5px" src="assets/img/sponsors/cpacket_logo.png"></a>
                    <a href="http://www.metageek.net/" target="_blank"><img width="127" style="margin-bottom: 25px" src="assets/img/sponsors/metageek_logo.png">

                    <p style="margin-bottom: 0px"><strong>Silver Level</strong></p>
                    
                    <a href="http://www.gigamon.com/" target="_blank"><img style="width:123px; margin-top: 20px; margin-bottom: 20px" class="sponsors" src="assets/img/sponsors/gigamon_logo.png"></a>
                    <a href="http://appliance.cloudshark.org/" target="_blank"><img class="sponsors" width="129" src="assets/img/sponsors/cloudshark_logo.png"></a>
                    <a href="http://www.dual-comm.com/" target="_blank"><img style="width:130px" class="sponsors" src="assets/img/sponsors/dualcomm_logo.png"></a>
                   
                    <a href="http://www.lovemytool.com/" target="_blank"><img style="width:125px;" class="sponsors" src="assets/img/sponsors/lovemtytool_Logo.png"></a>
                    <a href="http://www.napatech.com/" target="_blank"><img style="width:130px" class="sponsors" src="assets/img/sponsors/napatech_logo.png"></a>
                     <a href="http://www.insidethestack.com/" target="_blank"><img style="width:120px; height:45px; margin-left:5px; margin-top: 10px" class="sponsors" src="assets/img/sponsors/inside_products_logo.png"></a>
                    <a href="http://www.wiresharktraining.com/" target="_blank"><img style="width:130px;" class="sponsors" src="assets/img/sponsors/wireshark_university_logo.png"></a>
                   
                    <a href="http://www.bigswitch.com/" target="_blank"><img class="sponsors" style="margin-bottom:15px" width="130" src="assets/img/sponsors/big_switch_logo.png"></a>
                   
                    

                    <p style="margin-bottom: 5px"><strong>Hosting Sponsor</strong></p>
                    <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" style="margin-top: 5px;" src="assets/img/sponsors/riverbed.png"></a>
                </div>-->


            </aside>
            <!-- Sidebar -->

            </div>
            </div>
            </section>
            <!-- /PAGE BLOG -->

        </div>
        <!-- /Content area -->
</div>
</div>
</article>
</section>
</div>
</div>
</section>
</div>
</section>
</div>
    <!-- FOOTER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
   