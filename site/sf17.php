<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf17-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'17 Retrospective</h1>
            <p>June 19th - 22nd, 2017<br>Carnegie Mellon University | Pittsburgh, PA</p>
        </div>
    </div>
    <section id="main-content">

    <!-- Content area -->
    <div class="content-area retrospective-page post-excerpt">

        <!-- PAGE BLOG -->
        <section class="page-section with-sidebar sidebar-right">
        <div class="container">
        <div class="row">

        <!-- Content -->
        <section id="content" class="content col-sm-7 col-md-8">

            <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                <div class="post-header">
                    <div class="post-meta">
                        
                        
                    </div>
                </div>
                <div class="post-body">
                    <div class="post-excerpt">
                        <h3 class="post-title2"><strong>Keynote Presentations</strong></h3>
                         <div class="responsive-iframe">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/oqxw3ePtZTM" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <p><strong>The Past, Present & Future of the Wireshark Project</strong><br/>
                        Gerald Combs</p>
                        <div class="responsive-iframe">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/WyVgEof2uKg" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <p><strong><a href="assets/presentations17/steenkiste-keynote.pdf">Experience with the eXpressive Internet Architecture</a></strong><br/>
                        Peter Steenkiste</p>
                        <h3 class="post-title2"><strong>SharkFest’17 US Attendee Feedback</strong></h3>
                            <blockquote>“It’s a wonderful time to have the privilege to participate with such talent
                            and good will as that which is found in the Wireshark community. I believe
                            that as the risks present within the Internet increase, Wireshark and
                            the Wireshark community stand poised at the intersection of the Internet
                            nexus to identify, mitigate, and secure network resources everywhere.<br><br>
                             
                            Thank you again for all that you do to bring together the Wireshark
                            community annually for SharkFest!”</blockquote>
                            <blockquote>“Thank you for organizing a fabulous event for everyone. It was a great experience and I learned a lot.”</blockquote>
                            <blockquote>
                            “In my humble opinion, Sharkfest is a great success. Many of the presentations show common case issues, and how Wireshark helps to identify them in their own ways. Most of the people I met at Sharkfest are small to medium size companies. We all have similar issues, but sometimes we just don’t know how and where to begin tackling the issue. It looks so easy when the experts present, trust me it’s not quite similar in real life. This conference helps us to navigate through these obstacle courses and helps us to do our job better.”</blockquote>
                            <blockquote>“Thank you for the great SharkFest' 17 US conference in Pittsburgh, Pennsylvania for the past days. All of you – and don’t forget the guys behind the scenes - have done a great job again and we all together survived.”</blockquote>
                        <!--
                        <h3 class="post-title2"><strong>Blogs</strong></h3>
                        <p><a href="http://blog.packet-foo.com/2014/06/sharkfest-2014-recap/">SharkFest 2015 Recap</a> by Jasper Bongertz</p> -->
                        <!--
                        <h3 class="post-title2"><strong><a href="assets/presentations15/packetchallenge.pdf">Packet Challenge</a></strong></h3>
                        <p>Click <a href="assets/presentations15/packetchallenge.zip">HERE</a> to download the Packet Challenge questions/answers and the pcap files!  
                        </p> -->
                            
                        

                        <h3 class="post-title2"><strong>Blogs</strong></h3>
                        <p><a href="https://blog.packet-foo.com/2017/07/sharkfest-2017-us-recap-10-years-of-sharkfest/">SharkFest'17 US Recap</a> by Jasper Bongertz</p>

                        <h3 class="post-title2"><strong>SharkBytes</strong></h3>
                        
                        <div class="responsive-iframe">
                            
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqPH_KUS074DvdLM0NSG8c8C" frameborder="0" allowfullscreen></iframe>
                        </div>
                        
                        
                        
                        
                        
                        <h3 class="post-title2"><strong>Tuesday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li>01: <a href="assets/presentations17/01-04.pdf">Practical Tracewrangling: Exploring Capture File Manipulation/Extraction Scenarios - Part 1</a> by Jasper Bongertz</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/d62QJlSqlgs" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:42)</li>
                        </ul>
                        <li>02:<a href="https://prezi.com/view/wsDlLM8dJSTl8P3pgYam/">  An Introduction to Wireshark: Rookie to Vet in 75 Minutes</a> by Betty DuBois</li>
                        
                        <li>03: <a href="assets/presentations17/03.pdf"> Using Wireshark to Solve Real Problems for Real People: Step-by-Step Real-World Case Studies in Packet Analysis</a> by Kary Rogers</li>
                        <!--<ul>
                        <li><a href="assets/presentations17/grahambloice.zip"> Presentation Materials</a></li>
                        </ul>-->
                        <ul>
                        </ul>
                        <li>04: <a href="assets/presentations17/01-04.pdf"> Practical Tracewrangling: Exploring Capture File Manipulation/Extraction Scenarios - Part 2</a> by Jasper Bongertz</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/UOvs0e5df8U" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:11)</li>
                        </ul> 
          
                        <li>05: <a href="assets/presentations17/05.pdf"> Network Security...Haven't We Solved It Yet?</a> by Mike Kershaw</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/15wDU3Wx1h0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:02:21)</li>
                        </ul>-->
                        <li>06: <a href="assets/presentations17/06.pdf"> Workflow-based Analysis of Wireshark Traces: Now we can all be Experts</a> by Paul Offord</li>
                        <ul>
                            <li class="presVideo"><a href="https://youtu.be/7800vK1FAh0" target="_blank">Presentation Video</a> (1:04:26)</li>
                        </ul>
                        <li>07: <a href="assets/presentations17/07.pptx"> Undoing the Network Blame Game and Getting to the Real Root Cause of Slow Application Performance </a> by Chris Greer</li>
                        
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/o53DFI_Y3QQ"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:06)</li>
                        </ul> 
                        <li>08: <a href="assets/presentations17/08.pdf">  Command Line Review of Wireshark CLI Tools, tshark & more</a> by Christian Landström</li>
                        
                        <li>09: <a href="assets/presentations17/09.pdf"> Designing a Requirements-Based Packet Capture Strategy </a>by John Pittle</li>
                        <li>10: <a href="assets/presentations17/10.pdf"> Knowing the Unknown: How to Monitor & Troubleshoot an Unfamiliar Network </a>by Luca Deri</li>
                       
                         <ul>
                        <li class="presVideo"><a href="https://youtu.be/4jZw3kFQ0jI?list=PLz_ZpPUgiXqNzojkgOSULR7RK8X7vdID-" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:28)</li>
                        </ul> 
                        <li>11: <a href="assets/presentations17/11.pdf"> HANDS-ON TCP Analysis </a>by Jasper Bongertz</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/HTtVHxIh6ww" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:19:03)</li>
                        </ul>-->

                        <li>12: <a href="assets/presentations17/12.pdf">Baselining with Wireshark to Identify & Stop Unwanted Communications</a> by Jon Ford</li>

                        </ul>

                        <h3 class="post-title2"><strong>Wednesday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li> 13:<a href="assets/presentations17/13.pptx"> Augmenting Packet Capture with Contextual Meta-Data: the What, Why & How</a> by Dr. Stephen Donnelly</li>
                         <ul>
                        <li class="presVideo"><a href="https://youtu.be/YnYAJSM-Jms" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:06:37)</li>
                        </ul> 
                        <li>14: <a href="assets/presentations17/14.pdf"> Wireshark Case Study Exploration</a> by Sake Blok</li>
                        <li>15: <a href="assets/presentations17/15.pdf">Wireshark & Time: Accurate Handling of Timing When Capturing Frames</a> by Werner Fischer</li> 
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/pb1yb1eUlgY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:00)</li>
                        </ul> -->
                        <li>16:<!--<a href="assets/presentations17/16-19.pdf">--> Hands-On Analysis of Multi-Point Captures – Part 1 </a> by Jasper Bongertz and Christian Landström</li> 
                         <ul>
                        <li class="presVideo"><a href="https://youtu.be/6Yf1f3_YBnw" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:37)</li>
                        </ul> 
                        <li>17: <a href="assets/presentations17/17.pptx"> WiFiBeat...Visualize Data with Kibana & ElasticSearch</a>by Thomas d'Otreppe</li>
                        
                       
                        <li>18: <a href="assets/presentations17/18.pptx"> Analyzing Exploit Kit Traffic with Wireshark</a> by Bradley Duncan</li>
                        <li>19: <!--<a href="assets/presentations17/16-19.pdf" title="Presentation slides" target="_blank">-->Hands-On Analysis of Multi-Point Captures – Part 2 </a> by Jasper Bongertz and Christian Landström</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/ll2JGgHHA3E" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:08:02)</li>
                        </ul> 
                        <li>20: <a href="assets/presentations17/20.pdf" title="Presentation slides" target="_blank">Work-Shmerk/Mirai-Shmiraii: What are Those Evil Little IoT Devices Doing & How Can You Control Them? </a> by Brad Palm</li>
                        <li>21: <a href="assets/presentations17/21.pdf">Analysis Visualizations: Creating charts inside and outside of Wireshark to speed up your Analysis</a> by Robert Bullen </li>
                        <!-- <ul>
                        <li><a href="assets/presentations17/HTTP2.zip"> Presentation Materials</a></li>
                        </ul>
                         -->
                         <!--
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/MIPS0RPn-aw"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:05:00)</li>
                        </ul>-->
                        <li>22: <a href="assets/presentations17/22.pdf" title="Presentation slides" target="_blank"> Understanding Throughput & TCP Windows: A Walk-Through of the Factors that can limit TCP Throughput Performance</a> by Kary Rogers</li> 
                         <ul>
                        <li class="presVideo"><a href="https://youtu.be/tyk2-0MY9p0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:07:08)</li>
                        </ul> 
                        <li>23: <a href="assets/presentations17/23.zip"> Top 10 Wireshark TIPS & Tricks</a> by Megumi Takeshita</li>
                        <li>24: <a href="assets/presentations17/07.pptx"> Undoing the Network Blame Game and Getting to the Real Root Cause of Slow Application Performance</a> by Chris Greer</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/o53DFI_Y3QQ"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:06)</li>
                        </ul> 
                        </ul>

                  
                        <h3 class="post-title2"><strong>Thursday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li>25: <a href="assets/presentations17/06.pdf">Workflow-based Analysis of Wireshark Traces: Now we can all be Experts</a> by Paul Offord</li> 
                        <ul>
                            <li class="presVideo"><a href="https://youtu.be/7800vK1FAh0" target="_blank">Presentation Video</a> (1:04:26)</li>
                        </ul>
                        <li>26: <!--<a href="assets/presentations17/26.pdf">-->Network Security...Haven't We Solved it Yet?</a> by Mike Kershaw</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/CaIAb462a3w" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:36)</li>
                        </ul>-->
                        <li>27:<!--<a href="assets/presentations17/27.pdf">--> Network Forensics with Wireshark </a>  by Laura Chappell</li>
                        <li>28: <!--<a href="assets/presentations17/28.pdf">-->The Doctor is In! Packet Trace Reviews with the Experts</a> by Hansang Bae, Jasper Bongertz, Christian Landström, Sake Blok</li>
                        <!--
                        <ul>
                        <li class="presVideo"><a href=" https://youtu.be/dnGdmMCchjk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:49)</li>
                        </ul> -->
                        <li>29: <a href="assets/presentations17/29.pdf"> A Web-Based Approach to Enhance Network Packet Capture & Decode Analysis Techniques using the Wireshark Command Line Tools</a> by Ronald Henderson</li>
                        <li>30: <!--<a href="assets/presentations17/30.pdf" title="Presentation slides" target="_blank">-->Using the Python/Django Web Framework to Build a Remote Packet Capture Portal with tshark</a> by Kevin Burns</li>
                        <li>31: <a href="https://prezi.com/view/JAbMcRiCJvox1fT5aSBX/"title="Presentation slides" target="_blank"> SMB/CIFS Analysis: Using Wireshark to Efficiently Analyze & Troubleshoot SMB/CIFS</a> by Betty DuBois</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/k2PfYOoRiKM" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:21)</li>
                        </ul>-->
                        <li>32: <a href="assets/presentations17/32.7z"> Writing a Wireshark Dissector: 3 Ways to Eat Bytes </a>by Graham Bloice</li>
                        <li>33: <!--<a href="assets/presentations17/33.pdf">-->Wireshark & Time: Accurate Handling of Timing When Capturing Frames</a> by Werner Fischer</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=7PO-9HxHGdo&t=7m48s" title="Presentation video on YouTube" target="_blank">Presentation Video Part 1</a> (45:25)</li>
                        </ul>-->
                        <li>34: <a href="assets/presentations17/34.pdf">How tshark saved my SDN Forensics: Hands-on tshark Usage with a Minor Python Connection</a> by Mike McAlister, Joseph Bull</li> 
                        <!-- <ul>
                        <li class="presVideo"><a href="https://youtu.be/GRAY1OceGc4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:26)</li>
                        </ul> -->
                        <li>35: <!--<a href="assets/presentations17/05.pptx"-->My Life as a Troubleshooter: So what did you do today, Dad?</a> by Graeme Bailey</li>
                        <!-- <ul>
                        <li class="presVideo"><a href="https://youtu.be/15wDU3Wx1h0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:02:21)</li>
                        </ul> -->
                        <li>36: <a href="assets/presentations17/36.pdf"> Validating Your Packet Capture: How to be sure you’ve captured correct & complete data for analysis</a> by Scott Haugdahl, Mike Canney</li>
                        <li>37: <a href="assets/presentations17/37-40.zip"> Back to the Packet Trenches (Part 1)</a> by Hansang Bae</li>
                        <li>38: <!--<a href="assets/presentations17/02.pdf">--> Wireshark Tips & Tricks</a> by Laura Chappell</li>
                        <li>39: <a href="assets/presentations17/10.pdf"> Knowing the Unknown: How to Monitor & Troubleshoot an Unfamiliar Network</a> by Luca Deri</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/4jZw3kFQ0jI?list=PLz_ZpPUgiXqNzojkgOSULR7RK8X7vdID-" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:28)</li>
                        </ul> 
                        <li>40: <a href="assets/presentations17/37-40.zip"> Back to the Packet Trenches (Part 2)</a> by Hansang Bae</li>
                        <li>41: <!--<a href="assets/presentations17/02.pdf">--> Analyzing Exploit Kit Traffic with Wireshark</a> by Bradley Duncan</li>
                        <li>42: <a href="assets/presentations17/42.pdf"> TCP SACK Overview & Impact on Performance</a> by John Pittle</li>
                         
                        <!--
                        <h2 class="post-title2">Shark Bytes</h2>
                        <iframe width="560" height="317" style="margin: 10px 0;" src="https://www.youtube.com/embed/c119qZnKFsw" frameborder="0" allowfullscreen></iframe>
                        <p>Shark Bytes consist of "little crunchy bits of wisdom". Like the immensely popular TED talks, Shark Bytes aim to inform, inspire, surprise, and delight as presenters share their personal perspective on a topic in under 5 minutes.</p>
                        -->
                        </ul>
                    </div>
                </div>
                
            </article>

            <!-- About the author -->
            
            <!-- /About the author -->

            <!-- Comments -->
            
            <!-- /Comments -->

            <!-- Leave a Comment -->
            
            <!-- /Leave a Comment -->

        </section>
        <!-- Content -->

        <hr class="page-divider transparent visible-xs"/>

        <!-- Sidebar -->
        <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
            <div class="background-blue front-column1">
                <div class="grid-cell">
                    <h4 class="widget-title">A Word of Thanks</h4>
                        <p>SharkFest&#39;17 US, the tenth anniversary of the conference, was a roaring success thanks to the highly engaged community of core developers and Wireshark users in attendance. Special thanks to Gerald Combs for tirelessly, fearlessly guiding the Wireshark open source project and maintaining its relevancy, to core developers for traveling long distances and braving severe storm-related delays to advance Wireshark code together, to Laura Chappell for delivering another excellent troubleshooting class, jumping in at the last minute to replace presenters unable to keep their commitments due to travel complications, and for creating another highly-anticipated Packet Challenge, to Dr. Peter Steenkiste for his provocative keynote, to Sake Blok for the many man-hours dedicated to creating a thrilling group packet competition, to a staff and volunteer crew who went far beyond caring to serve attendees during the conference, to instructors who voluntarily shuffled lives and schedules to educate participants and learn from one another, to sponsors who so generously provided resources that made the conference possible, to the CMU social hosts who made our social events truly social, and to the Carnegie Mellon Conference Events team for working through months of minutiae to help stage the conference on the rare and beautiful CMU campus.</p>
                </div>
            </div>
            <div class="widget categories">
                <!--
                <h4 class="widget-title">Past Years</h4>
                <ul>
                    <li><a href="sf14.html">SharkFest'14</a></li>
                    <li><a href="sf13.html">SharkFest'13</a></li>
                    <li><a href="sf12.html">SharkFest'12</a></li>
                    <li><a href="sf11.html">Sharkfest'11</a></li>
                    <li><a href="sf10.html">Sharkfest'10</a></li>
                    <li><a href="sf09.html">Sharkfest'09</a></li>
                    <li><a href="sf08.html">Sharkfest'08</a></li>
                </ul> -->
            </div>
            
            <div class="widget flickr-feed">
                
               

        
        <div class="post-media">
        <a style="font-size: 25px;" data-lightbox="sf17" alt="Click Here to View Pictures from Sharkfest'17!" href='img/sharkfest17gallery/1.jpg'><img src="img/sharkfest17gallery/sf17gal.jpg" ></a></div>
            <a href='img/sharkfest17gallery/2.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/3.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/4.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/5.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/5-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/6.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/6-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/7.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/7-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/8.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/8-5.JPG' data-lightbox="sf17"></a>
            
            <a href='img/sharkfest17gallery/10.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/10-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/11.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/12.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/13.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/14.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/15.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/16.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/17.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/18.jpg' data-lightbox="sf17"></a>
            
            <a href='img/sharkfest17gallery/23.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/23-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/24.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/24-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/25-6.jpg' data-lightbox="sf17"></a> 
             <a href='img/sharkfest17gallery/37.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/38.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/39.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/40.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/41.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/42.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/43.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/44.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/45.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/46.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/47.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/48.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/49.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/50.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/26.jpg' data-lightbox="sf17"></a>
            
            
            <a href='img/sharkfest17gallery/27.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/27-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/28.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/28-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/29.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/29-5.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/30.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/31.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/32.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/33.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/34.jpg' data-lightbox="sf17"></a>
            
            <a href='img/sharkfest17gallery/36.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/36-5.JPG' data-lightbox="sf17"></a>
           
            <a href='img/sharkfest17gallery/51.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/52.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/19.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/20.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/21.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/22.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/53.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/54.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/55.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/56.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/57.jpg' data-lightbox="sf17"></a> 
            <a href='img/sharkfest17gallery/58.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/59.jpg' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/60.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/61.JPG' data-lightbox="sf17"></a>
            <a href='img/sharkfest17gallery/62.jpg' data-lightbox="sf17"></a>

        <!-- Sidebar -->
<div class="background-blue front-column1">
    <div class="grid-cell">
        <div class="sponsor-list">
            <div class="sponsor-imgs">
                        <h4>Host Sponsors</h4>
                        <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>
                        <a href="http://www.wiresharktraining.com/" target="_blank"><img class="sponsors" src="img/sponsors/wiresharku-white.png"></a>

                        <h4>Angel Shark Sponsors</h4>
                        <a href="http://www.bigswitch.com/" target="_blank"><img class="sponsors" src="img/sponsors/bigswitch-white.png"></a>
                        <a href="http://datacomsystems.com/" target="_blank"><class="sponsors" src="img/sponsors15/datacom.png"></a>
                        <a href="http://www.dual-comm.com/" target="_blank"><img class="sponsors" src="img/sponsors/dualcomm-white.png"></a>
                          <a href="https://www.endace.com/" target="_blank"><class="sponsors" src="img/sponsors/endace2.png"></a>
                          <a href="https://www.extrahop.com/" target="_blank"><class="sponsors" src="img/sponsors/extrahop.png"></a>
                        <a href="https://www.gigamon.com/" target="_blank"><img class="sponsors" src="img/sponsors/gigamon-new.png"></a>
                        <a href="http://www.ixiacom.com/" target="_blank"><img class="sponsors" src="img/sponsors/ixia-white.png"></a>
                        <a href="https://www.performancevision.com/" target="_blank"><img" class="sponsors" src="img/sponsors/pv.png"></a>

                        <h4>Wireshark Group Packet Challenge Sponsor</h4>
                        
                        <a href="https://www.garlandtechnology.com/" target="_blank"><img class="sponsors" src="img/sponsors/garland-white.png"></a>

                         <h4>Welcome Gift Sponsor</h4>
                         <a href="http://fmad.io/" target="_blank"><img class="sponsors" src="img/sponsors/fmadio.jpg"></a>
                         <a href="https://ntop.org/" target="_blank"><img class="sponsors" src="assets/img/sponsors16eu/ntop-no-space.png"></a>

                         <h4>Breaks Sponsor</h4>
                         <a href="http://truepathtechnologies.com/" target="_blank"><img class="sponsors" src="img/sponsors/truepath-white.png"></a>

                        <h4>In Absentia Sponsors</h4>
                        <a href="http://accoladetechnology.com/" target="_blank"><img class="sponsors" src="img/sponsors/accolade.png"></a>
                    
                        <h4>Honorary Sponsors</h4>
                        <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                                            
                </div>
        </div>
        </section>
        </aside>
        <!-- /PAGE BLOG -->

    </div>
    <!-- /Content area -->
</div>
</section>
</div>
</section>
</div>
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>



