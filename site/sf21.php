<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf21-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'21 Virtual US Retrospective</h1>
            <p>September 12th - 17th, 2021<br>Online</p>
        </div>
    </div>
    <section id="main-content">
        <div class="content-area retrospective-page post-excerpt">

            <!-- PAGE BLOG -->
            <section class="page-section with-sidebar sidebar-right">
            <div class="container">
            <div class="row">

            <!-- Content -->
            <section id="content" class="content col-sm-7 col-md-8">

                <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="post-header">
                        <div class="post-meta">
                        </div>
                    </div>
                    <div class="post-body">
                        <div class="post-excerpt">
                            <h3 class="post-title2"><strong>Keynote Presentations</strong></h3>
                            <div class="retro-vid-wrapper">
                                    <div class="responsive-iframe">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/MwTUuQ3lRUs" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <h4><strong><a href="assets/presentations21/gerald-keynote.pptx">Latest Wireshark Developments & Road Map</a></strong><br/>
                                Gerald Combs</h4>
                            </div>
                            <div class="retro-vid-wrapper">
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/3eHiZ6omzsE" frameborder="0" allowfullscreen></iframe>
                                </div>
                                
                                <h4><strong>The Zed Project: Stumbling Upon a New Data Model for Search and Analytics while Hacking Packets</strong><br/>Steve McCanne, Coding CEO, Brim Security</h4>
                            </div>  
                            <h3 class="post-title2"><strong>Thursday Classes</strong></h3>
                            <ul style="list-style:none;">
                                <li>01: <a href="assets/presentations21/01.zip">Analyzing DNS from the Server Perspective</a> by Betty DuBois</li>
                                <li>02: <a href="assets/presentations21/02.pdf">Network Forensics Analysis</a> by Rami AlTalhi</li>
                                <li>03: <a href="assets/presentations21/03.pptx"> Visualizing TLS Encryption - making sense of TLS in Wireshark</a> by Ross Bagurdes</li>
                                <li>04: Analyzing Megalodon Files by Jasper Bongertz</li>
                                <li>05: <a href="assets/presentations21/05.pdf">Hello, what’s your name? An overview of Wireshark’s name resolution options (and it is not only for IP addresses!)</a> by Sake Blok</li>
                                <li>06: <a href="assets/presentations21/06.pptx"> Wireshark in use on LTE and 5G networks</a> by Mark Stout</li>
                                <li>07: <a href="assets/presentations21/07.pptx">Intro to QUIC - The TCP Killer?</a> by Chris Greer</li>
                                <li>08: Network Forensic Case Studies: Those Who Don't Learn from the Past Are Doomed to Repeat It by Phill Shade</li>
                                <li>09: Looking for "Packets" in all the "Right" Places by Patrick Kinnison</li>
                                <li>10: Back to the Packet Trenches by Hansang Bae</li>
                                <li>11: <a href="assets/presentations21/11.zip">School from home: Watching the Wire with Wireshark</a> by Anthony Efantis</li>
                                <li>12: Wireshark and Enterprise Packet Capture by Dr. Stephen Donnelly</li>
                            </ul>
                            
                            <h3 class="post-title2"><strong>Friday Classes</strong></h3>
                            <ul style="list-style:none;">
                                <li>13: <a href="assets/presentations21/13.zip">Analysis and Troubleshooting of IPsec VPNs</a> by Jean-Paul Archier</li>
                                <li>14: <a href="assets/presentations21/14.zip">How smart are my “things”? A traffic analysis of IoT Devices </a> by Simone Mainardi</li> 
                                <li>15: The Packet Doctors are in! Packet trace examinations with the experts by Chris Greer, Sake Blok, Betty DuBois, and Kary Rogers</li>
                                <li>17: <a href="assets/presentations21/17.pdf">When it’s NOT a “Network Problem” – Identifying Higher-Layer Issues in Packet Data</a> by Wes Morgan</li>
                                <li>18: <a href="assets/presentations21/18.pptx">Intrusion Analysis and Threat Hunting with Suricata</a> by Josh Stroschein and Peter Manev</li>
                                <li>19: <a href="assets/presentations21/19.zip" title="Presentation slides" target="_blank">How I Learned to Stop Worrying and Love the PCAP </a> by Kary Rogers</li>
                                <li>20: Build Your Own IPv6 Learning Lab – for FREE (part 1) by Jeff Carrell</li>
                                <li>21: <a href="assets/presentations21/21.zip">TCP SACK overview & impact on performance</a> by John Pittle </li>
                                <li>22: Build Your Own IPv6 Learning Lab – for FREE (part 2)</a> by Jeff Carrell</li> 
                                <li>23: <a href="assets/presentations21/07-23.pdf">Wireshark and WiFi: capture techniques and challenges</a> by George Cragg</li>
                                <li>24: <a href="assets/presentations21/24.pdf">Capturing goodies: Wireshark on iPad pro and utilization of extcap interfaces</a> by Megumi Takeshita</li>
                            </ul>
                        </div>
                    </div>
                    
                </article>

            </section>
            <!-- Content -->

            <hr class="page-divider transparent visible-xs"/>

            <!-- Sidebar -->
            <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <div class="sponsor-list">
                            <div class="sponsor-imgs">
                                    <h4>Host Sponsor</h4>
                                    <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>

                                    <h4>Angel Shark Sponsors</h4>
                                    <a href="https://www.endace.com/" target="_blank"><img class="sponsors" src="img/sponsors/endace_big.png"></a>
                                    <a href="https://gnet-inc.com/" target="_blank"><img class="sponsors" src="img/sponsors/gnet.png"></a>
                                    <a href="https://fmad.io/" target="_blank"><img class="sponsors" src="img/sponsors/fmadio.jpg"></a>
                            </div>                         
                        </div>
                    </div>
                </div>
            </aside>
            </div>
            </section>
            <!-- /PAGE BLOG -->

        </div>
        <!-- /Content area -->
    </section>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>



