<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
    <!-- Content area -->
    <div class="container grid-container" id="main-container">
    <div class="sf09-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'08 Retrospective</h1>
            <p>March 31st - April 2nd, 2008<br>Foothill College | Los Altos Hills, California</p>
        </div>
    </div>
    <section id="main-content">
            <div class="content-area retrospective-page">

                <!-- PAGE BLOG -->
                <section class="page-section with-sidebar sidebar-right">
                <div class="container">
                <div class="row">

                <!-- Content -->
                <section id="content" class="content col-sm-7 col-md-8">

                    <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                        <div class="post-media">
                        </div>
                        <div class="post-header">
                            <div class="post-meta">
                                
                                
                            </div>
                        </div>
                        <div class="post-body">
                            <div class="post-excerpt">
                                <h3 class="post-title2"><strong>Links</strong></h3>
                                <ul>
                               
                                    <li><a href="http://laurachappell.blogspot.com/">Laura Chappell's blog</a></li>
                                    <li><a href="http://www.lovemytool.com/blog/sharkfest/">SHARKFEST coverage at LoveMyTool</a></li>
                                    <li><a href="http://computemagazine.com/technology/open-source-founders-reflect-on-project-milestones.html">Compute Magazine</a></li>
                                    <li><a href="http://thenetworkguy.typepad.com/">Scott Haugdahl</a></li>
                      
                                </ul>
                                    

                                <h3 class="post-title2"><strong>Pictures</strong></h3>
                                <ul style="list-style:none;">
                                   <li><a href="http://sharkfest08.snapfish.com/snapfish">Snapfish</a> (account required)</li>
                                </ul>

                                <h3 class="post-title2"><strong>Keynote Presentation</strong></h3>
                                <p><a href="http://sharkfest.wireshark.org/sharkfest.08/Sharkfest08_Keynote_Cerf.pdf" target="_blank">Sharkfest08_Keynote_Cerf.pdf</a></p>
                                
                                <h3 style="" class="post-title2"><strong>Presentations</strong></h3>
                                <ul style="list-style:none;">
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/BoF_Varenni_%20WinPcap%20Do's%20and%20Don'ts.zip">BoF_Varenni_ WinPcap Do's and Don'ts.zip</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/D01_Blok_Advanced%20Scripting,%20Command%20Line%20Usage%20with%20tshark.ppt" target="_blank">D01_Blok_Advanced Scripting, Command Line Usage with tshark.ppt</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/D02_Combs_Intro%20to%20Writing%20Wireshark%20Packet%20Dissectors.ppt" target="_blank">D02_Combs_Intro to Writing Wireshark Packet Dissectors.ppt</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/D03_Harris_Writing%20Wireshark%20Dissectors_Advanced.pdf" target="_blank">D03_Harris_Writing Wireshark Dissectors_Advanced.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/D04_Varenni_Writing%20your%20own%20Packet%20Capture%20Tool%20with%20WinPcap%20and%20AirPcap.zip">D04_Varenni_Writing your own Packet Capture Tool with WinPcap and AirPcap.zip</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/D05_Leutert_Analysing%20802.11n%20MIMO.pdf" target="_blank">D05_Leutert_Analysing 802.11n MIMO.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/D07_Lamping_Extending%20the%20Wireshark%20UI.zip">D07_Lamping_Extending the Wireshark UI.zip</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/D06_Johnson_802.11%20Packet%20Dissection%20with%20AirPcap,%20WinPcap.pdf" target="_blank">D06_Johnson_802.11 Packet Dissection with AirPcap, WinPcap.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/D09_Sharpe_File%20and%20Disk%20Sharing%20Protocols.ppt" target="_blank">D09_Sharpe_File and Disk Sharing Protocols.ppt</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/R01_Bae_Protocol%20Analysis%20in%20Complex%20Ent.%20Infrastructures.ppt" target="_blank">R01_Bae_Protocol Analysis in Complex Ent. Infrastructures.ppt</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-1_DuBois_Downloaded%20Wireshark%20-%20Now%20What.ppt" target="_blank">T1-1_DuBois_Downloaded Wireshark - Now What.ppt</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-2_Tviet_Virtues%20of%20Continuous,%20Complete%20Packet%20Capture.pdf" target="_blank">T1-2_Tviet_Virtues of Continuous, Complete Packet Capture.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-3_Chappell_-Performance%20Problems.pdf" target="_blank">T1-3_Chappell_-Performance Problems.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-5_Bardwell_Intro%20to%20WLAN%20Analysis.pdf" target="_blank">T1-5_Bardwell_Intro to WLAN Analysis.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-6_Wright%20and%20Kershaw_Leveraging%20Wireshark%20for%20Net%20Analysis.ppt" target="_blank">T1-6_Wright and Kershaw_Leveraging Wireshark for Net Analysis.ppt</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-7_DOtreppe_WLAN%20Analysis%20and%20Security.pdf" target="_blank">T1-7_DOtreppe_WLAN Analysis and Security.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-8_Leong_Non-Intrusive%20OOB%20Network%20Monitoring.ppt.pps" target="_blank">T1-8_Leong_Non-Intrusive OOB Network Monitoring.ppt.pps</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-10_Walberg_Expose%20VoIP%20problems%20with%20Wireshark.ppt" target="_blank">T1-10_Walberg_Expose VoIP problems with Wireshark.ppt</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-12_Tuexen_SCTP_SIGTRAN%20and%20SS7.pdf" target="_blank">T1-12_Tuexen_SCTP_SIGTRAN and SS7.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T1-13_Bihary_Tapping%20Basics.ppt" target="_blank">T1-13_Bihary_Tapping Basics.ppt</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-1_Fortunato_Analyzer%20Placement%20and%20Baseline%20Techniques.pdf" target="_blank">T2-1_Fortunato_Analyzer Placement and Baseline Techniques.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-2_Chappell_TCPIP_Resolution.pdf" target="_blank">T2-2_Chappell_TCPIP_Resolution.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-4_Chappell_Trace-File-Analysis_Latency.pdf" target="_blank">T2-4_Chappell_Trace-File-Analysis_Latency.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-5_Fortunato_Adv.%20Capture%20and%20Display%20Filtering.pdf" target="_blank">T2-5_Fortunato_Adv. Capture and Display Filtering.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-6_Chappell_DuBois_Trace%20file%20analysis%20-%20TCP%20window.pdf" target="_blank">T2-6_Chappell_DuBois_Trace file analysis - TCP window.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-7_Chappell_Network-Forensics.pdf" target="_blank">T2-7_Chappell_Network-Forensics.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-8_Chappell_Trace-File-Analysis_Loss.pdf" target="_blank">T2-8_Chappell_Trace-File-Analysis_Loss.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-9_Chappell_DuBois_Top%2010.pdf" target="_blank">T2-9_Chappell_DuBois_Top 10.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-10_Chappell_DuBois_Trace%20file%20analysis%20-%20Case%20Studies.pdf" target="_blank">T2-10_Chappell_DuBois_Trace file analysis - Case Studies.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-11_Fortunato_HTTP%20Trace%20File%20Analysis.pdf" target="_blank">T2-11_Fortunato_HTTP Trace File Analysis.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-12_Chappell_Complementary-Tools.pdf" target="_blank">T2-12_Chappell_Complementary-Tools.pdf</a></li>
                                    <li><a href="http://sharkfest.wireshark.org/sharkfest.08/T2-13_Chappell_Trace-File-Analysis_Infected.pdf" target="_blank">T2-13_Chappell_Trace-File-Analysis_Infected.pdf</a></li>
                                 </ul>
                                

                            </div>
                        </div>
                        
                    </article>

                    <!-- About the author -->
                    
                    <!-- /About the author -->

                    <!-- Comments -->
                    
                    <!-- /Comments -->

                    <!-- Leave a Comment -->
                    
                    <!-- /Leave a Comment -->

                </section>
                <!-- Content -->

                <hr class="page-divider transparent visible-xs"/>

                <!-- Sidebar -->
                <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
                    <div class="background-blue front-column1">
                        <div class="grid-cell">
                            <h4 class="widget-title">A Word of Thanks</h4>
                            <p>We would like to thank all of the Sharkfest attendees, presenters, and supporters for helping to create a great conference. See you next year!</p>
                        </div>
                    </div>
                </div>
                </div>
                </section>
                <!-- /PAGE BLOG -->

            </div>
            <!-- /Content area -->
</aside>
</div>
</div>
</section>
</div>
</section>
</div>
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>