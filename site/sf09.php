    <!-- HEADER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
    <!-- /HEADER -->

<div class="container grid-container" id="main-container">
    <div class="sf09-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'09 Retrospective</h1>
            <p>June 15th - 18th, 2009<br>Stanford University | Stanford, California</p>
        </div>
    </div>
    <section id="main-content">


    <!-- Content area -->
        <div class="content-area retrospective-page">

            <!-- PAGE BLOG -->
            <section class="page-section with-sidebar sidebar-right">
            <div class="container">
            <div class="row">

            <!-- Content -->
            <section id="content" class="content col-sm-7 col-md-8">

                <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="post-media">
                    </div>
                    <div class="post-header">
                        <h3 class="post-title"></h3>
                        <div class="post-meta">
                            
                            
                        </div>
                    </div>
                    <div class="post-body">
                        <div class="post-excerpt">
                            <h3 class="post-title2"><strong>Post Fest Reviews and Press</strong></h3>
                            <ul>
                            <li><a href="http://wlaniconoclast.blogspot.com/2009/06/sharkfest.html">Keith Parsons</a></li>
                            <li><a href="http://www.plixer.com/blog/netflow-analyzer/sharkfest-2009-a-great-event/">Michael Patterson</a></li>
                            <li><a href="http://laurachappell.blogspot.com/2009/06/iphone-youre-sexy-but-you-talk-too-much.html">Laura Chappell</a></li>
                            <li><a href="http://www.heise.de/open/Wireshark-ein-Business-Modell-steht-Kopf--/artikel/141265">Wireshark – ein Business-Modell steht Kopf</a></li>
                            <li><a href="http://www.heise.de/newsticker/SharkFest-2009-Zusammenkunft-der-Netzwerk-Haie--/meldung/140870">SharkFest 2009: Zusammenkunft der Netzwerk-Haie</a></li>
                            <li><a href="http://www.lovemytool.com/blog/2009/06/joke_snelders1.html">Show Notes from Sharkfest 2009</a></li>
                            </ul>
                                

                            <h3 class="post-title2"><strong>Pictures</strong></h3>
                            <ul style="list-style:none;">
                                <li><a href="http://www.facebook.com/album.php?aid=91415&amp;id=51502216870&amp;l=b587bd9039">Joan Snelders</a></li>
                                <li><a href="http://www.facebook.com/album.php?aid=2014140&amp;id=1372519431&amp;l=cc0c1de03b">Angela Sherman</a></li>
                            </ul>

                            <h3 class="post-title2"><strong>Keynote Presentation</strong></h3>
                            <p><a href="http://sharkfest.wireshark.org/sharkfest.09/Key-2_Roberts_Evolution%20of%20the%20Internet.ppt" target="_blank">Key-2 (Roberts) Evolution of the Internet</a></p>
                            
                            <h3 style="" class="post-title2"><strong>Basic Track Presentations</strong></h3>
                            <ul style="list-style:none;">
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/bu-2-tompkins-gearbit-wireshark-how-protocols-work-sharkfest09.pdf" target="_blank">BU-2 (Tompkins) How Protocols Work</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/BU3_Carpio_FundamentalsOfPassiveMonitoringAccessFINAL.pps" target="_blank">BU-3 (Carpio) Fundamentals Of Passive Monitoring Access</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/BU-4%20(DuBois)%20I%20just%20downloaded%20Wireshark%20-%20Now%20what%20do%20I%20do.zip">BU-4 (DuBois) I just downloaded Wireshark - Now what do I do</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/BU5_Leutert_Analyzing%20WLANs%20with%20Wireshark%20&%20AirPcap.pdf" target="_blank">BU-5 (Leutert) Analyzing WLANs with Wireshark &amp; AirPcap</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/BU-6_Webb_PoweringNetworkVisibility_rev1.zip">BU-6 (Webb) Powering Network Visibility</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/BU-7_O'Donnell_The%20Reality%20of%2010G%20Analysis.pptx" target="_blank">BU-7 (O'Donnell) The Reality of 10G Analysis</a></li>
                              <li>BU-8 (Degioanni) Complementing Wireshark in Wireless Troubleshooting.<br> <i>Live demonstration Session, no slides available.</i> </li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/bu-9-tompkins-gearbit-wireshark_charts_&_io_graphs-sharkfest09.pdf" target="_blank">BU-9 (Tompkins) Wireshark Charts &amp; I/O Graphs</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/BU-10_Bardwell_Sharkfest2009-HANDOUT-J0615.pdf" target="_blank">BU-10 (Bardwell) Wireshark Saves the WLAN!</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/BU-11%20(DuBois)%20SPAN%20vs.%20Taps%20-%20When%20to%20use%20what%20-%20Benefits%20and%20Caveats.zip">BU-11 (DuBois) SPAN vs. Taps - When to use what - Benefits and Caveats</a></li>
                            </ul>
              
          
                  <h3 class="post-title2"><strong>Advanced Track Presentations</strong></h3>
                  
            <ul style="list-style:none;">
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/AU-1_Heine_Wireless%20Network%20Optimization%20with%20Wireshark.pdf" target="_blank">AU-1 (Heine) Wireless Network Optimization with Wireshark</a></li>
                              <li>AU-3 (Degioanni) CACE Labs SNEAK PEAK of Pilot v2.0. <br><i>Live demonstration Session, no slides available.</i> </li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/AU2_Blok_SSL_Troubleshooting_with_Wireshark_and_Tshark.pps" target="_blank">AU-2 (Blok) SSL Troubleshooting with Wireshark and Tshark</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/AU-5_Bae_Protocol%20Analysis%20in%20a%20Complex%20Enterprise.ppt" target="_blank">AU-4, AU-5 (Bae) Protocol Analysis in a Complex Enterprise</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/AU-6_Patterson_SuccessWithNetFlow.zip">AU-6 (Patterson) Successful Ways to Use NetFlow</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/au7-hinz-yr20-industrialethernet-sharkfest09.ppt" target="_blank">AU-7 (Hinz) Industrial Ethernet</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/au-8-tompkins-gearbit-wireshark-finding-the-latency-sharkfest09.pdf" target="_blank">AU-8 (Tompkins) Finding the Latency</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/AU9-Walberg-Expose%20VoIP%20problems%20with%20wireshark.ppt" target="_blank">AU-9 (Walberg) Expose VoIP Problems with Wireshark</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/AU-10_Chappell_Network_Forensics-Wireshark_as_Evidence_Collector.pptx" target="_blank">AU-10 (Chappell) Network Forensics - Wireshark as Evidence Collector</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/AU11_Tuexen_SCTP.pdf" target="_blank">AU-11 (Tüxen) SCTP</a></li>
                              <li><a href="http://sharkfest.wireshark.org/sharkfest.09/AU-12_Chappell_Tips_and_Tricks-Enterprise_Case_Studies.pptx" target="_blank">AU-12 (Chappell) Tips and Tricks - Enterprise Case Studies</a></li>
            </ul>
                  <h3 class="post-title2"><strong>Developer Track Presentations</strong></h3>
                  
            <ul style="list-style:none;">
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT-1_Combs_Writing%20Wireshark%20Dissectors%20and%20Plugins.zip">DT-1 (Combs) Writing Wireshark Dissectors &amp; Plugins</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT-2_Tuexen_SourceCode.pdf" target="_blank">DT-2 (Tüxen) Source Code</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT-2_Combs_API%20Additions.pdf" target="_blank">DT-2 (Combs) API Additions</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT-3_Donnelly_Now%20and%20Then%20How%20and%20When.ppt" target="_blank">DT-3 (Donnelly) Now and Then How and When</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT5_Varenni_WinPcapDosDonts.pptx" target="_blank">DT-5 (Varenni) WinPcap Do's &amp; Dont's</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT06_Bjorlykke_Lua%20Scripting%20in%20Wireshark.pdf" target="_blank">DT-6 (Bjørlykke) Lua Scripting in Wireshark</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT-7_Kershaw_Get%20Thinking%20About%20WiFi%20Security!.ppt" target="_blank">DT-7 (Kershaw) Get Thinking About WiFi Security!</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT-8-Napatech-Presentation-Sharkfest-2009.pdf" target="_blank">DT-8 (Sanders) Wireshark in a Multi-Core Environment Using Hardware Acceleration</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT9_Fisher_ExtendWiresharkWithGTK.ppt" target="_blank">DT-9 (Fisher) Extend Wireshark With GTK</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.09/DT10_Varenni_WritingYourOwnPacketCaptureToolWithWinPcapAndAirPcap.zip">DT-10 (Varenni) Writing Your Own Packet Capture Tool With WinPcap &amp; AirPcap</a></li>
        </ul>
                            

                        </div>
                    </div>
                    
                </article>

                <!-- About the author -->
                
                <!-- /About the author -->

                <!-- Comments -->
                
                <!-- /Comments -->

                <!-- Leave a Comment -->
                
                <!-- /Leave a Comment -->

            </section>
            <!-- Content -->

            <hr class="page-divider transparent visible-xs"/>

            <!-- Sidebar -->
            <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
                <div class="background-blue front-column1">
                  <div class="grid-cell">
                    <h4 class="widget-title">A Word of Thanks</h4>
                    <p>Another Sharkfest has come and gone, and we thank each participant, sponsor, presenter, keynote speaker, coordinator, caterer, staff and general support person for making this a successful, richly educational event once again. All session presentations are available from this page, and recordings of many sessions can be found at <a href="www.lovemytool.com">www.lovemytool.com</a>.</p>
                </div>
              </div>
                
            </div>
            </div>
            </section>
            <!-- /PAGE BLOG -->

        </div>
        <!-- /Content area -->
</aside>
</div>
</div>
</section>
</div>
</section>
</div>


        <!-- FOOTER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
    <!-- /FOOTER -->

    