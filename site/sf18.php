<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf18-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'18 Retrospective</h1>
            <p>June 25th - 28th, 2018<br>Computer History Museum | Mountain View, California</p>
        </div>
    </div>
    <section id="main-content">
        <div class="content-area retrospective-page post-excerpt">

            <!-- PAGE BLOG -->
            <section class="page-section with-sidebar sidebar-right">
            <div class="container">
            <div class="row">

            <!-- Content -->
            <section id="content" class="content col-sm-7 col-md-8">

                <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="post-header">
                        
                        <div class="post-meta">
                            
                            
                        </div>
                    </div>
                    <div class="post-body">
                        <div class="post-excerpt">

                            
                            <!--
                            <h3 class="post-title2"><strong><a href="assets/presentations15/packetchallenge.pdf">Packet Challenge</a></strong></h3>
                            <p>Click <a href="assets/presentations15/packetchallenge.zip">HERE</a> to download the Packet Challenge questions/answers and the pcap files!  
                            </p> -->
                                
                            <h3 class="post-title2"><strong>Keynote Presentations</strong></h3>
                                <div class="retro-vid-wrapper">
                                     <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/YBOQXQT5dEA" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <h4><strong>Twenty Years Of Code And Community</strong><br/>
                                    Gerald Combs</h4>
                                </div>
                                <div class="retro-vid-wrapper">
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/O-ZJ3gaBZkE" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    
                                    <h4><strong>Wireshark: The Microscope of the 21st Century</strong><br/>
                                    Usman Muzaffar</h4>
                                </div>
                                <h3 class="post-title2"><strong>Blogs</strong></h3>
                                <div class="retro-vid-wrapper">
                                    <p><a href="https://youtu.be/7Sno828oTkk">SharkFest US 2018 Review</a> by Denise    Fishburne</p>
                                </div>

                                    <h3 class="post-title2"><strong>SharkBytes</strong></h3>
                                <div class="retro-vid-wrapper"> 
                                    <div class="responsive-iframe">
                                        
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqPOFTJfNCfuzCwU67AUBgeN" frameborder="0" allowfullscreen></iframe>
                                    </div> 
                                </div>
                            
                            
                            
                            
                            
                            <h3 class="post-title2"><strong>Tuesday Classes</strong></h3>
                            <ul style="list-style:none;">
                            <li>01: <a href="#">In the Packet Trenches - (Part 1)</a> by Hansang Bae</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=u9n1KG7f3N8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:46)</li>
                            </ul>
                            <li>02:<a href="https://prezi.com/view/ggGOl7RHv0Xuen5YwS82/">  An Introduction to Wireshark: Rookie to Veteran in 2 sessions (Part 1)</a> by Betty DuBois</li>
                            
                            <li>03: <a href="assets/presentations18/03.7z"> Writing a Wireshark Dissector: 3 ways to eat bytes</a> by Graham Bloice</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/biNdEqWoxrE" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:11)</li>
                            </ul>
                            <li>04: <a href="#"> In the Packet Trenches - (Part 2)</a> by Hansang Bae</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/DhYwn8iXuf0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:11)</li>
                            </ul> 
              
                            <li>05: <a href="https://prezi.com/view/ggGOl7RHv0Xuen5YwS82/">  An Introduction to Wireshark: Rookie to Veteran in 2 sessions (Part 2)</a> by Betty DuBois</li>
                            <!--<ul>
                            <li class="presVideo"><a href="https://youtu.be/15wDU3Wx1h0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:02:21)</li>
                            </ul>-->
                            <li>06: <a href="assets/presentations18/06.pdf"> Using more of the features of Wireshark to write better dissectors</a> by Richard Sharpe</li>
                            <!--<ul>
                                <li class="presVideo"><a href="https://youtu.be/7800vK1FAh0" target="_blank">Presentation Video</a> (1:04:26)</li>
                            </ul>-->
                            <li>07: <a href="assets/presentations18/07.pptx"> Using Wireshark to solve real problems for real people: Step by-step case studies in packet analysis </a> by Kary Rogers</li>
                            
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/8N6cwWrhGXM"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:53)</li>
                            </ul> 
                            <li>08: <a href="assets/presentations18/31.pdf">  Traffic analysis of cryptocurrency & blockchain networks</a> by Brad Palm and Brian Greunke</li>
                            
                            <li>09: <a href="#"> Developer Lightning Talks </a>by Wireshark Core Developers</li>
                            <li>10: <a href="assets/presentations18/10.pdf"> Hands-on analysis of multi-point captures </a>by Christian Landström</li>
                           
                            <!-- <ul>
                            <li class="presVideo"><a href="https://youtu.be/4jZw3kFQ0jI?list=PLz_ZpPUgiXqNzojkgOSULR7RK8X7vdID-" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:28)</li>
                            </ul> -->
                            <li>11: <a href="assets/presentations18/11.pptx"> Augmenting packet capture with contextual meta-data: the what, why, and how </a>by Stephen Donnelly</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/er_HZvLZuZA" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (51:43)</li>
                            </ul>

                            <li>12: <a href="assets/presentations18/12.zip">Point and Shoot Packet! Point your packet effectively & Shoot the trouble with Wireshark</a> by Megumi Takeshita</li>

                           

                            
                            <li> 13:<a href="assets/presentations18/13.pdf"> Practical Tracewrangling: exploring capture file manipulation/extraction scenarios</a> by Jasper Bongertz</li>
                             <ul>
                            <li class="presVideo"><a href="https://youtu.be/7tGfywGdrIU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:31:02)</li>
                            </ul> 
                            <li>14: <a href="#">BGP is not only a TCP session: Learning about the protocol that holds networks together</a> by Werner Fischer</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/GNP-sRzvbGY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:00:04)</li>
                            </ul>
                            <li>15: <a href="#">How to get 100% of your data off the wire</a> by Greg Zemlin</li> 

                            <!--<ul>
                            <li class="presVideo"><a href="https://youtu.be/0X2BVwNX4ks" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:00)</li>
                            </ul> -->
                            </ul>
                            <h3 class="post-title2"><strong>Wednesday Classes</strong></h3>
                            <ul style="list-style:none;">
                            <li>16:<!--<a href="assets/presentations18/16-19.pdf">--> TCP - Tips, Tricks, & Traces (Part 1) </a> by Chris Greer</li> 
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/P00qomrZfw4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:23)</li>
                            </ul> 
                            <li>17: <a href="assets/presentations18/17.pptx"> extcap – Packet capture beyond libpcap/winpcap: bluetooth sniffing, android dumping & other fun stuff! </a>by Roland Knall</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/GjseviQJDaU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (45:35)</li>
                            </ul>
                           
                            <li>18: <a href="assets/presentations18/18.pdf">    Generating Wireshark Dissectors: A status report</a> by Richard Sharpe</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/fOseb4BSra8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (56:11)</li>
                            </ul>
                            <li>19: <!--<a href="assets/presentations18/16-19.pdf" title="Presentation slides" target="_blank">-->TCP - Tips, Tricks, & Traces (Part 2) </a> by Chris Greer</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/RNYE7DelF3o" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:11:54)</li>
                            </ul> 
                            <li>20: <a href="#" title="Presentation slides" target="_blank">Wireshark in the “Real World”: Top ways to use Wireshark in the real world of an IT engineer</a> by Patrick Kinnison</li>
                            <li>21: <a href="assets/presentations18/21.pdf">sFlow: Theory & practice of a sampling technology and its analysis with Wireshark</a> by Simone Maindardi </li>
                            <!-- <ul>
                            <li><a href="assets/presentations18/HTTP2.zip"> Presentation Materials</a></li>
                            </ul>
                             -->
                             
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/SX_LBoGgZK4"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:53)</li>
                            </ul>
                            <li>22: <a href="assets/presentations18/22.pdf" title="Presentation slides" target="_blank"> Writing a TCP analysis expert system</a> by Jasper Bongertz</li> 
                             <ul>
                            <li class="presVideo"><a href="https://youtu.be/TbADZswg_h8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:37)</li>
                            </ul> 
                            <li>23: <a href="assets/presentations18/23.zip"> Playing with "MATCHES": Using regular expressions for fun & profit</a> by Mike Hammond</li>
                            <ul>
                                <li class="presVideo"><a href="https://youtu.be/tyEEQJ-ikjY" target="_blank">Presentation Video</a> (1:21:08)</li>
                            </ul>
                            <li>24: <a href="assets/presentations18/24.pdf"> Know Abnormal, Find Evil: A Wireshark Beginner’s Guide for the Security Professional</a> by Maher Adib</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/bLUFZ_ysrYk"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:12)</li>
                            </ul> 
                            
                      
                            
                            <li>25: <a href="https://prezi.com/view/McyHI6DRM3V536k84t2o/">A deep dive into SIP: everything you need to know to debug & troubleshoot SIP packets</a> by Betty DuBois</li> 
                            <!--<ul>
                                <li class="presVideo"><a href="https://youtu.be/tyEEQJ-ikjY" target="_blank">Presentation Video</a> (1:04:26)</li>
                            </ul>-->
                            <li>26: <!--<a href="assets/presentations18/26.pdf">-->Analyzing Windows malware traffic with Wireshark</a> by Bradley Duncan</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/hL04q6fXHV8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:37:01)</li>
                            </ul>
                            <li>27:<!--<a href="assets/presentations18/27.pdf">--> My TCP ain’t your TCP: Stack behavior back then & today </a>  by Simon Lindermann</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/OvV17GQlw-I" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:49)</li>
                            </ul>
                            <li>28: <!--<a href="assets/presentations18/28.pdf">-->The Packet Doctors are In! Packet trace examinations by the experts</a> by Hansang Bae, Jasper Bongertz, Christian Landström, Sake Blok and Kary Rogers</li>
                            <!--
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/OvV17GQlw-I" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:49)</li>
                            </ul> -->
                            <li>29: <a href="#"> Baselining with Wireshark to identify & stop unwanted communications</a> by Jon Ford</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/6vK-QDYxQB0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (59:51)</li>
                            </ul>
                            
                            <li>30: <a href="assets/presentations18/30.pdf" title="Presentation slides" target="_blank">BGP is not only a TCP session: Learning about the protocol that holds networks together</a> by Werner Fischer</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/GNP-sRzvbGY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:00:04)</li>
                            </ul> 
                            </ul>
                            <h3 class="post-title2"><strong>Thursday Classes</strong></h3>
                            <ul style="list-style:none;">
                            <li>31: <a href="assets/presentations18/31.pdf"> Traffic analysis of cryptocurrency & blockchain networks</a> by Brad Palm and Brian Greunke</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/MHEH4KWQtSQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:21:15)</li>
                            </ul>
                            <li>32: <a href="assets/presentations18/32.pdf"> We’ll never do it right: A look at security, what we’re doing and how we’re trying to fix things </a>by Mike Kershaw</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/xp-QwN79dBY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:26:03)</li>
                            </ul> 
                            <li>33: <a href="assets/presentations18/33.zip">Wireshark CLI tools & scripting</a> by Sake Blok</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/IZ439VNvJqo" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:11:14)</li>
                            </ul>
                            <li>34: <a href="#">Patterns in TCP retransmissions: Using Wireshark to better understand the retransmission process</a> by Scott Reid</li> 
                             <ul>
                            <li class="presVideo"><a href="https://youtu.be/yFlgTWRNdhg" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:11:33)</li>
                            </ul> 
                            <li>35: <!--<a href="assets/presentations18/05.pptx"-->Behind the Green Lock: Examining SSL encryption/decryption using Wireshark</a> by Ross Bagurdes</li>
                             <ul>
                            <li class="presVideo"><a href="https://youtu.be/0X2BVwNX4ks" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:02:20)</li>
                            </ul> 
                            <li>36: <a href="#"> Wireshark and beyond! Complementing your Wireshark analysis with other open source & low-cost tools</a> by Mike Canney</li>
                            <li>37: <a href="#"> Packet monitoring in the days of IoT and Cloud</a> by Luca Deri</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/RGt7gWICWGs" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:09)</li>
                            </ul>
                            <li>38: <!--<a href="assets/presentations18/02.pdf">--> Baselining with Wireshark to identify & stop unwanted communications</a> by Jon Ford</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/6vK-QDYxQB0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (59:51)</li>
                            </ul>
                            <li>39: <a href="#"> Introduction to practical network signature development for open source IDS (Part 1)  </a> by Jason Williams and Jack Mott</li>
                             
                            <li>40: <a href="assets/presentations18/40.pptx"> Mangling packets on the fly with divert sockets: how to hack a Cisco router ACL</a> by Kary Rogers</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/3eBjqRb0FPw" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:04:33)</li>
                            </ul>
                            <li>41: <!--<a href="assets/presentations18/02.pdf">--> My TCP ain’t your TCP: Stack behavior back then and today</a> by Simon Lindermann</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/OvV17GQlw-I" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:49)</li>
                            </ul>
                            <li>42: <a href="#"> Introduction to practical network signature development for open source IDS (Part 2)  </a> by Jason Williams and Jack Mott</li>
                            <li>43: <a href="#"> OPEN FORUM: Aha! Moments in packet analysis</a> by Chris Greer</li>
                            <li>44: <a href="#"> Analyzing Windows malware traffic with Wireshark</a> by Bradley Duncan</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/hL04q6fXHV8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:37:01)</li>
                            </ul>
                            <li>45:  <a href="#"> Introduction to practical network signature development for open source IDS (Part 3)  </a> by Jason Williams and Jack Mott</li>
                            </ul>
                            <!--
                            <h3 class="post-title2">Shark Bytes</h3>
                            <iframe width="560" height="318" style="margin: 10px 0;" src="https://www.youtube.com/embed/c119qZnKFsw" frameborder="0" allowfullscreen></iframe>
                            <p>Shark Bytes consist of "little crunchy bits of wisdom". Like the immensely popular TED talks, Shark Bytes aim to inform, inspire, surprise, and delight as presenters share their personal perspective on a topic in under 5 minutes.</p>
                            -->

                        </div>
                    </div>
                    
                </article>

                <!-- About the author -->
                
                <!-- /About the author -->

                <!-- Comments -->
                
                <!-- /Comments -->

                <!-- Leave a Comment -->
                
                <!-- /Leave a Comment -->

            </section>
            <!-- Content -->

            <hr class="page-divider transparent visible-xs"/>

            <!-- Sidebar -->
            <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <h4 class="widget-title">A Word of Thanks</h4>
                        <p>SharkFest'18 US celebrated the 20th anniversary of the Wireshark project and proved a blazing success thanks to the generous, giving community in attendance. Particular thanks to Gerald Combs and his merry band of core developers for inspiring the many first-time participants by opening with a keynote that illuminated the 20-year history of the project, to Laura Chappell for creating a truly crowd-pleasing Packet Palooza Pub Quiz, to Usman Muzaffar for his thoughtful keynote that explained why Wireshark can be considered the microscope of the 21st century, to instructors who selflessly donated time and wisdom to educate and mentor participants, to sponsors who so generously provided the resources to make the conference possible, to the Computer History Museum Events team for their expert guidance, to tireless caterers who served up varied and delicious daily fare, to Albert our AV Angel, to a staff and volunteer crew who once again went overboard in making the conference as smooth and pleasant an experience as possible for attendees, and to Aaron and Lainey for delighting us all with their golden intelligence!</p>
                    </div>
                </div>

                    
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <div class="sponsor-list">
                            <div class="sponsor-imgs">
                                    <h4>Host Sponsors</h4>
                                    <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>
                                    <a href="http://www.wiresharktraining.com/" target="_blank"><img class="sponsors" src="img/sponsors/wiresharku-white.png"></a>

                                    <h4>Angel Shark Sponsors</h4>
                                    <a href="http://www.cloudflare.com/" target="_blank"><img class="sponsors" src="img/sponsors/cloudflare-white.png"></a>
                                    <a href="http://www.dual-comm.com/" target="_blank"><img class="sponsors" src="img/sponsors/dualcomm-white.png"></a>
                                     
                                      <a href="http://fmad.io/" target="_blank"><img class="sponsors" src="img/sponsors/fmadio.jpg"></a>
                                    <a href="https://www.garlandtechnology.com/" target="_blank"><img class="sponsors" src="img/sponsors/garland-white.png"></a>
                                    <a href="http://www.metamako.com/" target="_blank"><img class="sponsors" src="img/sponsors/metamako-white.png"></a>
                                    <a href="http://www.napatech.com/" target="_blank"><img class="sponsors" src="img/sponsors/napatech-white.png"></a>
                                    <a href="http://www.profitap.com/" target="_blank"><img class="sponsors" src="img/sponsors/profitap-white.png"></a>


                                    <h4>Wireshark Packetpalooza Sponsor</h4>
                                    
                                     <a href="https://www.endace.com/" target="_blank"><img class="sponsors" src="img/sponsors/endace_big.png"></a>

                                
                                    <h4>Honorary Sponsors</h4>
                                    <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                            </div>
                                                        
                        </div>
                    </div>
                </div>
            </aside>
            </div>
            </section>
            <!-- /PAGE BLOG -->

        </div>
        <!-- /Content area -->
    </section>
</div>
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>



