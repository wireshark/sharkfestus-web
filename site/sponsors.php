<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="text-centered">
    <section id="main-content">
        <div class="container">
        	<div class="row sponsors-page about-container">
                <div class="col-sm-8 col-xs-12 background-blue">
                    <h2>Host Sponsor</h2>
                    <div class="col-lg-12 sponsor-margin-top">
                        <a target="_blank" href="https://www.sysdig.com">
                            <img src="img/sponsors/sysdig-white.png" />
                        </a>
                        <p><strong>Sysdig</strong> is driving the standard for cloud and container security. The company pioneered cloud-native runtime threat detection and response by creating Falco and Sysdig as open source standards and key building blocks of the Sysdig platform. With the platform, teams can find and prioritize software vulnerabilities, detect and respond to threats, and manage cloud configurations, permissions and compliance. From containers and Kubernetes to cloud services, teams get a single view of risk from source to run, with no blind spots, no guesswork, no black boxes. The largest and most innovative companies around the world rely on Sysdig.

                        </p>
                    </div>
                    <h2>Angel Shark Sponsors</h2>
                    <div class="col-lg-12 sponsor-margin-top">
                        <a target="_blank" href="https://allegro-packets.com/en/ ">
                            <img src="img/sponsors/allegro-white.png" />
                        </a>
                        <p>With great support for Wireshark packet analysis, <strong>Allegro Network Multimeter</strong> enables network firefighters to conveniently identify and extract packets of interest from both Live network traffic and large pcap files. The leading-edge solutions developed by Allegro Packets, show packet-based network traffic statistics in real-time, intuitively presented according to the L2-L7 OSI structure. Network performance issues can easily be detected, by navigating the clearly structured dashboards and statistics pages. With the greatest ease, any traffic, protocol or connection can be selectively captured for last Mile analysis with Wireshark.</p>

                        <p>The product range extends from portable, great affordable adhoc analysis appliances to 4U rack-mount solutions, from 1G to 200G. German based Allegro Packets is the global pioneer for fast network analysis. Its customers include network specialists from many organizations, including large scale Enterprises and IT/VoIP service providers, across a wide range of market sectors around the world.
                        </p>
                        <!-- <a target="_blank" href="https://www.comworth.com.sg/">
                            <img src="img/sponsors/comworth-white.png" />
                        </a> -->
                        <!-- <p><strong>Comworth</strong> develops and builds High Performance, Scalable, Lossless Packet Recording and Capture Appliances. Adopting State of Art technologies, ComWorth continues to test boundaries, driving higher performances. The Sirius NDR (Network Data Recorder) currently is capable of 100G capture and recording of up to 20 Petabyte of Data Storage. Together with other Special Purpose Appliances: “Procyon” with Intuitive GUI (for Ease of Use and Management) it support Intelligent Packet Filtering, Packet Aggregation and Load Balancing.</p>

                        <p>With our expertise, we can integrate your solution into Enterprise networks, enabling network visibility and monitoring. Learn more @ <a target="_blank" href="https://www.comworth.com.sg/">www.comworth.com.sg</a> / <a target="_blank" href="https://www.comworth.co.jp/">www.comworth.co.jp</a></p>
                        </p> -->
                        <a target="_blank" href="https://www.endace.com/">
                            <img src="img/sponsors/endace_big.png" />
                        </a>
                        <p><strong>Endace’s</strong> scalable, open packet-capture platform provides network-wide packet recording at speeds of 100Gbps and beyond. With fast, centralized search, powerful data-mining and the ability to integrate with, and host, security and performance monitoring solutions the open EndaceProbe platform puts definitive packet evidence right at your fingertips. Find out why Cisco, Palo Alto Networks, IBM, Splunk, Gigamon, Darktrace and many others partner with Endace to provide 100% accurate Network Recording at www.endace.com or follow Endace on Twitter, LinkedIn and Youtube.
                        </p>
                        <a target="_blank" href="https://www.profitap.com/">
                            <img src="img/sponsors/profitap-white.png">
                        </a>
                        <p><strong>Profitap</strong> develops and manufactures hard- and software solutions that drive network visibility and analytics on all traffic across physical and virtual infrastructures. These network visibility solutions are designed with the security, forensics, deep packet capture, and network & application performance monitoring sectors in mind.</p>

                        <p>Profitap network solutions help eliminate network downtime, add security to existing and new networks worldwide, assist in lawful interception applications and reduce network complexity. Profitap’s network monitoring tools are highly performant, secure, and user-friendly. They provide complete visibility and access to your network 24/7.</p>

                        <p>As experts in the field, Profitap products set new standards in an industry where the definition of excellence is constantly being challenged. With over 1,000 clients from 55 countries, many of which are among Fortune 500 companies, Profitap has a strong track record of trust and reliability. 
                        </p>
                        <a target="_blank" rel="noopenner noreferrer" href="https://www.progress.com/">
                            <img src="img/sponsors/progress-flowmon-white.png" />
                        </a>
                        <p>Dedicated to propelling business forward in a technology-driven world, <strong><a href="https://www.progress.com/" rel="noopenner noreferrer" target="_blank">Progress</a></strong> (Nasdaq: PRGS) helps businesses drive faster cycles of innovation, fuel momentum and accelerate their path to success. As the trusted provider of the best products to develop, deploy and manage high-impact applications, Progress enables customers to develop the applications and experiences they need, deploy where and how they want and manage it all safely and securely. Hundreds of thousands of enterprises, including 1,700 software companies and 3.5 million developers, depend on Progress to achieve their goals—with confidence. Learn more at www.progress.com, and follow us on <a href="https://www.linkedin.com/company/progress-software/" target="_blank" rel="noopenner noreferrer">LinkedIn</a>, <a href="https://www.youtube.com/user/ProgressSW" target="_blank" rel="noopenner noreferrer">YouTube</a>, <a href="https://twitter.com/progresssw" target="_blank" rel="noopenner noreferrer">Twitter</a>, <a href="https://www.facebook.com/progresssw" target="_blank" rel="noopenner noreferrer">Facebook</a> and <a href="https://www.instagram.com/progress_sw_" target="_blank" rel="noopenner noreferrer">Instagram</a>.
                        </p>
                    </div>
                    <h2>Bull Shark esPCAPe Challenge Sponsor</h2>
                    <div class="col-lg-12 sponsor-margin-top">
                        <a target="_blank" href="https://www.qacafe.com/">
                            <img src="img/sponsors/qa-cafe.png" />
                        </a>
                        <p><strong>QA Cafe</strong> is a dynamic software company composed of experts in networking, consumer electronics, and security developing industry-leading network device test solutions and network analysis tools. Their leading products include CloudShark, a secure solution that enables network and security teams to organize, analyze, and collaborate on packet captures.</p>
                    </div>
                    <h2>Welcome Dinner Sponsor</h2>
                    <div class="col-lg-12 sponsor-margin-top">
                        <a target="_blank" href="https://www.riverbed.com/">
                            <img src="img/sponsors/riverbed-new.png" />
                        </a>
                        <p><strong>Riverbed is the only company with the collective richness of telemetry from network to app to end user, that illuminates and then accelerates every interaction, so organizations can deliver a seamless digital experience and drive enterprise performance. Riverbed offers two industry-leading portfolios: Alluvio by Riverbed, a differentiated Unified Observability portfolio that unifies data, insights, and actions across IT, so customers can deliver seamless, secure digital experiences; and Riverbed Acceleration, providing fast, agile, secure acceleration of any app, over any network, to users anywhere. Together with our thousands of partners, and market-leading customers globally –including 95% of the FORTUNE 100–, we empower every click, every digital experience. Riverbed. Empower the Experience. Learn more at <a target="_blank" rel="noopenner noreferrer" href="riverbed.com">riverbed.com</a>.</p>
                    </div>
                    <h2>Honorary Sponsor</h2>
                    <div class="col-lg-12 sponsor-padding-bottom">
                        <a target="_blank" href="https://www.networkdatapedia.com/">
                            <img src="img/sponsors/networkdatapedia.png" />
                        </a>
                        <p>With <strong>NetworkDataPedia</strong>, formerly known as LoveMyTool, network analysts can access the latest information, tips, and tricks about network monitoring, analysis, security, performance, and management. Our primary goal, since LoveMyTool, has been to empower network technologies to succeed in what they do.  Secondly, we strive to assist businesses of all sizes in identifying issues and solutions to their technology.</p>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 column-about">
                    <div class="col-xs-12 background-blue front-column1 about-cell">
                        <img src="img/sf21v/sponsorpage.jpg">
                        <div class="grid-cell">
                            <p>View the SharkFest’22 US Sponsorship Form to read about the different sponsorship levels available. If your company would like to sponsor SharkFest, please contact <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a>.</p>
                        </div>
                        <div class="about-button">
                            <a class="" href="assets/sponsor-doc22.pdf"><h4>Sponsorship Form</h4></a>
                        </div>
                    </div>
                    <div class="retro-vid-wrapper background-blue col-xs-12 about-cell">
                        <div class="grid-cell">
                          <div class="sponsor-list">
                            <div class="sponsor-imgs">
                                <h4>Sponsor Interviews</h4>
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqOTlBn_W0x3tFstX6A0BY_6" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>