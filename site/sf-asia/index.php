<?php include "header.php"; ?>
    <div class="container grid-container">
        <div class="row grid-row">
            <div class="col-lg-10 col-centered no-padding">
                <div class="col-xs-12 col-lg-12 fullcolumn col-centered front-background">
                    <!--<a class="textdec-none" href="#"> <!-- Put register link there -->
                        <div class="box1text">
                            <div class="box1background">
                                
                                <div id="clockdiv">
                                  <div>
                                    <span class="days"></span>
                                    <div class="smalltext">Days</div>
                                  </div>
                                  <div>
                                    <span class="hours"></span>
                                    <div class="smalltext">Hours</div>
                                  </div>
                                  <div>
                                    <span class="minutes"></span>
                                    <div class="smalltext">Minutes</div>
                                  </div>
                                  <div>
                                    <span class="seconds"></span>
                                    <div class="smalltext">Seconds</div>
                                  </div>
                                </div>
                            </div>
                        </div>   
                    <!--</a>-->
                </div>
            </div>
            <div class="col-lg-10 col-centered no-padding">
                <!--
                <div class="col-xs-6 col-lg-6 fullcolumn blue-background">
                    <a class="textdec-none" href="retrospective.php"> 
                                <h3>SharkFest Retrospective</h3>
                                <p>Travel back in time through the history of SharkFest™ Europe conferences</p>
                                <a href="retrospective.php">
                                    <button type="button" class="btn btn-primary btn-med">TRAVEL BACK NOW</button>
                                </a>
     
                    </a>
                </div> -->
                <div class="col-xs-6 col-lg-6 fullcolumn blue-background1">
                    <a class="textdec-none" href="register.php"> <!-- Put register link there -->
                                <h3>SharkFest’17 Asia Early Bird Pre-Registration is Open! </h3>
                                <p>Pre-Register now to receive discounts for SharkFest and Laura Chappell’s “Troubleshooting with Wireshark” course and reserve one of a limited number of conference seats.</p>
                                <a href="register.php">
                                    <button type="button" class="btn btn-primary btn-med">REGISTER</button>
                                </a>
     
                    </a>
                </div>
                <div class="col-xs-6 col-lg-6 fullcolumn blue-background2">
                    <a class="textdec-none" href="register.php"> <!-- Put register link there -->
                                <h3>Laura Chappell's Troubleshooting with Wireshark Course (April 7th - 8th)</h3>
                                <p>Join the founder of Wireshark University for a 2-day “Troubleshooting with Wireshark” course preceding the start of SharkFest Asia.</p>
                                <a href="register.php">
                                    <button type="button" class="btn btn-primary btn-med">REGISTER</button>
                                </a>
     
                    </a>
                </div>
            </div>
            <div class="col-lg-10 col-centered no-padding">
                <div class="col-xs-6 col-lg-6 fullcolumn sponsor-background">
                    <a class="textdec-none" href="sponsors.php"> <!-- Put register link there -->
                        
                                <h2>Become a SharkFest'17 Asia Sponsor</h2>
                                <a href="sponsors.php">
                                    <button type="button" class="btn btn-primary btn-med">LEARN MORE</button>
                                </a>

                    </a>
                </div>
                <div class="col-xs-6 col-lg-6 fullcolumn about-background">
                    <a class="textdec-none" href="speakapp.php">
                                <h3>Call for SharkFest’17 Asia Session Submissions is On!</h3>
                                <p>SharkFest’17 Asia session submissions are now being accepted.  Session content guidelines and the topic submission form are available here.</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php include "footer.php"; ?>