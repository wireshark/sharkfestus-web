<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/jason-williams.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>  Jason Williams,  Security Researcher, OISF / Proofpoint</h2>
		<p>
		Jason is a security researcher with global enterprise experience in detecting, hunting and remediating threats with open source technologies. Primarily focusing on network communications, Jason has written thousands of commercial and community Suricata rules for Emerging Threats to help defenders protect their networks.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>