<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/scott-reid.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>  Scott Reid, Technical Specialist, VGRIT</h2>
		<p>
		Scott Reid is a member of the Problem Analysis Group of the IT department at a large health care organization in Sweden and has been using Wireshark and its predecessor, Ethereal, for over ten years. In his position as Technical Specialist, he uses Wireshark daily for a range of functions - from benchmarking to development and preproduction testing to troubleshooting large-scale IT systems. An extensive variety of protocols and technologies are examined and evaluated regularly, and Wireshark is often used while giving presentations to demonstrate the details and particulars of a case.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>