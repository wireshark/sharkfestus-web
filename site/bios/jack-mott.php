<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/jack-mott.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>  Jack Mott,  Security Researcher, OISF / Proofpoint</h2>
		<p>
		Jack Mott is a security researcher who focuses on open source solutions to detect, track and hunt malware and malicious activity. He has been a signature writer for the Emerging Threats team for several years, producing community/premium Suricata signatures to help protect networks worldwide. Jack is a strong believer in the open source mission as well as helping people and organizations solve security issues with open source solutions.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>