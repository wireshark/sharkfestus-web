<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/usman.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Usman Muzaffar - VP of Engineering, Cloudflare</h2>
		<p>
		Usman Muzaffar is Head of Engineering for Cloudflare, where he leads the development team that is making the internet safer and faster for 7 million internet properties. Before Cloudflare, Usman was co-founder and CTO of Selligy (acquired by Veeva Systems). In the past twenty years he has held leadership positions in software engineering, product management, and sales at big and small companies, and has worked on a wide range of projects including graphics drivers, mobile apps for sales people, and distributed build systems.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>