<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/john-pittle.jpg">
	</div>
	<div class="col-sm-9">
		<h2>John Pittle, Distinguished Performance Consultant, Riverbed Technology, Inc.</h2>
		<p>
		As a Performance Management Strategist, John helps his customers develop and execute strategies for integrating Performance Management as an IT discipline across the organization.  He has been actively focused on Performance Engineering and Analysis for networks, systems, and applications since the early 90s;  performance troubleshooting is his passion and joy. His packet analysis toolbox includes Wireshark (of course), as well as NetShark, AppResponse, Packet Analyzer, and Transaction Analyzer.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>