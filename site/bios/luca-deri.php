<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/luca-deri.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Luca Deri - Network developer, ntop founder</h2>
		<p>
		Luca Deri is the leader of the ntop project (<a href="www.ntop.org">www.ntop.org</a>), aimed at developing an open-source monitoring platform for high-speed traffic analysis. He worked for University College of London and IBM Research, prior to receiving his PhD at the University of Berne with a thesis about software components for traffic monitoring applications.  Well known in the open-source and Linux community, he currently shares his time between the ntop project and the University of Pisa where he has been appointed as lecturer at the CS department.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>