<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/brian-greunke.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Brian Greunke, Security Advocate, BruteForce</h2>
		<p>
		Brian is a security advocate, technical leader, and full-stack developer with nearly 10 years of experience guiding technical teams in fast-paced, high-stakes environments. He enjoys exploring cutting edge technologies and using them to build security, decentralization, and privacy into the software and systems we use every day.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>