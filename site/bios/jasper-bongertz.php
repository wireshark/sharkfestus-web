<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/jasper.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Head of Incident Response @ G Data Advanced Analytics</h2>
		<p>
		Jasper Bongertz is a network security expert with focus on network forensics and incident response at G DATA Advanced Analytics in Bochum, Germany. He started working freelance in 1992 while he was studying computer science at the Technical University of Aachen. In 2009, Jasper became a Senior Consultant and trainer for Fast Lane, where he created a large training portfolio with a special focus on Wireshark and network hacking. In 2013, he joined Airbus CyberSecurity, focusing on IT security, Incident Response and Network Forensics, before moving on and joining G DATA Advanced Analytics in August 2019 as the Principal Network Security Specialist and Head of Incident Response. Jasper is the creator of the packet analysis tool “TraceWrangler”, which can be used to convert, edit and sanitize PCAP files. His blog regarding packet capture, network analysis, network forensics and general security topics can be found at blog.packet-foo.com.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>