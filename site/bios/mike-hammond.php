<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/mike-hammond.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2> Mike Hammond, Senior Technical Trainer, Global Knowledge</h2>
		<p>
		Mike is a veteran technology trainer in the IT Professional space, delivering engaging training sessions on Windows, UNIX, and networking topics. A Wireshark Certified Network Analyst and Wireshark Certified Trainer, Mike delivers 5-day “Troubleshooting TCP/IP Networks with Wireshark” classes through Global Knowledge - the industry's leading IT and business skills training provider. Mike also is an avid chess player and strategy board game fan, a proud husband, and an exhausted father of two swimmers and a gymnast. He types 50 words per minute using the Dvorak keyboard layout, as all thinking people should.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>