<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/chris-sanders.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Chris Sanders, Founder @ Applied Network Defense and Rural Technology Fund</h2>
		<p>
		Chris Sanders is an information security author, trainer, and researcher originally from Mayfield, KY. He is the founder of Applied Network Defense, a company focused on delivering high quality, accessible information security training.</p>

		<p>In previous roles, Chris worked with the US Department of Defense, InGuardians, and Mandiant to build security operation centers and train practitioners focused on defending defense, government, and Fortune 500 networks.</p>

		<p>Chris is also the founder and director of the Rural Technology Fund, a non-profit that donates scholarships and equipment to public schools to further technical education in rural and high poverty areas. In 2016, the RTF put computer science education resources into the hands of over 10,000 students.</p>
 
		<p>Chris has authored several books and articles, including the international best seller “Practical Packet Analysis” from No Starch Press, currently in its third edition and in seven languages, and “Applied Network Security Monitoring” from Syngress. His current research focus is the intersection of cyber defense and cognitive psychology to enhance the field of security investigative technique through a better understanding of the human thought and learning processes.</p>
		<p>Chris blogs at <a href="http://www.chrissanders.org">http://www.chrissanders.org</a>. You can learn more about Applied Network Defense athttp://www.appliednetworkdefense.com and the RTF at ><a href="http://www.ruraltechfund.org">http://www.ruraltechfund.org</a>.  </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>