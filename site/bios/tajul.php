<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/tajul.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Tajul Azhar Mohd Tajul Ariffin, Lecturer, Polytecnic Mersing, Johor</h2>
		<p>
		Network visibility has become a niche in the daily work of Tajul Azhar Mohd Tajul Ariffin as an InfoSec lecturer and researcher. As a speaker at various InfoSec events in Malaysia, he is inspired by the phrase “packets never lie”, quoted at many Wireshark gatherings. He ventures into security threat modelling by introducing Elastic Stack as a medium to analyze all traffic in his Cyber Range Lab. As a Blue Team Specialist, he builds visibility with Wireshark and Elastic Stack as a shortcut for students to understand networking concepts in the real world.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>