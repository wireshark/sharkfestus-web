<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/nalini-elkins.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>Nalini Elkins, CEO, Inside Products</h2>
		<p>
		Nalini is the CEO and Founder of Inside Products.  She has also been the CEO / CTO of 3 other high-tech companies.  Her area of technical expertise is in performance metrics for wide area networking.  Her latest standard is RFC8250: Performance Metrics and Diagnostics for IPv6.  She is a commercial software product developer whose products have been marketed by IBM as well as other major software vendors. She received the A.A. Michelson award (given once a year) in 2014 from the Computer Measurement Group for her contributions to the industry.   Her IP Problem Finder products are expert system analytic tools incorporating Wireshark output.  She loves Wireshark!   Packets don't lie.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>