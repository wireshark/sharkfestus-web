<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/sake.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Sake Blok - Packet Analyst, SYN-bit</h2>
		<p>
		Sake has been analyzing packets since the end of the last century. Over the years, he’s uncovered device bugs from multiple vendors and presented his findings to the vendors to fix issues. He’s also discovered many misconfiguration on customer networks that have led to functional or performance problems with applications running over the network and provided resolutions through reports presented to his customers. In 2009, Sake started the company SYN-bit to provide Network Analysis services to enterprises across Europe. In the course of his work, Sake started developing extra functionality for Wireshark that he missed in his day-to-day job. He also enhanced multiple protocol dissectors to suit his analysis needs. In 2007 he was asked by Gerald to join the Wireshark Core Development team.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>