
<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/anthony-efantis.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Anthony Efantis, Principal Network Engineer at Sealing Teach</h2>
		<p>Tony E. holds many industry certifications including: CCIE# 64908, GNFA, PCNSA & SEC+. He maintains a blog @ <a href="https://showipintbri.github.io" target="_blank">https://showipintbri.github.io</a> and is the co-host on the Network Collective podcast and live stream: <a href="https://networkcollective.com" target="_blank">https://networkcollective.com</a>. He enjoys participating in packet hacking challenges and CTF's at security conferences.
		</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>