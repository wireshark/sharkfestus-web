<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/brad-palm.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Brad Palm, Security Engineer, BruteForce</h2>
		<p>
		Brad is a seasoned problem solver and convergent thinker who enjoys the challenges of merging the physical and virtual worlds. As an analyst working in the cybersecurity and network efficiency domains, he focuses on network capture and analysis for enterprise networks, testing emerging technologies and devices for customers, and utilizing captured traffic to influence developmental test scenarios. His professional interests include tinkering with open source virtualization/container/provisioning technologies, learning about software defined and mesh networking, and increasing the usability of secure information systems to facilitate wide scale adoption.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>