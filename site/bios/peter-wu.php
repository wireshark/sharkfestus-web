<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/peter-wu.PNG">
	</div>
	<div class="col-sm-9">
		<h2>Peter Wu - Wireshark Core Developer</h2>
		<p>
		Peter Wu is a Masters student in Information Security at the Eindhoven University of Technology, and contributor to many open source projects. His contribution to Wireshark started in 2013 with SSL decryption fixes. As Software Developer at Code Yellow, he helped in developing a VoIP product. </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>