<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/ryan-richter.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Ryan Richter, Troubleshooter & Founder, TARCA</h2>
		<p>Ryan solves problems related to security, infrastructure, networking, applications, and organizations. He is an advocate of the scientific method and uses a variety of programming languages, network analysis tools, and Linux utilities in his daily work.
</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>