<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/megumi-takeshita.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Megumi Takeshita, Packet Otaku and Owner, Ikeriri Network Service</h2>
		<p>
		Megumi Takeshita, known as Packet Otaku, runs a packet analysis company after having worked as a network analyst at BayNetworks and Nortel Networks for many years. Ikeriri Network Service is a reseller of Riverbed, Metageek, Dualcomm, Profitap and other packet capture products in Japan. Megumi has written more than 10 books about packet analysis and deep inspection using Wireshark in Japanese and has also attended every SharkFest since they began in 2008.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>