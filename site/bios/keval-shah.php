<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/keval-shah.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Keval Shah, Solution Sales Engineer, Gigamon</h2>
		<p>
		 Keval has 12+ years of technical consulting and solution engineering experience in the field of enterprise networking & security, He has enjoyed architecting and transforming businesses.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>