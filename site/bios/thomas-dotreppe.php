<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/thomas-dotreppe.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Thomas d’Otreppe, AirCrack-ng Author</h2>
		<p>
		Thomas D’Otreppe is a wireless security researcher and author of Aircrack-ng, the most popular and complete suite of tools for WiFi network security assessments. He also created OpenWIPS-ng, an open source Wireless Intrusion Prevention System. Thomas is a contributor to the WiFi stack and toolset in Backtrack Linux, which has now become Kali Linux, the de facto top choice Linux distribution for penetration testing and vulnerability assessment across multiple technology domains. He is also known as an author of a pro-active wireless security course which has been delivered to large numbers of IT Security professionals worldwide. Thomas speaks and teaches in the Americas and Europe and is a well-known speaker at DefCon, BlackHat, DerbyCon, SharkFest, Mundo Hacker Day, BruCON and other venues.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>