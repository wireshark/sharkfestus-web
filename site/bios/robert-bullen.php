<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/robert-bullen.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Robert Bullen, Cloud Services Engineer, Blue Cross Blue Shield of MN</h2>
		<p>
		Robert Bullen has been in the packet analysis space in one form or another for most of his 20-year career as both a developer and user. For the last five years he’s been an application performance engineer for two Minnesota-based enterprises where he has relied heavily on the Wireshark tool suite, and during which time he contributed enhancements to the Wireshark code base. Robert has attended more Sharkfests than he hasn’t. This will be his third time as a Sharkfest speaker.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>