<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/peter-manev.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Peter Manev, QA/Trainer, Open Information Security Foundation</h2>
		<p>
		Peter has been involved with Suricata IDS/IPS/NSM from its very early days in 2009 as QA lead, currently a Suricata executive council member. 
        Peter has 15 years experience in the IT industry, including enterprise and government level IT security practice. As an adamant admirer and 
        explorer of innovative open source security software he is also one of the creators of SELKS - an open source threat detection security distro. He 
        is also one of the founders of Stamus Networks, a company providing security solutions based on Suricata.</p>
	</div>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>