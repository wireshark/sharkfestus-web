<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/josh-clark.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>Josh Clark, Distributed Performance Engineer at Huntington National Bank </h2>
		<p>
		Josh is a Distributed Performance Engineer at Huntington National Bank, which means he gets to have fun with all the weirdest problems in the most complex systems the bank implements. Wireshark is his tool of choice to figure things out.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>