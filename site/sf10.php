<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <div class="sf10-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'10 Retrospective</h1>
            <p>June 14th - 17th, 2010<br>Stanford University | Stanford, California</p>
        </div>
    </div>
    <section id="main-content">
    <!-- Content area -->
            <div class="content-area retrospective-page">

                <!-- PAGE BLOG -->
                <section class="page-section with-sidebar sidebar-right">
                <div class="container">
                <div class="row">

                <!-- Content -->
                <section id="content" class="content col-sm-7 col-md-8">

                    <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                        <div class="post-media">
                        </div>
                        <div class="post-header">
                            <h3 class="post-title"></h3>
                            <div class="post-meta">
                                
                                
                            </div>
                        </div>
                        <div class="post-body">
                            <div class="post-excerpt">

                                <h3 class="post-title2"><strong>Post Fest Reviews and Press</strong></h3>
                                <p><a href="http://www.lovemytool.com/blog/sharkfest/">Love My Tool</a></p>

                                <h3 class="post-title2"><strong>Pictures</strong></h3>
                                <p>Joke Snelders<a href="http://www.facebook.com/album.php?aid=182664&id=51502216870"> Photos</a><br>
                               </p>

                                <h3 class="post-title2"><strong>Keynote Presentation</strong></h3>
                                <p><a href="http://sharkfest.wireshark.org/sharkfest.10/Key-2_Saal%20Reading%20the%20Traffic%20-%20Learning%20Behavior%20from%20Watching%20Bits.pdf" target="_blank">Key-1 (Saal) Reading the Traffic: Learning Behavior from Watching Bits</a></p>
                                
                                <h3 style="" class="post-title2"><strong>Basic Track Presentations</strong></h3>
                                <ul style="list-style:none;">
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-1_Bardwell-Secrets_Revealed-PART_1.pdf" target="_blank">B-1 (Bardwell) 802.11 Secrets Revealed – Part 1: RF Signal Propagation</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-2_Tompkins%20Analyzing%20TCPIP%20Networks%20with%20Wireshark.pdf" target="_blank">B-2 (Tompkins) Analyzing TCPIP Networks with Wireshark</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-3_Patterson%20Where%20NetFlow%20and%20Packet%20Capture%20Complement%20Each%20Other.ppt" target="_blank">B-3 (Patterson) Where NetFlow and Packet Capture Complement Each Other</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-4_Carrell_Network_Access_Security-Broken_Now_What.pdf" target="_blank">B-4 (Carrell) Network Access Security – It's Broken, Now What?</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-5_Parsons%20HANDS-ON%20LAB%20-%20WLAN%20Analysis%20with%20Wireshark%20&%20AirPcap.pdf" target="_blank">B-5 (Parsons) HANDS-ON LAB: WLAN Analysis with Wireshark &amp; AirPcap<br />
                Exercises</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-5_Parsons%20HANDS-ON%20LAB%20-%20WLAN%20Analysis%20with%20Wireshark%20&%20AirPcap%20Exercises.pdf" target="_blank">B-5 (Parsons) Exercise Files</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-6_Leutert%20Discovering%20IPv6%20with%20Wireshark.pdf" target="_blank">B-6 (Leutert) Discovering IPv6 with Wireshark</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-7_Battaglia%20TAPS%20Demystified.ppt" target="_blank">B-7 (Battaglia) TAPS Demystified</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-8_Chappell%2010%20Cool%20Things%20You%20Should%20Know%20How%20To%20Do%20with%20Wireshark.pdf" target="_blank">B-8 (Chappell) 10 Cool Things You Should Know How To Do with Wireshark</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-9_Bardwell-Secrets_Revealed-PART_2.pdf" target="_blank">B-9 (Bardwell) 802.11 Secrets Revealed – Part 2: Bit Transmission &amp; Channel Capacity</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-10_Bongertz%20Wireshark%20vs.%20the%20Cloud.pdf" target="_blank">B-10 (Bongertz) Wireshark vs. &quot;the Cloud&quot;</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/B-11_Bae%20Basic%20TCPIP%20Analysis.ppt" target="_blank">B-11 (Bae) Basic TCP/IP Analysis</a></li>
            </ul>
                  
              
                      <h3 class="post-title2"><strong>Advanced Track Presentations</strong></h3>
                      
                <ul style="list-style:none;">
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-1_Bae%20Stump%20the%20Expert!%20Packet%20Trace%20Whispering.ppt" target="_blank">A-1 (Bae) Stump the Expert! Packet Trace Whispering</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-2_Belcher%20Advanced%20Threat%20Intelligence%20and%20Session%20Analysis.pptx" target="_blank">A-2 (Belcher) Advanced Threat Intelligence and Session Analysis</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-3_Walberg%20VoIP%20Troubleshooting.ppt" target="_blank">A-3 (Walberg) VoIP Troubleshooting</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-4_Desai%20Visibility%20into%20the%20DMZ%20with%20Wireshark.pdf" target="_blank">A-4 (Desai) Visibility into the DMZ with Wireshark</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-5_LDegioanni%20The%20Shark%20Distributed%20Monitoring%20System.ppt" target="_blank">A-5 (LDegioanni) The Shark Distributed Monitoring System</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-6_Blok%20HANDS-ON%20LAB%20-%20Using%20Wireshark%20Command%20Line%20Tools%20and%20Scripting.zip">A-6 (Blok) HANDS-ON LAB: Using Wireshark Command Line Tools and Scripting</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-6_Blok%20Lab%20Guide.pdf" target="_blank">A-6 (Blok) Exercise Files</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-7_John%20He%20SPAN%20Out%20of%20the%20Box.pdf" target="_blank">A-7 (John) He SPAN Out of the Box</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-8_Bae%20CASE%20STUDY%20-%20Wireshark%20in%20the%20Large%20Enterprise.ppt" target="_blank">A-8 (Bae) CASE STUDY: Wireshark in the Large Enterprise</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-9_Donnelly%20Operating%20a%20Flexible%20Network%20Montioring%20Infrastructure.pptx" target="_blank">A-9 (Donnelly) Operating a Flexible Network Montioring Infrastructure</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-10_Leutert%20WLAN%20802.11n%20MIMO%20Analysis.pdf" target="_blank">A-10 (Leutert) WLAN 802.11n MIMO Analysis</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/A-11_Chappell%20HANDS-ON%20LAB-%20Death%20of%20a%20Network%20-%20Identify%20the%20Hidden%20Cause%20of%20Crappy%20Netwokr%20Performance.pdf" target="_blank">A-11 (Chappell) HANDS-ON LAB- Death of a Network: Identify the Hidden<br />
                Cause of Crappy Network Performance</a></li>
            </ul>
                      <h3 class="post-title2"><strong>Developer Track Presentations</strong></h3>
                      
                <ul style="list-style:none;">
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-1_Combs%20Basic%20Dissector%20Development.pdf" target="_blank">D-1 (Combs) Basic Dissector Development</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-2_Chung%20A%20Variety%20of%20Ways%20to%20Capture%20and%20Analyze%20Packets.pdf" target="_blank">D-2 (Chung) A Variety of Ways to Capture and Analyze Packets</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-3_Woodings%20Visualizing%20RF.ppt" target="_blank">D-3 (Woodings) Visualizing RF</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-4_Leong%20Power-Boosting%20Your%20Tools%20by%20Adding%20Meta-Data%20Tags%20to%20Packets.zip">D-4 (Leong) Power-Boosting Your Tools by Adding Meta-Data Tags to Packets</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-5_Kershaw%20Scripting%20Wireless%20Utilities.ppt" target="_blank">D-5 (Kersha)w Scripting Wireless Utilities</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-6_d'Otreppe%20Wireless%20Security.ppt" target="_blank">D-6 (d'Otreppe) Wireless Security</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-7_Combs%20Wireshark%20and%20Lua.zip">D-7 (Combs) Wireshark and Lua</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-7_Lyon%20Scripting%20and%20Extending%20Nmap%20and%20Wireshark%20with%20Lua.pdf" target="_blank">D-7 (Lyon) Scripting and Extending Nmap and Wireshark with Lua</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-8_Combs%20Advanced%20Dissector%20Development.pdf" target="_blank">D-8 (Combs) Advanced Dissector Developmen</a>t</li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-9_Sanders%20Using%20Specialized%20Network%20Adapters%20to%20Improve%20A%20ccuracy%20of%20Network%20Analysis%20in%20Highly-Utlized%20Networks.pdf" target="_blank">D-9 (Sanders) Using Specialized Network Adapters to Improve Accuracy of<br />
                Network Analysis in Highly-Utlized Networks</a></li>
                <li><a href="http://sharkfest.wireshark.org/sharkfest.10/D-10_LDegioanni%20To%20the%20Terabyte%20and%20Beyond.ppt" target="_blank">D-10 (Degioanni) To the Terabyte and Beyond</a></li>
            </ul>
                                

                            </div>
                        </div>
                        
                    </article>

                    <!-- About the author -->
                    
                    <!-- /About the author -->

                    <!-- Comments -->
                    
                    <!-- /Comments -->

                    <!-- Leave a Comment -->
                    
                    <!-- /Leave a Comment -->

                </section>
                <!-- Content -->

                <hr class="page-divider transparent visible-xs"/>

                <!-- Sidebar -->

                <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
                  <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <h4 class="widget-title">A Word of Thanks</h4>
                        <p>Another Sharkfest has come and gone, and we thank each participant, sponsor, presenter, keynote speaker, coordinator, caterer, staff and general support person for making this a successful, richly educational event once again. All session presentations are available from this page, and recordings of many sessions can be found at <a href="www.lovemytool.com">www.lovemytool.com</a>.</p>
                    </div>
                  </div>
                    

                </div>
                </div>
                </section>
                <!-- /PAGE BLOG -->

            </div>
            <!-- /Content area -->
</aside>
</div>
</div>
</section>
</div>
</section>
</div>
            <!-- FOOTER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
    