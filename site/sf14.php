<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf14-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'14 Retrospective</h1>
            <p>June 16th - 20th, 2014<br>Dominican University of California | San Rafael, California</p>
        </div>
    </div>
    <section id="main-content">
            <!-- Content area -->
            <div class="content-area retrospective-page">

                <!-- PAGE BLOG -->
                <section class="page-section with-sidebar sidebar-right">
                <div class="container">
                <div class="row post-excerpt">

                <!-- Content -->
                <section id="content" class="content col-sm-7 col-md-8">

                    <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                        <div class="post-media">
                        </div>
                        <div class="post-header">
                            <h3 class="post-title"></h3>
                            <div class="post-meta">                
                            </div>
                        </div>
                        <div class="post-body">
                            <div class="post-excerpt">
                                <h3 class="post-title2"><strong>Blogs</strong></h3>
                                <p><a href="http://blog.packet-foo.com/2014/06/sharkfest-2014-recap/">SharkFest 2014 Recap</a> by Jasper Bongertz</p>

                                <h3 class="post-title2"><strong>Packet Challenge</strong></h3>
                                <p>The SharkFest 2014 <a href="http://wiresharktraining.com/sharkfest2014.html">Packet Challenge answer key </a>is online at Wireshark University.</p>
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" style="margin: 10px 0;" src="http://www.youtube.com/embed/ZIuNBg24FGg" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <h3 class="post-title2"><strong>SharkFest Jeopardy</strong></h3>
                                <p>Click here for the full [<a href="assets/presentations/Sharkfest Jeopardy.pdf">PDF</a> || <a href="assets/presentations/Sharkfest Jeopardy.ppsx">PPT</a>] of SharkFest Jeopardy</a></p>

                                <h3 class="post-title2"><strong>Keynote Presentations</strong></h3>
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/NRu3s9xBDZE" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <p><strong>Continuous Measurement</strong><br/>
                                Vinton Cerf</p>
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/sPSxWWXWKpk" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <p><strong>The Past and Future</strong><br/>
                                Tim O'Neill</p>
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/dqfw_pEin0g" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <p><strong>The Future of Wireshark: Inside Wireshark Qt</strong><br/>
                                Gerald Combs and Laura Chappell</p>
                                
                                <h3 class="post-title2"><strong>Beginner Track</strong></h3>
                                <ul style="list-style:none;">
                                <li>B1: Art of Packet Analysis [<a href="presentations/B1.pdf">PDF</a> ||<a href="assets/presentations/B1-Wireshark for Beginners-Hansang_Bae.pptx"> PPT</a>] by Hansang Bae</li>
                                <ul>
                                <li class="presVideo"><a href="http://www.youtube.com/embed/qvDmdv1-Xik" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:09)</li>
                                </ul>
                                <li>B4: <a href="assets/presentations/Fun-With-Traces-Beginner-Deck.pdf">Fun with Traces</a> by Stuart Kendrick</li>
                                <li>B5: TCP Analysis - First Steps [<a href="assets/presentations/B5 - TCP Analysis - First Steps.pdf">PDF</a> ||<a href="assets/presentations/B5 - TCP Analysis - First Steps.pptx"> PPT</a>] by Jasper Bongertz</li>
                                <li>B6: <a href="assets/presentations/B6-Get Started with HTTP Analysis.pdf">Get Started with HTTP Analysis</a> by Robert Bullen</li>

                  
                                <li>B10: <a href="assets/presentations/B10 - Understanding Wiresharks Reassembly Features.pdf">Understanding Wiresharks Reassembly Features</a> by Christian    Landström</li>
                                <li>B11: <a href="assets/presentations/B11 - IPv6 Security  Assessment Tools and Infrastructure Mitigation_JeffCarrell_finals1.pdf">IPv6 Security Assessment    Tools and Infrastructure Mitigation</a> by Jeff Carrell</li>
                                <li>B12: <a href="assets/presentations/B12 - VoIP Troubleshooting - Phillip Shade.pdf"> VoIP Troubleshooting </a>by Phillip Shade</li>
                                </ul>

                                <h3 class="post-title2"><strong>Intermediate Track</strong></h3>
                                <ul style="list-style:none;">
                                <li>I1: <a href="assets/presentations/I1 Final - Haugdahl_b.pdf">Best Practices for Packet Collection, Aggregation &amp; Distribution in the Enterprise</a> by J. Scott Haugdahl</li>
                                <li>I2: <a href="assets/presentations/I2.pdf">Common Mistakes In Packet Collection</a> by Chris Greer</li>
                                <ul>
                                <li class="presVideo"><a href="http://www.youtube.com/embed/rHQ1zg0rjX0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (38:16)</li></ul>
                                <li>I3: Maximizing Packet Capture Performance [<a href="assets/presentations/I3: Sharkfest_2014_ABrown - Copy.pdf">PDF</a> ||<a href="assets/presentations/I3.pptx"> PPT</a>] by Andrew Brown</li>
                                <li>I4:<a href="assets/presentations/I4.pptx"> How to Troubleshoot the Top 5 Causes for Poor Application Performance with Wireshark/Pilot</a> by Mike Canney</li>
                                <li>I5: Monitoring Mobile Network Traffic (3G/LTE) [<a href="assets/presentations/I5.pdf">PDF</a> ||<a href="assets/presentations/I5.pptx"> PPT</a>] by Luca Deri</li>
                                <li>I6:<a href="assets/presentations/I6.pptx"> Getting the Most out of Your SDN</a> by Steve Riley</li>

                                <li>I8: Wireshark in the Large Enterprise [<a href="assets/presentations/I8-Wireshark in the Large Enterprise-Hansang_Bae.pdf">PDF</a> ||<a href="assets/presentations/I8-Wireshark in the Large Enterprise-Hansang_Bae.pptx"> PPT</a>] by Hansang Bae</li>
                                <li>I9: <a href="assets/presentations/I9 - Understanding the Wireshark TCP Expert.pdf">Understanding the TCP Expert</a> by Jasper Bongertz</li>

                                <li>I11: <a href="assets/presentations/Visualizing_Problems Through_Packets.pdf" title="Presentation slides" target="_blank">Visualizing Problems through Packets </a> by Kevin Burns</li>
                                <ul>
                                <li class="presVideo"><a href="https://www.youtube.com/embed/uLhpe6T1kr8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:07:10)</li>
                                </ul>
                                <li>I10: <a href="assets/presentations/I10 Anatomy of a CYber Attack.pptx">Anatomy of a Cyber Attack</a> by Tim O'Neill</li>
                                <li>I12: <a href="assets/presentations/i12-capturing-a-packet_from-wire-to-wireshark_0.5-upload.pdf">Capturing a Packet from Wire to Wires</a> by Joerg Mayer</li>
                                <li>I13: <a href="assets/presentations/I13-Analysis and Visualizations.pdf">Analysis and Visualization</a> by Robert Bullen</li>
                                </ul>
                          
                                <h3 class="post-title2"><strong>Advanced Track</strong></h3>
                                <ul style="list-style:none;">
                                <li>A1: <a href="assets/presentations/A1 Dissectors.pptx">Writing a Wireshark Dissector</a> by Graham Bloice</li>
                                <li>A2: <a href="assets/presentations/A2 - Defending the Network.pdf">Defending the Network</a> by Jasper Bongertz and Christian Landström</li>
                                <li>A3: <a href="https://support.riverbed.com/apis/steelscript/SharkFest2014.slides.html">Packet Analysis and Visualization with SteelScript</a> by Christopher White</li>
                                <li>A5: <a href="assets/presentations/Diagramming-IT-Environments-Deck.pdf">Diagramming IT Environments</a> by Stuart Kendrick</li>
                                <li>A6: <a href="assets/presentations/A6.pptx">Large-Scale Passive Network Monitoring using Ordinary Switches</a> by Justin Scott and Rich Groves</li>
                                <li>A7: <a href="assets/presentations/A7 - Dive Even Deeper.pptx">Dive Even Deeper - Capturing, Analyzing and Filtering System Events</a> by Loris Degioanna and Davide Schiera</li>
                                <li>A8: <a href="assets/presentations/A8 and A13 Lua.pptx">Old &amp; Busted: C-code… New Hotness: Lua!</a> by Hadriel Kaplan</li>
                                <li>A9: <a href="assets/presentations/A9.pdf">Wi-Fi Threats and Countermeasures</a> by Gopinath KN</li>
                                <li>A11: <a href="assets/presentations/Definitive-Diagnostic-Data-Deck.pdf">Definitive Diagnostic Data Deck</a> by Stuart Kendrick</li>
                                <li>A12: <a href="assets/presentations/A12 - (Not So) False Positives in Application Performance Analysis.pdf">(Not So) False Positives in Application Performance Analysis</a> by Christian Landström</li>
                                <li>A13: <a href="assets/presentations/A8 and A13 Lua.pptx">Old &amp; Busted: C-code… New Hotness: Lua!</a> by Hadriel Kaplan</li>
                                </ul>  
                                
                                <h3 class="post-title2">SharkBytes</h3>
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/c119qZnKFsw" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <p>SharkBytes consist of "little crunchy bits of wisdom". Like the immensely popular TED talks, Shark Bytes aim to inform, inspire, surprise, and delight as presenters share their personal perspective on a topic in under 5 minutes.</p>

                            </div>
                        </div>
                        
                    </article>

                    <!-- About the author -->
                    
                    <!-- /About the author -->

                    <!-- Comments -->
                    
                    <!-- /Comments -->

                    <!-- Leave a Comment -->
                    
                    <!-- /Leave a Comment -->

                </section>
                <!-- Content -->

                <hr class="page-divider transparent visible-xs"/>

                <!-- Sidebar -->
    <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
        <div class="background-blue front-column1">
            <div class="grid-cell">
                <h4 class="widget-title">A Word of Thanks</h4>
                <p>Post-conference feedback is in, and the preponderance of attendees agree that SharkFest’14 was a smashing success! We’d like to express our immense gratitude to the developers, keynote speakers, presenters, attendees, sponsors, events staff, Dominican University of California summer conference staff, and volunteers who generously collaborated to coordinate the 7th annual Wireshark community event.</p>
            </div>
        </div>
                    <div class="widget categories">
                        <!--
                        <h4 class="widget-title">Past Years</h4>
                        <ul>
                            <li><a href="sf14.html">SharkFest'14</a></li>
                            <li><a href="sf13.html">SharkFest'13</a></li>
                            <li><a href="sf12.html">SharkFest'12</a></li>
                            <li><a href="sf11.html">Sharkfest'11</a></li>
                            <li><a href="sf10.html">Sharkfest'10</a></li>
                            <li><a href="sf09.html">Sharkfest'09</a></li>
                            <li><a href="sf08.html">Sharkfest'08</a></li>
                        </ul> -->
                    </div>

                    <div class="widget flickr-feed">
                        
                       

                
                <div class="post-media">
                <a style="font-size: 25px;" data-lightbox="sf14" alt="Click Here to View Pictures from Sharkfest'14!" href='img/sharkfest14gallery/sharkfest19.jpg'><img src="img/sharkfest14gallery/retrogallerylink.jpg" ></a></div>
                    <a href='img/sharkfest14gallery/sharkfest2.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest3.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest4.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest5.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest6.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest7.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest8.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest9.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest10.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest11.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest12.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest13.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest14.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest15.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest16.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest17.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest18.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest20.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest1.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest25.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest26.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest27.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest28.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest29.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest30.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest31.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest32.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest33.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest34.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest35.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest36.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest37.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest38.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest39.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest40.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest41.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest42.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest43.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest44.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest45.jpg' data-lightbox="sf14"></a>
                    <a href='img/sharkfest14gallery/sharkfest46.jpg' data-lightbox="sf14"></a>
                    </div>
                     <script>
                    jQuery('a.gallery').colorbox();
                </script>

                        
                


        <!--
                    <div class="widget tag-cloud">
                        <h4 class="widget-title">Tags</h4>
                        <ul>
                            <li><a href="#">Creative</a></li>
                            <li><a href="#">Design</a></li>
                            <li><a href="#">Art</a></li>
                            <li><a href="#">Autpor Name</a></li>
                            <li><a href="#">Coorperate</a></li>
                            <li><a href="#">Wordpress</a></li>
                            <li><a href="#">3D Animations</a></li>
                        </ul>
                    </div>

                    <div class="widget">
                        <h4 class="widget-title">Join The Newsletter</h4>
                        <form method="post" action="#">
                            <div class="form-group">
                                <input type="text" placeholder="Type and hit enter... " class="form-control" name="search" title="search">
                                <input type="submit" hidden="hidden"/>
                            </div>
                        </form>
                    </div>

                </aside>
                <!-- Sidebar -->
    <div class="background-blue front-column1">
        <div class="grid-cell">
            <div class="sponsor-list">
                <div class="sponsor-imgs">
                        <h4><strong>Great White Shark Sponsor</strong></h4>
                        <a href="http://www.cpacket.com/index.php" target="_blank"><img class="sponsors" src="img/sponsors/cpacket-white.png"></a>

                        <h4><strong>Angel Shark Sponsors</strong></h4>
                        <a href="http://www.apcon.com/about-us" target="_blank"><img class="sponsors" src="img/sponsors/apcon-white.png"></a>
                        <a href="http://www.endace.com/" target="_blank"><img class="sponsors" src="img/sponsors/endace_big.png"></a>
                        <a href="http://www.gigamon.com/" target="_blank"><img class="sponsors" src="img/sponsors/gigamon_logo.png"></a>
                        <a href="http://www.bigswitch.com/" target="_blank"><img class="sponsors" src="img/sponsors/bigswitch-white.png"></a>

                        <h4><strong>Silvertip Shark Sponsors</strong></h4>
                        <a href="http://www.dual-comm.com/" target="_blank"><img class="sponsors" src="img/sponsors/dualcomm-white.png"></a>
                        <a href="http://www.ntop.org/" target="_blank"><img class="sponsors" src="img/sponsors/ntop.png"></a>
                        <a href="http://www.garlandtechnology.com/" target="_blank"><img  class="sponsors" src="img/sponsors/garland-white.png"></a>
                        <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                        <a href="http://www.napatech.com/" target="_blank"><img class="sponsors" src="img/sponsors/napatech-white.png"></a>
                        <a href="http://www.aristanetworks.com/" target="_blank"><img class="sponsors" src="img/sponsors/arista-bright.png"></a>
                        <a href="http://www.wiresharktraining.com/" target="_blank"><img class="sponsors" src="img/sponsors/wiresharku-white.png"></a>
                        <a href="http://www.ixiacom.com/" target="_blank"><img class="sponsors" src="img/sponsors/ixia-white.png"></a>
                        <a href="http://www.interfacemasters.com/" target="_blank"><img class="sponsors" src="img/sponsors/interface.png"></a>
                        <a href="http://www.insidethestack.com/" target="_blank"><img class="sponsors" src="img/sponsors/inside-white.png"></a>

                        <h4><strong>Hosting Sponsor</strong></h4>
                        <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>
                    </div>
            </div>
        </div>
    </div>
                </div>
                </section>
                </aside>
                <!-- /PAGE BLOG -->

            </div>
            <!-- /Content area -->
        </div>
    </section>
</div>
</section>
</div>

    <!-- FOOTER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
    
