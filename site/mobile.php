<?php
// NOTE: you need to include this script at top of the page

require_once('inc/mobile_device_detect.php');
$redirect = mobile_device_detect(true, true, true, true, true, true);
// redirect all mobiles to mobile site and all other browsers to desktop site
if($redirect==false){
  header('Location:agenda.html');
}; 
?>
<!DOCTYPE html>
<html class="cufon-active cufon-ready" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="">
<meta name="description" content="">
<title>Sharkfest‘13 Mobile Agenda • Wireshark Developer and User Conference • June 16th - 19th, 2013 • UC Berkeley</title>

<!-- FOR MOBILE -->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<!-- Javascript -->
<!-- Changed for the new version -->
<script src="js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="js/jquery-ui.js" type="text/javascript"></script>
<!-- new -->
<script  type="text/javascript" src="js/jquery.mobile.js"></script>

<!-- FONT REPLACEMENT, ACCORDION, POPUPS FUNCT. -->
<script type="text/javascript">
	$(document).ready(function(){
	

		// ACCORDION
		$('.accordion .head').click(function() {
			$('.accordion div').animate({ opacity: 'hide', height: 'hide' }, 'slow');
			if ($(this).next().is(':visible')) {
				$(this).removeClass('open');
				$(this).next().animate({ opacity: 'hide', height: 'hide' }, 'slow');
			}
			else{
				$(this).next().animate({ opacity: 'show', height: 'show' }, 'slow');
				$(this).addClass('open');
			};
			return false;
		}).next().animate({ opacity: 'hide', height: 'hide' }, 'slow');
		
		// CLOSE POPUPS
		$('.closePopup').click(function() {
			$(this).parent().popup('close');
		});
		
			// force links
		$('a').click(function(){ window.location.href = $(this).attr('href'); });
		
	});
</script>

<!-- CSS -->
<link href="mobile.css" title="screen" type="text/css" rel="stylesheet" media="screen" />
</head>
<body class="page agenda mobile">
<div id="wrapper" data-role="page">
  <div id="header"> 
    <!-- you can change the div for h1, h2, etc for SEO -->
    <div id="logo"><a href="#" title=""><span>SHARKFEST ‘12, Wireshark Developer and User Conference</span></a></div>
  </div>
  <div id="status-bar">
    <h2 class="pageTitle left">Agenda</h2>
    <p class="links right"><a href="agenda.html" title="Home Page">Desktop</a> | Mobile</p>
  </div>
  <div id="main">
    <div class="accordion">
      <h3 class="head"><a href="#">Sunday, June 16</a></h3>
      <div class="content"> 
        
        <!-- 1º accordion content -->
        <table>
          <tbody>
            <tr>
              <td width="100" nowrap="nowrap" class="tableTime"><p>12:00p &ndash; 6:00p</p></td>
              <td class="a"><p>BADGE PICK UP &amp; REGISTRATION</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>12:00p &ndash; 1:30p</p></td>
              <td class="a"><p>BOX LUNCH - Building 14</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>1:30p &ndash; 2:45p</p></td>
              <td class="b"><p><span>NAP-1: Application Performance Analysis</span> </p>
                <p>Mike Canney - Meeting Room 102</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-1: Deep
                  Dive Packet Analysis</span> </p>
                <p>Hansang Bae - Krutch Theater</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-1: Understanding Encryption Services</span> </p>
                <p>Larry Greenblatt - Garden Room</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>WD-1: Setting Up a Wireshark Development Environment: Preparation for WD-2 and <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a></span> </p>
                <p>Wireshark Core Developers - Executive Dining Room</p>
              <p><strong>(Wireshark Development</strong>)</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>2:45p &ndash; 3:00p</p></td>
              <td class="a"><p>SESSION TRANSITION</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>3:00p &ndash; 4:15p</p></td>
              <td class="b"><p><span>NAP-2: It's Not the Network! The Value of Root Cause Analysis</span> </p>
                <p>Graeme Bailey - Garden Room</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-2:  Intro to IPv6 Addressing</span> </p>
                <p>Nalini Elkins - Krutch Theater</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-2:  VoIP Fundamentals</span> </p>
                <p>Phill Shade - Meeting Room 102</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>WD-2: Introduction
                  to Programming with Wireshark: Preparation for <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a></span> </p>
                <p>Wireshark Core Developers - Executive Dining Room</p>
                <p><strong>(Wireshark Development</strong>)</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>4:15p &ndash; 4:30p</p></td>
              <td class="a"><p>SESSION TRANSITION</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>4:30p &ndash; 5:45p</p></td>
              <td class="b"><p><span>NAP-3: Microsoft SMB Troubleshooting</span> </p>
                <p>Rolf Leutert - Krutch Theater</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-3: Debugging Wireless with Wireshark Including Large Trace Files, AirPcap &amp; Cascade Pilot</span> </p>
                <p>Megumi Takeshita - Meeting Room 102</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-3: IPv6 Security Assessment Tools & Infrastructure Mitigation </span> </p>
                <p class="b">Jeff Carrell - Garden Room</p>
              <p class="b"><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>WD-2: Introduction
                  to Programming with Wireshark: Preparation for <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a></span> </p>
                <p>Wireshark Core Developers - Executive Dining Room</p>
              <p><strong>(Wireshark Development</strong>)</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>5:45p &ndash; 6:30p</p></td>
              <td class="a specialevent"><p class="specialevent"><a class="specialevent" title="Find out more about the event" href="#popupDinnerReception" data-rel="popup">WELCOME RECEPTION AND PRE-DINNER SOCIAL</a> &ndash; Grand Court</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>6:30p &ndash; 8:30p </p></td>
              <td class="a specialevent"><p class="specialevent"><a class="specialevent" title="Find out more about the event" href="#popupDinnerReception" data-rel="popup">WELCOME DINNER</a> &ndash; Grand Court</p></td>
            </tr>
          </tbody>
        </table>
        <!-- END 1º accordion content --> 
        
      </div>
      <h3 class="head"><a href="#">Monday, June 17</a></h3>
      <div class="content"> 
        <!-- 2º accordion content -->
        <table>
          <tbody>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>8:00a &ndash; 9:00a</p></td>
              <td class="a"><p>BREAKFAST - Great Hall</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>9:00a &ndash; 10:00a</p></td>
              <td class="a"><p><span> KEYNOTE &#8211; History of the Wireshark Project: A Reminiscence of the
                  15-Year Life of the Project</span></p>
                <p>Gerald Combs and Special Guests - Krutch Theater</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>10:00a &ndash; 10:15a</p></td>
              <td class="a"><p>BREAK - ROOM CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>10:15a &ndash; 11:30a</p></td>
              <td class="b"><p><span>NAP-4: Wireless Network Optimization</span></p>
                <p>Trent Cutler  - Meeting Room 102</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-4: Inside the
                  TCP Handshake</span></p>
                <p>Betty DuBois - Krutch Theater</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-4: Trace File Sanitization NG</span></p>
                <p>Jasper Bongertz - Garden Room</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-1: Root Cause Analysis</span></p>
                <p><strong>MAX SEATS: 25</strong></p>
                <p>Stuart Kendrick - HOL: 204</p>
              <p><strong>(Hands-on Labs)</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-2: WiFi Security &amp; aircrack-ng</span></p>
                <p><strong>MAX SEATS: 15</strong></p>
                <p> Thomas D'Otreppe &amp; Jon
                  Ford - HOL: 104</p>
              <p><strong>(Hands-on Labs)</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>11:30a &ndash; 11:45a</p></td>
              <td class="a"><p>BREAK - ROOM CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>11:45a &ndash; 1:00p</p></td>
              <td class="b"><p><span>NAP-5: Correlating Traces From Multiple Tiers</span></p>
                <p>Paul Offord - Krutch Theater</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-5: Capture Limitations of a Laptop: When Does It Start Dropping Packets?</span></p>
                <p>Chris Greer - Meeting Room 102</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-5: Using Wireshark to Gather Forensic Evidence On Malware Outbreaks in Enterprise Networks</span></p>
                <p>Christian Landström - Garden Room</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-1: Root Cause Analysis <em>Continued</em></span> </p>
                <p>Stuart Kendrick - HOL: 204</p>
              <p><strong>(Hands-on Labs)</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-2: WiFi Security &amp; aircrack-ng <em>Continued</em></span> </p>
                <p> Thomas D'Otreppe &amp; Jon
                  Ford - HOL: 104</p>
              <p><strong>(Hands-on Labs)</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>1:00p &ndash; 1:45p</p></td>
              <td class="a"><p>LUNCH</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>11:45p &ndash; 3:00p</p></td>
              <td class="b"><p><span>NAP-6: Why Pilot?</span></p>
                <p>Martin Lewald & Janice Spampinato - Krutch Theater</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-6: Wireshark in the Large Enterprise</span></p>
                <p style="color:red;"><strong>CANCELLED</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-6:
                  I Can Hear You Tunneling...</span></p>
                <p>Alex Weber - Garden Room </p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
              </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-1: Root Cause Analysis <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-2: WiFi Security &amp; aircrack-ng <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>3:00p &ndash; 3:15p</p></td>
              <td class="a"><p>BREAK &#8211; ROOM CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>3:15p &ndash; 4:30p</p></td>
              <td class="b"><p><span>NAP-7 Network Programming with Riverbed FlyScript</span></p>
                <p style="color:red;"><strong>CANCELLED</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-7 Troubleshooting from the Field</span></p>
                <p>Herbert Grabmayer - Meeting Room 102</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-7 Wireshark Network Forensics</span></p>
                <p>Laura Chappell - Krutch Theater</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-1: Root Cause Analysis <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-2: WiFi Security &amp; aircrack-ng <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>4:30p &ndash; 4:45p</p></td>
              <td class="a"><p>BREAK &#8211; ROOM CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>4:45p &ndash; 6:00p</p></td>
              <td class="b"><p><span>NAP-8: Using Wireshark as an Application Engineer</span></p>
                <p>Tim Poth - Krutch Theater</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-8: IPv6 Address Planning</span></p>
                <p>Nalini
                  Elkins - Garden Room</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-8: Why Is Cryptography So Hard to Get Right?</span></p>
                <p>Ron Bowes - Meeting Room 102</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-1: Root Cause Analysis <em>Continued</em></span> </p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-2: WiFi Security &amp; aircrack-ng <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>6:30p &ndash; 9:00p</p></td>
              <td  colspan="4" class="a specialevent"><p class="specialevent"><a class="specialevent" title="Find out more about the event" href="#popupSpecialEveningEvent" data-rel="popup">OFFSITE EVENING EVENT AND DINNER</a><br />
                  Bus transportation to and from the Lawrence Hall of Science will be provided.</p></td>
            </tr>
          </tbody>
        </table>
        
        <!-- END 2º accordion content --> 
      </div>
      <h3 class="head"><a href="#">Tuesday, June 18</a></h3>
      <div class="content"> 
        <!-- 3º accordion content -->
        <table>
          <tbody>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>8:00a &ndash; 9:00a</p></td>
              <td class="a"><p>BREAKFAST - Great Hall</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>9:00a &ndash; 10:00a</p></td>
              <td class="a"><p><span>KEYNOTE: &quot; The Nice Thing About Standards is
                  That There are so Many of Them!&quot;: Musings of an Early Networker</span></p>
                <p> Rich Seifert, M.S.E.E., M.B.A, J.D., President of
                  Networks and Communications Consulting and co-author of the original Ethernet
                  Specification</p>
                <p>Krutch Theater</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>10:00a &ndash; 10:15a</p></td>
              <td class="a"><p>BREAK - ROOM CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>10:15a &ndash; 11:30a</p></td>
              <td class="b"><p><span>NAP-9: Application Performance Analysis</span></p>
                <p>Mike Canney - Meeting Room 102</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-9: Network Monitoring is Dead, Wireshark is part of the Resurrection</span></p>
                <p>Rony Kay - Garden Room</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-9: Attack Trends & Techniques</span></p>
                <p>Steve Riley - Krutch Theater</p>
              <p><strong>(Security</strong>)</p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-3: Cyber Security  Investigation  &amp; Network  Forensic  Analysis:  Practical  Techniques  For Analyzing  Suspicious  Network  Traffic</span></p>
                <p><strong>Max Seats: 12</strong></p>
                <p>Phill Shade - HOL: 204</p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-4: IPv6: Build It, Use It</span></p>
                <p><strong>Max Seats: 16</strong></p>
                <p>Jeff Carrell - HOL: 104</p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>11:30a &ndash; 11:45a</p></td>
              <td class="a"><p>BREAK - ROOM CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>11:45a &ndash; 1:00p</p></td>
              <td class="b"><p><span>NAP-10: Enabling Visibility for Wireshark Across Physical, Virtual and SDN</span></p>
                <p>Patrick Leong - Krutch Theater</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-10: Writing a Wireshark Dissector</span></p>
                <p>Graham Bloice - Meeting Room 102</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-10: Wireshark Network Forensics</span></p>
                <p>Laura Chappell - Garden Room</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-3: Cyber Security  Investigation  &amp; Network  Forensic  Analysis:  Practical  Techniques  For Analyzing  Suspicious  Network  Traffic <em>Continued</em></span></p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-4: IPv6: Build It, Use It <em>Continued</em></span></p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>1:00p &ndash; 1:45p</p></td>
              <td class="a"><p>LUNCH - Great Hall</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>1:45p &ndash; 3:00p</p></td>
              <td class="b"><p><span>NAP-11: Expanding Wireshark Beyond Ethernet &amp; Network Interfaces </span></p>
                <p>Mike Kershaw & Mike Ryan - Meeting Room 102</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-11: How to Use Wireshark To Analyze Video</span></p>
                <p>Betty DuBois - Krutch Theater</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-14: Understanding Encrytion Services Using Wireshark</span></p>
                <p>Larry Greenblatt - Meeting Room 203</p>
                <p style="color:red;"><strong>TIME SLOT CHANGED</strong></p>
              <p><strong>(Security</strong>)</p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-3: Cyber Security  Investigation  &amp; Network  Forensic  Analysis:  Practical  Techniques  For Analyzing  Suspicious  Network  Traffic <em>Continued</em></span></p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-4: IPv6: Build It, Use It <em>Continued</em></span></p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>3:00p &ndash; 3:15p</p></td>
              <td class="a"><p>BREAK - ROOM
                  CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>3:15p &ndash; 4:30p</p></td>
              <td class="b"><p><span>NAP-12: Packet Optimization and Visibility using Wireshark <br />
                  and pcaps</span></p>
                <p>Gordon Beith - Garden Room</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-12: WLAN Troubleshooting with Wireshark & AirPcap </span></p>
                <p>Rolf Leutert - Meeting Room 102</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-12: Trace File Sanitization NG</span></p>
                <p>Jasper Bongertz - Krutch Theater</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-3: Cyber Security  Investigation  &amp; Network  Forensic  Analysis:  Practical  Techniques  For Analyzing  Suspicious  Network  Traffic <em>Continued</em></span></p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-4: IPv6: Build It, Use It <em>Continued</em></span></p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>4:30p &ndash; 4:45p</p></td>
              <td class="a"><p>BREAK &#8211; ROOM CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>4:45p &ndash; 6:00p</p></td>
              <td class="b"><p><span>NAP-13: Wireshark Users
                  Ask <br />
                  The Experts!</span></p>
                <p>Moderator : Chris Bidwell - Krutch Theater</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-13: IPv6 Trace Analysis Using Wireshark</span></p>
                <p>Nalini Elkins - Garden Room</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-13: How 802.11ac Will Hide Problems From Wireshark</span></p>
                <p>Joe Bardwell - Meeting Room 102</p>
              <p><strong>(Security</strong>)</p></td>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-3: Cyber Security  Investigation  &amp; Network  Forensic  Analysis:  Practical  Techniques  For Analyzing  Suspicious  Network  Traffic <em>Continued</em></span></p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-4: IPv6: Build It, Use It <em>Continued</em></span> </p></td>
            <tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>6:00p &ndash; 8:00p</p></td>
              <td class="a specialevent"><p class="specialevent"><a class="specialevent" title="Find out more about the event" href="#popupVendorShowcaseReception" data-rel="popup">VENDOR SHOWCASE AND RECEPTION</a> - Grand Court</p></td>
            </tr>
          </tbody>
        </table>
        
        <!-- END 3º accordion content --> 
      </div>
      <h3 class="head"><a href="#">Wednesday, June 19</a></h3>
      <div class="content"> 
        <!-- 4º accordion content -->
        <table>
          <tbody>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>8:00a &ndash; 9:00a</p></td>
              <td class="a"><p>BREAKFAST</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>9:00a &ndash; 10:00a</p></td>
              <td class="b"><p><span>KEYNOTE: To Engineer is Human... So Is
                  Being Lazy</span></p>
                <p> Charles Kaplan, Technical
                  Director, Office of the CTO, Riverbed</p>
                <p>Krutch Theater</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>10:15a &ndash; 11:30a</p></td>
              <td class="b"><p><span>NAP-14: Accessing Packet Traces from Multiple Locations with Packet Trace Warehouse</span></p>
                <p>Bill Eastman - Krutch Theater</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-14: Top 5 False Positives When Analyzing Networks</span></p>
                <p>Jasper Bongertz - Meeting Room 102</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-11:  IPv6 Security</span></p>
                <p>Nalini Elkins - Room 203</p>
                <p style="color:red;"><strong>TIME SLOT CHANGED</strong></p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-5: SSL
                  Troubleshooting with Wireshark</span></p>
                <p><strong>Max Seats: 30</strong></p>
                <p>Mak Kolybabi - HOL: 204</p>
              <p><strong>(Hands-on Labs)</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-6: IPv6: Build It, Use It</span></p>
                <p><strong>Max Seats: 16</strong></p>
                <p>Jeff Carrell - HOL: 104</p>
              <p><strong>(Hands-on Labs)</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-7: Cyber Security Investigation & Network Forensic Analysis - Practical Techniques For Analyzing Suspicious Network Traffic</span></p>
                <p><strong>Max Seats: 12</strong></p>
                <p>Phill Shade - Seminar C</p>
              <p><strong>(Hands-on Labs)</strong></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>11:30a &ndash; 11:45a</p></td>
              <td class="a"><p>BREAK - ROOM CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>11:45a &ndash; 1:00p</p></td>
              <td class="b"><p><span>NAP-15: Understanding Wireshark's Reassembly Features</span></p>
                <p>Christian Landström - Meeting Room 203</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-15: So You've Found the Suspect Traffic, But What's Causing It?</span></p>
                <p>Graeme Bailey - Krutch Theater</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-15: Why Is Cryptography So Hard to Get Right?</span></p>
                <p>Ron Bowes - Meeting Room 102</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-5: SSL
              Troubleshooting with Wireshark <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-6: IPv6: Build It, Use It <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-7: Cyber Security Investigation & Network Forensic Analysis- Practical Techniques For Analyzing Suspicious Network Traffic <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>1:00p &ndash; 2:00p</p></td>
              <td class="a"><p>LUNCH  - Great Hall</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>2:00p &ndash; 3:15p</p></td>
              <td class="b"><p><span>NAP-16: Capture Limitations of a Laptop: When Does It Start Dropping Packets?</span></p>
                <p>Chris Greer - Krutch Theater</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-16: Wireshark in the 
                  Large Enterprise</span></p>
                <p>Hansang Bae - Meeting Room 102</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-16: I Can Hear You Tunneling</span></p>
                <p>Alex Weber - Meeting Room 203</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-5: SSL
              Troubleshooting with Wireshark <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-6: IPv6: Build It, Use It <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-7: Cyber Security Investigation & Network Forensic Analysis - Practical Techniques For Analyzing Suspicious Network Traffic <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>3:15p &ndash; 3:30p</p></td>
              <td class="a"><p>BREAK - ROOM
                  CHANGE</p></td>
            </tr>
            <tr>
              <td nowrap="nowrap" class="tableTime"><p>3:30p &ndash; 4:45p</p></td>
              <td class="b"><p><span>NAP-17: Network Virtualization: the SDN You REALLY Want</span></p>
                <p>Steve Riley - Meeting Room 102</p>
              <p><strong>(Network &amp; Application Performance</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>PA-17: TCP Performance Problem Analysis Through Multiple Network Segments</span></p>
                <p>Jasper Bongertz & Christian Landström - Krutch Theater</p>
              <p><strong>(Packet Analysis</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>SEC-17: Wireless Intrusion Detection</span></p>
                <p>Mike Kershaw - Meeting Room 203</p>
              <p><strong>(Security</strong>)</p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-5: SSL
              Troubleshooting with Wireshark <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-6: IPv6: Build It, Use It <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>HOL-7: Cyber Security Investigation & Network Forensic Analysis - Practical Techniques For Analyzing Suspicious Network Traffic <em>Continued</em></span></p></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="b"><p><span>Drop-In <a class="specialevent" title="Find out more about the event" href="#popupHackathon" data-rel="popup">HACKATHON</a> With Wireshark Core Developers</span></p>
              <p>Executive Dining Room</p></td>
            </tr>
            <tr>
              <td  nowrap="nowrap" class="tableTime"><p>4:45p &ndash; 6:00p</p></td>
              <td class="a specialevent"><p class="specialevent"><a class="specialevent" title="Find out more about this event" href="#popupHappyhour" data-rel="popup">FAREWELL HAPPY HOUR ON THE TERRACE</a> - Ginko Court</p></td>
            </tr>
          </tbody>
        </table>
        
        <!-- END 4º accordion content --> 
      </div>
      <div id="footer"></div>
    </div>
    <!-- END accordion --> 
    
    <!-- START Popup content -->
    <div data-role="popup" id="popupDinnerReception" class="mobilePopup"> <span class="closePopup">x</span>
      <h2 class="title">Welcome Reception &amp; Dinner</h2>
      <div class="sponsor"> <img src="assets/photos/welcome_reception.png" alt="Welcome Reception &amp; Dinner" width="180" height="120"/>
        <p> <strong>Sunday, June 16<br />
          5:45p &ndash; 8:30p<br />
          Grand Court<br />
          </strong>Join old friends and new for appetizers and drinks during the Welcome Reception from 5:45p – 6:30p. Afterward, a festive dinner will be served, giving you even more time to network, catch up with your peers, and make plans for the rest of the conference days. </p>
      </div>
    </div>
    <div data-role="popup" id="popupHackathon" class="mobilePopup"> <span class="closePopup">x</span>
      <h2 class="title">Hackathon with Wireshark Core Developers</h2>
      <div class="sponsor">
        <p><strong>Sunday through Wednesday – Drop-in<br />
          Sunday 1:30p &ndash; 5:45p<br />
          Monday &ndash; Tuesday 10:15a &ndash; </strong><strong>6:00p<br />
          Wednesday </strong><strong>10:15a &ndash; </strong><strong>4:45p</strong><strong><br />
          Executive Dining Room<br />
          </strong>Beginning on Sunday with a working session designed to familiarize avid and wannabe developers with the project structure and tool suite required to build the Wireshark project, you’ll move on to an “Introduction to Programming with Wireshark” session and the succeeding drop-in hackathon with core developers that will continue through conference end. </p>
      </div>
    </div>
    <div data-role="popup" id="popupSpecialEveningEvent" class="mobilePopup"> <span class="closePopup">x</span>
      <h2 class="title">Offsite Evening Event and Dinner</h2>
      <div class="sponsor"> <img class="left" src="assets/photos/special_event_collage.png" alt="Special Event" width="180" height="120"/>
        <p><strong>Monday, June 17<br />
          6:30p – 9:00p<br />
          Lawrence Hall of Science<br />
          </strong>After spending all day in sessions, it's time to unwind and have some fun. Join us at the Lawrence Hall of Science where we'll enjoy great food, drink, music, and amazing views of the San Francisco Bay.</p>
        <p>**Bus transportation to and from the Lawrence Hall of Science will be provided.</p>
      </div>
    </div>
    <div data-role="popup" id="popupVendorShowcaseReception" class="mobilePopup"> <span class="closePopup">x</span>
      <div class="sponsor">
        <h2 class="title">Vendor Showcase &amp; Reception</h2>
        <img src="assets/photos/vendor_reception.png" width="180" height="120" alt="Vendor Reception"/>
        <p><strong>Tuesday, June 18<br />
          6:00p – 8:00p<br />
          Grand Court<br />
          </strong>Enjoy an evening of networking with our sponsors and other Sharkfest attendees as we gather for drinks and appetizers while learning about even more cool technology.</p>
      </div>
    </div>
    <div data-role="popup" id="popupHappyhour" class="mobilePopup"> <span class="closePopup">x</span>
      <div class="sponsor">
        <h2 class="title">Farewell Happy Hour</h2>
        <img src="assets/photos/happy_hour.jpg" width="180" height="120" alt="Happy Hour"/>
        <p><strong>Wednesday, June 19<br />
          4:45p – 6:00p<br />
          Ginko Court<br />
          </strong>After enjoying four full days of sessions and networking, come on by the happy hour for a cold adult beverage, snacks and an opportunity to network one last time with your peers.</p>
      </div>
    </div>
    <div data-role="popup" id="popupChallenge" class="mobilePopup"> <span class="closePopup">x</span>
      <div class="sponsor">
        <h2 class="title">Packet Challenge Project</h2>
        <img src="assets/photos/packet_challenge.png" alt="Special Event" width="180" height="120"/>
        <p>We have created a set of Wireshark Challenges for Sharkfest 2013. The Challenges are intended to give attendees a chance to use existing Wireshark skills or those learned in the various sessions at the conference. All who successfully complete the Challenges will receive a "Certified Packet Whisperer" lapel pin and will be eligible for a drawing for an <a href="http://www.riverbed.com/products-solutions/products/performance-management/wireshark-enhancement-products/Wireless-Traffic-Packet-Capture.html" title="More about AirPcap" target="_blank">AirPcap Nx</a> and <a href="http://www.riverbed.com/products-solutions/products/performance-management/wireshark-enhancement-products/Personal-Desktop-Network-Analyzer.html" title="More information about Cascade Pilot PE" target="_blank">Cascade Pilot Personal Edition</a> license.  Two AirPcap Nx and Cascade Pilot PE licenses will be raffled off to qualified CPW entrants.</p>
      </div>
    </div>
    
    <!-- END popup content--> 
    
  </div>
  <!-- END MAIN --> 
</div>
<!-- END WRAPPER -->

</body>
</html>