<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="lodging-page-img">
</div>
<div class="container">
	<div class="row lodging-page">
		<div class="col-sm-10 col-centered about-container about-text">
			<h2>Kansas City Marriott Downtown</h2>
			<p>200 West 12th Street<br> Kansas City, Missouri 64105 USA</p>
			<p>Discover downtown Kansas City and the Power and Light District from your upscale accommodations at Kansas City Marriott Downtown. Set in the vibrant downtown district, this elegant hotel occupies a historic building, reflecting the charm and history of Kansas City. Newly restyled with modern decor and excellent service, the hotel offers spacious, tasteful hotel rooms and suites with floor-to-ceiling windows and lovely downtown Kansas City views. Enjoy plush bedding, complimentary Wi-Fi and an array of thoughtful amenities. </p>
			<p>All rooms are non-smoking and include: </p>
			<ul>
				<li>• Complimentary high-speed Wi-Fi</li>
				<li>• Large desk and an ergonomic chair </li>
				<li>• 55-inch flat-screen TV and the latest in guest room entertainment television options (like NetFlix)</li>
				<li>• Luxurious Revive™ bedding</li>
				<li>• Kuerig coffee machine</li>
				<li>• Iron and ironing board</li>
				<li>• Hair dryer</li>
				<li>• Safe</li>
			</ul>
			<h2>Booking</h2>
			<p><strong>Rooms are $189 per night, excluding tax</strong></p>
			<p> <strong>Please note that the reservation dates are July 8-15. If you need to arrive earlier or leave later, please send an email with your request to: <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a></strong></p>
			<p><a href="https://book.passkey.com/e/50293209"><strong>Book your room</strong></a></p>
			<p><a href="img/sf20us/parking.pdf"><strong>Parking Map and Pricing</strong></a></p>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>