<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="agenda-background-image">
</div>
<div class="row grid-row reg-row">
	<div class="col-lg-7 col-centered index-text">
	    <div class="">
	        <a class="front-button" href="assets/SharkFest22Agenda.pdf"><h4>Conference Agenda PDF</h4></a>
	        <h4>The conference takes place in the Central time zone.</h4>
	    </div>
	</div>
</div>
	
<div class="container-fluid agenda-container">
	<div class="row">
		<div class="col-md-10 col-centered">
			<div class="row schedule-lvl1">
				<ul class="nav nav-pills nav-justified schedule">
				    <li class="active"><a data-toggle="pill" href="#tab-first"><h4>Day 01<br><span>July 9</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-second"><h4>Day 02<br><span>July 10</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-third"><h4>Day 03<br><span>July 11</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-fourth"><h4>Day 04<br><span>July 12</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-fifth"><h4>Day 05<br><span>July 13</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-sixth"><h4>Day 06<br><span>July 14</span></h4></a></li>
				 </ul>
			</div>
			<div class="tab-content">
				<div id="tab-first" class="table-responsive container tab-pane fade in active">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl1-tab-first"><h4>Pre-Conference Class I: Introduction to Packets - How to capture & analyze them using Wireshark</h4>
						    	<p></p></a></li>
						 </ul>
						<div id="lvl1-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Check-in & Badge Pick up</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Breakfast</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">9:00 - 12:00</th>
										<th class="no-border">
											<h4>Class in session (with morning break)</h4>
											<p>Instructor: <a href="bios/betty-dubois">Betty DuBois</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">12:00 - 1:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">1:00 - 5:00</th>
										<th class="no-border">
											<h4>Class in session (with afternoon break)</h4>
											<p>Instructor: <a href="bios/betty-dubois">Betty DuBois</a></p>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-second" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl2-tab-first"><h4>Pre-Conference Class I: Introduction to Packets - How to capture & analyze them using Wireshark</h4>
						    	<p></p></a></li>
						 </ul>
						<div id="lvl2-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Breakfast</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">9:00 - 12:00</th>
										<th class="no-border">
											<h4>Class in session (with morning break)</h4>
											<p>Instructor: <a href="bios/betty-dubois">Betty DuBois</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">12:00 - 1:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">1:00 - 5:00</th>
										<th class="no-border">
											<h4>Class in session (with afternoon break)</h4>
											<p>Instructor: <a href="bios/betty-dubois">Betty DuBois</a></p>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-third" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl22-tab-first"><h4>Pre-Conference Class II: “Cybersecurity Threat Hunting – Go Deep with Wireshark”</h4></a></li>
							<li><a data-toggle="pill" href="#lvl22-tab-second"><h4>SharkFest</h4></a></li>
						</ul>
						 <div id="lvl22-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Check-in & Badge Pick up</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Breakfast</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">9:00 - 12:00</th>
										<th class="no-border">
											<h4>Class in session (with morning break)</h4>
											<p>Instructor: <a href="bios/chris-greer">Chris Greer</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">12:00 - 1:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">1:00 - 5:00</th>
										<th class="no-border">
											<h4>Class in session (with afternoon break)</h4>
											<p>Instructor: <a href="bios/chris-greer">Chris Greer</a></p>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl22-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">12:00 - 8:00</th>
										<th class="no-border">
											<h4>SharkFest‘22 US Check-In & Badge Pick-Up</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">1:00 - 5:00</th>
										<th class="no-border">
											<h4>Developer Den Drop-In</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">6:00 - 8:30</th>
										<th class="no-border">
											<h4>SharkFest ‘22 US Welcome Dinner &amp; Sponsor Showcase</h4>
											<p style="color: red">SharkFest'22 US Attendees Only</p>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-fourth" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl23-tab-first"><h4>Basie AA1/BB1 (Beginner/Intermediate)</h4></a></li>
							<li><a data-toggle="pill" href="#lvl23-tab-second"><h4>Andy Kirk A/B (Intermediate/Advanced)</h4></a></li>
							<li><a data-toggle="pill" href="#lvl23-tab-third"><h4>Julia Lee A/B (Security and Workshops)</h4></a></li>
						 </ul>
						 <div id="lvl23-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">9:00 - 10:00</th>
										<th class="no-border">
											<h4>Keynote: Latest Wireshark Developments & Road Map (Basie AA1/BB1)</h4>
											<p>Instructor: <a href="bios/gerald-combs">Gerald Combs</a> & Friends</p>
										</th>
									</tr>
									<tr>
										<th class="no-border">10:15 - 11:30</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<h4>01: Network Troubleshooting from Scratch</h4>
											<p>Instructor: <a href="bios/jasper-bongertz">Jasper Bongertz</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">11:45 - 1:00</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>04: Build Your Own: Remotely accessible packet-capture drop box for troubleshooting networks with <$100</h4>
											<p>Instructor: <a href="bios/anthony-efantis">Anthony Efantis</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">1:00 - 2:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">2:00 - 3:15</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<h4>07: Wireshark at Enterprise Scale</h4>
											<p>Instructor: <a href="bios/stephen-donnelly">Dr Stephen Donnelly</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">3:30 - 4:45</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>10: Understanding TCP Throughput</h4>
											<p>Instructor: <a href="bios/kary-rogers">Kary Rogers</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">5:00 - 6:15</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>13: Troubleshoot like a Doctor</h4>
											<p>Instructor: <a href="bios/josh-clark">Josh Clark</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">6:00 - 8:30</th>
										<th class="no-border">
											<h4>Sponsor Technology Showcase Reception, Treasure Hunt & Dinner (Barney Allis Historic Lobby)</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl23-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">10:15 - 11:30</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>02: Wireshark and WiFi: Multicast Case Study</h4>
											<p>Instructor: <a href="bios/george-cragg">George Cragg</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">11:45 - 1:00</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>05: Duct tape and baling wire: Extending Wireshark with Lua</h4>
											<p>Instructor: Chuck Craft</p>
										</th>
									</tr>
									<tr>
										<th class="no-border">1:00 - 2:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">2:00 - 3:15</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>08: Wireshark with LTE and 5G Packet Core</h4>
											<p>Instructor: <a href="bios/mark-stout">Mark Stout</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">3:30 - 4:45</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>11: Using Wireshark to learn IPv6</h4>
											<p>Instructor: <a href="bios/jeff-carrell">Jeff Carrell</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">5:00 - 6:15</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>14: Hands on Deep Dive</h4>
											<p>Instructor: <a href="bios/hansang-bae">Hansang Bae</a></p>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl23-tab-third" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">10:15 - 11:30</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>03: Dissecting WPA3</h4>
											<p>Instructor: <a href="bios/megumi-takeshita">Megumi Takeshita</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">11:45 - 1:00</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>06: Abusing the Network – An Overview of Malicious Network Activity and How to Detect It</h4>
											<p>Instructor: <a href="bios/phill-shade">Phill Shade</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">1:00 - 2:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">2:00 - 3:15</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>09: LOG4SHELL: Getting to know your adversaries</h4>
											<p>Instructor: <a href="bios/sake-blok">Sake Blok</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">3:30 - 4:45</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>12: DEVELOPER DEN DROP IN (In-person/Zoom/Discord)</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">5:00 - 6:15</th>
										<th class="no-border">
											<h4>Developer Den Drop In (In-person/Zoom/Discord)</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-fifth" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
							<li class="active"><a data-toggle="pill" href="#lvl24-tab-first"><h4>Basie AA1/BB1 (Beginner/Intermediate)</h4></a></li>
							<li><a data-toggle="pill" href="#lvl24-tab-second"><h4>Andy Kirk A/B (Intermediate/Advanced)</h4></a></li>
							<li><a data-toggle="pill" href="#lvl24-tab-third"><h4>Julia Lee A/B (Security and Workshops)</h4></a></li>
						 </ul>
						 <div id="lvl24-tab-first" class="table-responsive container tab-pane fade in active">
					<table class="schedule-table table">
						<tbody>
							<tr>
								<th class="no-border">9:00 - 10:00</th>
								<th class="no-border">
									<h4>Keynote: Introducing Logray - Gerald Combs, Founder, Wireshark; Director of Open Source Projects and Loris Degioanni, CTO and Founder, Sysdig (Basie AA1/BB1)</h4>
								</th>
							</tr>
							<tr>
								<th class="no-border">10:15 - 11:30</th>
								<th class="no-border">
									<img src="img/littlefin.png">
									<h4>15: Contribute to Wireshark: the low hanging fruits</h4>
									<p>Instructor: <a href="#">Uli Heilmeier</a></p>
								</th>
							</tr>
							<tr>
								<th class="no-border">11:45 - 1:00</th>
								<th class="no-border">
									<img src="img/littlefin.png">
									<img src="img/littlefin.png">
									<h4>18: The Packet Doctors are in! Packet trace examinations with the experts</h4>
									<p>Instructors: Drs <a href="bios/hansang-bae">Bae</a>, <a href="bios/sake-blok">Blok</a>, & <a href="bios/jasper-bongertz">Bongertz</a></p>
								</th>
							</tr>
							<tr>
								<th class="no-border">1:00 - 2:00</th>
								<th class="no-border">
									<h4>Lunch</h4>
								</th>
							</tr>
							<tr>
								<th class="no-border">2:00 - 3:15</th>
								<th class="no-border">
									<img src="img/littlefin.png">
									<h4>19: TCP Conversation Completeness – What it is, how to use it.</h4>
									<p>Instructor: <a href="bios/chris-greer.php">Chris Greer</a></p>
								</th>
							</tr>
							<tr>
								<th class="no-border">3:30 - 4:45</th>
								<th class="no-border">
									<img src="img/littlefin.png">
									<h4>22: The Life of a Packet, The art of the trace file synchronization</h4>
									<p>Instructor: <a href="bios/mike-canney">Mike Canney</a></p>
								</th>
							</tr>
							<tr>
								<th class="no-border">5:00 - 6:15</th>
								<th class="no-border">
									<img src="img/littlefin.png">
									<img src="img/littlefin.png">
									<h4>24: 10 Tools I use that Compliment Wireshark</h4>
									<p>Instructor: <a href="bios/anthony-efantis">Anthony Efantis</a></p>
								</th>
							</tr>
							<tr>
								<th class="no-border">6:30 - 8:30</th>
								<th class="no-border">
									<h4>Sponsor Technology Showcase Reception, esPCAPe Group Packet Challenge & Dinner (Basie Foyer)</h4>
								</th>
							</tr>
						</tbody>
					</table>
					</div>
					<div id="lvl24-tab-second" class="table-responsive container tab-pane fade">
						<table class="schedule-table table">
							<tbody>
								<tr>
									<th class="no-border">10:15 - 11:30</th>
									<th class="no-border">
										<img src="img/littlefin.png">
										<h4>16: Analyzing capture files in Python with PyShark</h4>
										<p>Instructor: <a href="#">Dor Green</a></p>
									</th>
								</tr>
								<tr>
									<th class="no-border">1:00 - 2:00</th>
									<th class="no-border">
										<h4>Lunch</h4>
									</th>
								</tr>
								<tr>
									<th class="no-border">2:00 - 3:15</th>
									<th class="no-border">
										<img src="img/littlefin.png">
										<img src="img/littlefin.png">
										<h4>20: TCP SACK Overview and Impact on Performance</h4>
										<p>Instructor: <a href="bios/john-pittle">John Pittle</a></p>
									</th>
								</tr>
								<tr>
									<th class="no-border">3:30 - 4:45</th>
									<th class="no-border">
										<img src="img/littlefin.png">
										<img src="img/littlefin.png">
										<h4>23: WFH Update – Analysis of VPN Network Performance</h4>
										<p>Instructor: <a href="bios/chris-hull">Chris Hull</a></p>
									</th>
								</tr>
								<tr>
									<th class="no-border">5:00 - 6:15</th>
									<th class="no-border">
										<img src="img/littlefin.png">
										<img src="img/littlefin.png">
										<h4>25: Advanced dissector features</h4>
										<p>Instructor: <a href="bios/roland-knall">Roland Knall</a></p>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
					<div id="lvl24-tab-third" class="table-responsive container tab-pane fade">
						<table class="schedule-table table">
							<tbody>
								<tr>
									<th class="no-border">10:15 - 11:30</th>
									<th class="no-border">
										<img src="img/littlefin.png">
										<h4>17: Visualizing and Decrypting TLS 1.3</h4>
										<p>Instructor: <a href="bios/ross-bagurdes">Ross Bagurdes</a></p>
									</th>
								</tr>
								<tr>
									<th class="no-border">1:00 - 2:00</th>
									<th class="no-border">
										<h4>Lunch</h4>
									</th>
								</tr>
								<tr>
									<th class="no-border">2:00 - 3:15</th>
									<th class="no-border">
										<img src="img/littlefin.png">
										<img src="img/littlefin.png">
										<h4>21: Build Your Own Wireshark Learning Lab – Part 1</h4>
										<p>Instructor: <a href="bios/jeff-carrell">Jeff Carrell</a></p>
									</th>
								</tr>
								<tr>
									<th class="no-border">3:30 - 4:45</th>
									<th class="no-border">
										<img src="img/littlefin.png">
										<img src="img/littlefin.png">
										<h4>Build Your Own Wireshark Learning Lab – Part 2</h4>
										<p>Instructor: <a href="bios/jeff-carrell">Jeff Carrell</a></p>
									</th>
								</tr>
								<tr>
									<th class="no-border">5:00 - 6:15</th>
									<th class="no-border">
										<img src="img/littlefin.png">
										<img src="img/littlefin.png">
										<h4>Build Your Own Wireshark Learning Lab – Part 3</h4>
										<p>Instructor: <a href="bios/jeff-carrell">Jeff Carrell</a></p>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				</div>
				<div id="tab-sixth" class="tab-pane fade table-responsive container">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl25-tab-first"><h4>Basie AA1/BB1 (Beginner/Intermediate)</h4></a></li>
						    <li><a data-toggle="pill" href="#lvl25-tab-second"><h4>Andy Kirk A/B (Intermediate/Advanced)</h4></a></li>
							<li><a data-toggle="pill" href="#lvl25-tab-third"><h4>Julia Lee A/B (Security and Workshops)</h4></a></li>
						 </ul>
						 <div id="lvl25-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">9:00 - 10:00</th>
										<th class="no-border">
											<h4>SharkBytes (Basie AA1/BB1)</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">10:15 - 11:30</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<h4>26: Packet Capture 101</h4>
											<p>Instructor: <a href="bios/jasper-bongertz">Jasper Bongertz</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">11:45 - 1:00</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>29: Intro to QUIC - The TCP Killer?</h4>
											<p>Instructor: <a href="bios/chris-greer">Chris Greer</a></p>
										</th>
									</tr> 
									<tr>
										<th class="no-border">1:00 - 2:00</th>
										<th class="no-border">
											<h4>A walkthrough of the SharkFest Group/CTF & Individual Packet Challenges (Basie AA1/BB1)</h4>
										</th>
									</tr>
									
									<tr>
										<th class="no-border">2:00 - 4:00</th>
										<th class="no-border">
											<h4>Closing Remarks, Packet Challenge Awards and Farewell Reception (Basie Foyer)</h4>
										</th>
									</tr>
								</tbody>
							</table>
					</div>
					<div id="lvl25-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">10:15 - 11:30</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<h4>27: When TCP reassembly gets complicated (this session will be on Zoom only)</h4>
											<p>Instructor: <a href="bios/tom-peterson">Tom Peterson</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">11:45 - 1:00</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>30: Hands on Deep Dive</h4>
											<p>Instructor: <a href="bios/hansang-bae">Hansang Bae</a></p>
										</th>
									</tr> 
								</tbody>
							</table>
						</div>
						<div id="lvl25-tab-third" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">10:15 - 11:30</th>
										<th class="no-border">
											<img src="img/littlefin.png">
											<img src="img/littlefin.png">
											<h4>28: When a packet is not a packet</h4>
											<p>Instructor: <a href="bios/mike-kershaw">Mike Kershaw</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">11:45 - 1:00</th>
										<th class="no-border">	
											<h4>31: Developer Den Drop In (In-person/Zoom/Discord)</h4>				
										</th>
									</tr> 
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 
</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>