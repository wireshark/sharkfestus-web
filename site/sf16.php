<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf16-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'16 Retrospective</h1>
            <p>June 13th - 16th, 2016<br>Computer History Museum | Mountain View, California</p>
        </div>
    </div>
    <section id="main-content">
    <!-- Content area -->
    <div class="content-area retrospective-page post-excerpt">

        <!-- PAGE BLOG -->
        <section class="page-section with-sidebar sidebar-right">
        <div class="container">
        <div class="row">

        <!-- Content -->
        <section id="content" class="content col-sm-7 col-md-8">

            <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                <div class="post-media">
                </div>
                <div class="post-header">
                    <div class="post-meta">
                        
                        
                    </div>
                </div>
                <div class="post-body">
                    <div class="post-excerpt">
                        <!--
                        <h3 class="post-title2"><strong>Blogs</strong></h3>
                        <p><a href="http://blog.packet-foo.com/2014/06/sharkfest-2014-recap/">SharkFest 2015 Recap</a> by Jasper Bongertz</p> -->
                        <!--
                        <h3 class="post-title2"><strong><a href="assets/presentations15/packetchallenge.pdf">Packet Challenge</a></strong></h3>
                        <p>Click <a href="assets/presentations15/packetchallenge.zip">HERE</a> to download the Packet Challenge questions/answers and the pcap files!  
                        </p> -->
                            
                        <h3 class="post-title2"><strong>Keynote Presentation</strong></h3>
                        <div class="responsive-iframe">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/x0hCRSfFN78" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <p><strong>The Ancient History of Computers and Network Sniffers</strong><br/>
                        Len Shustek</p>
                        
                        <h3 class="post-title2"><strong>Blogs</strong></h3>
                        <p><a href="https://blog.packet-foo.com/2016/07/sharkfest-2016-recap/">SharkFest'16 Recap</a> by Jasper Bongertz <br>
                        <a href="https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-1-ethernet-basics/">The Network Capture Playbook Part 1</a> by Jasper Bongertz<br>
                        <a href="https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-2-speed-duplex-and-drops/">The Network Capture Playbook Part 2</a> by Jasper Bongertz<br>
                        <a href="https://blog.packet-foo.com/2016/11/the-network-capture-playbook-part-3-network-cards/">The Network Capture Playbook Part 3</a> by Jasper Bongertz<br>
                        <a href="https://blog.packet-foo.com/2016/11/the-network-capture-playbook-part-4-span-port-in-depth/">The Network Capture Playbook Part 4</a> by Jasper Bongertz<br>
                        <a href="https://blog.packet-foo.com/2016/12/the-network-capture-playbook-part-5-network-tap-basics/">The Network Capture Playbook Part 5</a> by Jasper Bongertz<br>
                        </p>

                        <h3 class="post-title2"><strong>SharkBytes</strong></h3>
                        <div class="responsive-iframe">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/3vtZ97BNqvg" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <!--
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Q5gDOozxnY0" frameborder="0" allowfullscreen></iframe>-->
                        
                        
                        
                        
                        <h3 class="post-title2"><strong>Tuesday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li>01: <a href="assets/presentations16/01.pptx">In the Packet Trenches</a> by Hansang Bae</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/22F0Q0tAxTw" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:32)</li>
                        </ul>
                        <li>02:<a href="assets/presentations16/02.pdf">  We Still Don’t Get It! Security is Still Hard</a> by Mike Kershaw</li>
                        
                        <li>03: <a href="assets/presentations16/03.7z"> Writing a Dissector: 3 Ways to Eat Bytes</a> by Graham Bloice</li>
                        <!--<ul>
                        <li><a href="assets/presentations16/grahambloice.zip"> Presentation Materials</a></li>
                        </ul>-->
                        <ul>
                        </ul>
                        <li>04: <a href="assets/presentations16/04.pdf">T-Shark for the Win</a> by Christian Landström</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/CEOMw650D_Y" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:05)</li>
                        </ul>
          
                        <li>05: <a href="assets/presentations16/05.pptx"> TCP Tips, Tricks, and Traces: Let’s Chat About What Makes Applications Crawl</a> by Chris Greer</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/15wDU3Wx1h0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:02:21)</li>
                        </ul>
                        <li>06: <!--<a href="assets/presentations16/06.pdf">--> Analyzing and Re-Implementing a Proprietary Protocol</a> by Jonah Stiennon</li>
                        <li>07: <a href="assets/presentations16/07.pdf">Tackling the Haystack: How to Process Large Numbers of Packets – Part 1</a> by Jasper Bongertz</li>
                        <!--
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/MIPS0RPn-aw"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:05:00)</li>
                        </ul> -->
                        <li>08: <a href="assets/presentations16/08.pdf"> Network Baselining with Wireshark to Identify and Stop Unwanted Communications</a> by Jon Ford</li>
                        
                        <li>09: <a href="assets/presentations16/09.pdf"> Troubleshooting IPv6 with Wireshark – Part 1 </a>by Jeff Carrell</li>
                        <li>10: <a href="assets/presentations16/07.pdf">Tackling the Haystack: How to Process Large Numbers of Packets – Part 2 </a>by Jasper Bongertz</li>
                       
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/8WVahHErC-0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:22)</li>
                        </ul>
                        <li>11: <!--<a href="assets/presentations16/11.pptx">--> Top 10 Wireshark 2 Features </a>by Laura Chappell</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/HTtVHxIh6ww" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:19:03)</li>
                        </ul>-->

                        <li>12: <a href="assets/presentations16/09.pdf">Troubleshooting IPv6 with Wireshark – Part 2 </a>by Jeff Carrell</li>

                        </ul>

                        <h3 class="post-title2"><strong>Wednesday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li> <a href="assets/presentations16/13.pdf">13: Capture Filter Sorcery: How to Use Complex BPF Capture Filters in Wireshark</a> by Sake Blok</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/DS4j9pwVuog" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:06:13)</li>
                        </ul>
                        <li>14: <a href="assets/presentations16/14.pdf"> Cisco ACI and Wireshark: Getting Back Our Data</a> by Karsten Hecker</li>
                        <li>15: <!--<a href="assets/presentations16/15.pdf">-->Adventures in Packet Analysis: Run Wireshark Everywhere!</a> by Maher Adib</li> 
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/pb1yb1eUlgY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:00)</li>
                        </ul> -->
                        <li>16:<a href="assets/presentations16/16.pdf"> Advanced Wireshark Display Filters: How to Zoom in on the 10 Packets You Actually Need </a> by Betty DuBois</li> 
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/EKPef0BFTQY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:22)</li>
                        </ul>
                        <li>17: <a href="assets/presentations16/17.pdf"> Tempering tshark & tcpdump with tmux </a>by Boyd Stephens</li>
                        
                       
                        <li>18: <a href="assets/presentations16/18.pptx"> Determining Topology from a Capture File</a> by Chris Bidwell</li>
                        <li>19: <a href="assets/presentations16/19.pptx" title="Presentation slides" target="_blank">Markers – Beacons in an Ocean of Packets</a> by Matthew York</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/M2SE8hf4h3s" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (54:56)</li>
                        </ul>
                        <li>20: <a href="assets/presentations16/20.pdf" title="Presentation slides" target="_blank">Troubleshooting with Layer 2 Control Protocols </a> by Werner Fischer</li>
                        <li>21: <a href="assets/presentations16/21.pdf">Wireshark 2.0 Tips for HTTP1/2 Analysis</a> by Megumi Takeshita</li>
                        <ul>
                        <li><a href="assets/presentations16/HTTP2.zip"> Presentation Materials</a></li>
                        </ul>
                        <!--<ul>

                        <li class="presVideo"><a href="https://youtu.be/MIPS0RPn-aw"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:05:00)</li>
                        </ul>-->
                        <li>22: <a href="assets/presentations16/22.pdf" title="Presentation slides" target="_blank"> Detection and Verification of IoCs (Indicators of Compromise)</a> by Jasper Bongertz</li> 
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/V-KM4p0x-po" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:00)</li>
                        </ul>
                        <li>23: <a href="assets/presentations16/23.pdf"> Troubleshooting a Multi-Tier Application in a Production Environment</a> by Captain Brad Palm</li>
                        <li>24: <a href="assets/presentations16/24.pptx"> The Packet A(nalysis) Team: Case Studies in Helping Solve Problems with Packet Analysis</a> by Kary Rogers</li>
                        </ul>
                  
                        <h3 class="post-title2"><strong>Thursday Classes</strong></h3>
                        <ul style="list-style:none;">
                        <li>25: <!--<a href="assets/presentations16/25.pdf">-->Troubleshooting in the Large Enterprise – Part 1</a> by Hansang Bae</li> 
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/HGcbhCVZ8MU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:27)</li>
                        </ul> -->
                        <li>26: <a href="assets/presentations16/26.pdf">Forensic Network Analysis in the Time of APTs</a> by Christian Landström</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://youtu.be/CaIAb462a3w" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:36)</li>
                        </ul>-->
                        <li>27:<a href="assets/presentations16/27.pdf"> WiFi Capture and Injection on Various OSes – Revisited </a>  by Thomas D’Otreppe</li>
                        <li>28: <!--<a href="assets/presentations16/28.pdf">-->Troubleshooting in the Large Enterprise – Part 2</a> by Hansang Bae</li>
                       
                        <ul>
                        <li class="presVideo"><a href=" https://youtu.be/dnGdmMCchjk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:49)</li>
                        </ul>
                        <li>29: <!--<a href="assets/presentations16/29.pdf">-->Detecting Suspicious Traffic</a> by Laura Chappell</li>
                        <li>30: <!--<a href="assets/presentations16/30.pdf" title="Presentation slides" target="_blank">-->Learning About Networking by Using Wireshark with GNS3: Learn Safely in an Emulator</a> by John Schreiner</li>
                        <li>31: <a href="assets/presentations16/31.zip" title="Presentation slides" target="_blank"> Using Wireshark Command Line Tools & Scripting</a> by Sake Blok</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/k2PfYOoRiKM" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:21)</li>
                        </ul>
                        <li>32: <a href="assets/presentations16/32.pdf"> SDN/OpenFlow Analysis </a>by Jeff Carrell</li>
                        <li>33: <!--<a href="assets/presentations16/33.pdf">-->Using Wireshark to Prove Root Cause: Real-World Troubleshooting Tales</a> by Graeme Bailey</li>
                        <!--<ul>
                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=7PO-9HxHGdo&t=7m48s" title="Presentation video on YouTube" target="_blank">Presentation Video Part 1</a> (45:25)</li>
                        </ul>-->
                        <li>34: <a href="assets/presentations16/34.pdf">Top 5 False Positives</a> by Jasper Bongertz</li> 
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/GRAY1OceGc4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:26)</li>
                        </ul>
                        <li>35: <a href="assets/presentations16/05.pptx">TCP Tips, Tricks, and Traces: Let’s Chat About What Makes Applications Crawl</a> by Chris Greer</li>
                        <ul>
                        <li class="presVideo"><a href="https://youtu.be/15wDU3Wx1h0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:02:21)</li>
                        </ul>
                        <li>36: <a href="assets/presentations16/02.pdf"> We Still Don’t Get It! Security is Still Hard</a> by Mike Kershaw</li>
                        </ul>  
                        <!--
                        <h3 class="post-title2">Shark Bytes</h3>
                        <iframe width="560" height="316" style="margin: 10px 0;" src="https://www.youtube.com/embed/c119qZnKFsw" frameborder="0" allowfullscreen></iframe>
                        <p>Shark Bytes consist of "little crunchy bits of wisdom". Like the immensely popular TED talks, Shark Bytes aim to inform, inspire, surprise, and delight as presenters share their personal perspective on a topic in under 5 minutes.</p>
                        -->
                    </div>
                </div>
                
            </article>

            <!-- About the author -->
            
            <!-- /About the author -->

            <!-- Comments -->
            
            <!-- /Comments -->

            <!-- Leave a Comment -->
            
            <!-- /Leave a Comment -->

        </section>
        <!-- Content -->

        <hr class="page-divider transparent visible-xs"/>

        <!-- Sidebar -->
        <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
            <div class="background-blue front-column1">
                <div class="grid-cell">
                <h4 class="widget-title">A Word of Thanks</h4>
                <p>The success of SharkFest'16 is due in very large part  to Gerald Combs and the core developers excellent stewardship of the Wireshark open source project, to the tireless efforts of Laura Chappell who provides innumerable hours of expert instruction before and during the conference, to Laura's merry band of packet analysis mentors who guide and inform all comers to The Reef, to the world-class presenters who contribute their time and talent to educate the gathered Wireshark community, to the sponsors who so generously donate their time and resources to make the event possible every year, and to the SharkFest crew of volunteer staff without whom the curtain for this event could not be raised.   Yottabytes of thanks to all these, and to the attendees who show up every year to immerse themselves in this unique educational experience.</p>
                </div>
            </div>
            <div class="widget categories">
                <!--
                <h4 class="widget-title">Past Years</h4>
                <ul>
                    <li><a href="sf14.html">SharkFest'14</a></li>
                    <li><a href="sf13.html">SharkFest'13</a></li>
                    <li><a href="sf12.html">SharkFest'12</a></li>
                    <li><a href="sf11.html">Sharkfest'11</a></li>
                    <li><a href="sf10.html">Sharkfest'10</a></li>
                    <li><a href="sf09.html">Sharkfest'09</a></li>
                    <li><a href="sf08.html">Sharkfest'08</a></li>
                </ul> -->
            </div>
            
            <div class="widget flickr-feed">
                
               

        
        <div class="post-media">
        <a style="font-size: 25px;" data-lightbox="sf16" alt="Click Here to View Pictures from Sharkfest'16!" href='img/sharkfest16gallery/2.jpg'><img src="img/sharkfest16gallery/1.jpg" ></a></div>
            <a href='img/sharkfest16gallery/2.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/3.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/4.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/5.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/6.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/7.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/8.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/133.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/9.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/10.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/11.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/12.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/12.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/13.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/14.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/15.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/16.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/17.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/18.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/19.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/20.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/21.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/23.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/24.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/25.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/26.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/27.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/28.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/29.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/30.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/31.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/32.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/33.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/34.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/35.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/36.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/37.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/38.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/39.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/40.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/41.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/42.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/43.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/44.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/45.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/46.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/47.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/48.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/49.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/50.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/51.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/52.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/53.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/54.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/55.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/56.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/57.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/58.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/59.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/60.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/61.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/62.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/63.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/64.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/65.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/66.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/67.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/68.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/69.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/70.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/71.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/72.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/73.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/74.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/75.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/76.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/77.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/78.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/79.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/80.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/81.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/82.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/83.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/84.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/85.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/86.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/87.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/88.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/89.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/90.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/91.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/92.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/93.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/94.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/95.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/96.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/97.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/98.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/99.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/100.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/101.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/102.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/103.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/104.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/105.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/106.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/107.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/108.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/109.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/110.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/111.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/112.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/114.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/115.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/116.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/117.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/118.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/119.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/120.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/121.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/122.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/123.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/124.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/125.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/126.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/127.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/128.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/129.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/130.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/131.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/132.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/133.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/134.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/135.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/136.jpg' data-lightbox="sf16"></a>
            <a href='img/sharkfest16gallery/137.jpg' data-lightbox="sf16"></a>

            </div>
             <script>
            jQuery('a.gallery').colorbox();
        </script> 

                
        


<!--
            <div class="widget tag-cloud">
                <h4 class="widget-title">Tags</h4>
                <ul>
                    <li><a href="#">Creative</a></li>
                    <li><a href="#">Design</a></li>
                    <li><a href="#">Art</a></li>
                    <li><a href="#">Autpor Name</a></li>
                    <li><a href="#">Coorperate</a></li>
                    <li><a href="#">Wordpress</a></li>
                    <li><a href="#">3D Animations</a></li>
                </ul>
            </div>

            <div class="widget">
                <h4 class="widget-title">Join The Newsletter</h4>
                <form method="post" action="#">
                    <div class="form-group">
                        <input type="text" placeholder="Type and hit enter... " class="form-control" name="search" title="search">
                        <input type="submit" hidden="hidden"/>
                    </div>
                </form>
            </div>

        </aside>
        <!-- Sidebar -->
<div class="background-blue front-column1">
    <div class="grid-cell">
        <div class="sponsor-list">
            <div class="sponsor-imgs">
                <h4 class="widget-title">Sponsors</h4>
                        <p><strong>Host Sponsor</strong></p>
                        <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>

                        <p><strong>Reef Sponsors</strong></p>
                        <a href="http://www.bigswitch.com/" target="_blank"><img class="sponsors" src="img/sponsors/bigswitch-white.png"></a>
                        <a href="http://www.cpacket.com/index.php" target="_blank"><img class="sponsors" src="img/sponsors/cpacket-white.png"></a>
                        <a href="http://www.pluribusnetworks.com/" target="_blank"><img class="sponsors" src="img/sponsors/pluribus-white.png"></a>
                        


                        <p><strong>Angel Shark Sponsors</strong></p>
                        
                        
                        <a href="http://datacomsystems.com/" target="_blank"><img class="sponsors" src="img/sponsors/datacom-white.png"></a>
                        <a href="http://www.dual-comm.com/" target="_blank"><img class="sponsors" src="img/sponsors/dualcomm-white.png"></a>
                        <a href="http://www.garlandtechnology.com/" target="_blank"><img class="sponsors" src="img/sponsors/garland-white.png"></a>
                        <a href="http://www.insidethestack.com/" target="_blank"><img class="sponsors" src="img/sponsors/inside_products_logo.png"></a>
                        <a href="http://www.ixiacom.com/" target="_blank"><img class="sponsors" src="img/sponsors/ixia-white.png"></a>
                        <a href="http://www.niagaranetworks.com/" target="_blank"><img class="sponsors" src="img/sponsors/niagara-white.png"></a>
                        <a href="http://www.tribelab.com/" target="_blank"><img class="sponsors" src="img/sponsors/tribelab.png"></a>
                        

                        <p><strong>In Absentia Sponsors</strong></p>
                        <a href="http://accoladetechnology.com/" target="_blank"><img class="sponsors" src="img/sponsors15/accolade.png"></a>
                        <a href="https://www.toyotechus.com/" target="_blank"><img class="sponsors" src="img/sponsors/toyo-white.png"></a>
                        

                        <p><strong>Honorary Sponsors</strong></p>
                        <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                        <a href="http://www.wiresharktraining.com/" target="_blank"><img class="sponsors" src="img/sponsors/wiresharku-white.png"></a>                     
            </div>
            </div>
        </section>
        </aside>
    
        <!-- /PAGE BLOG -->

    </div>
    <!-- /Content area -->

</div>
</div>
</section>
</div>
</div>
</aside>
</div>
</div>
</section>
</div>
</section>
</div>

    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>



