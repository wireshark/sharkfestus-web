<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf12-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'12 Retrospective</h1>
            <p>June 24th - 27th, 2012<br>UC Berkeley, Clark Kerr Campus | Berkeley, California</p>
        </div>
    </div>
    <section id="main-content">
      <!-- Content area -->
      <div class="content-area retrospective-page">

          <!-- PAGE BLOG -->
          <section class="page-section with-sidebar sidebar-right">
          <div class="container">
          <div class="row">

          <!-- Content -->
          <section id="content" class="content col-sm-7 col-md-8">

              <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                  <div class="post-media">
                  </div>
                  <div class="post-header">
                      <div class="post-meta"> 
                      </div>
                  </div>
                  <div class="post-body">
                      <div class="post-excerpt">
                          <h3 class="post-title2"><strong>Blogs</strong></h3>
                          <p><a href="http://blog.riverbed.com/2012/06/sharkfest-12-photos.html">Event Photos</a></p>

                          <h3 class="post-title2"><strong>Press Release</strong></h3>
                          <p><a href="http://www.riverbed.com/us/company/news/press_releases/2012/press_080612.php">Global Wireshark Developer and User Community Connected Through Successful Sharkfest '12 Educational Conference</a><br>
                          <a href="http://www.riverbed.com/us/company/news/press_releases/2012/press_030812.php">Riverbed Announces Sharkfest 2012</a>
                          </p>

                          <h3 class="post-title2"><strong>Keynote Presentation</strong></h3>
                          
                          <p><a href="assets/presentations/Keynote-Steve_Riley1.pdf" title="Download presentation PDF" target="_blank">In The Cloud, Everything You Think You Know Is Wrong</a>&nbsp;by Steve Riley</p>
                          
                          <h3 style="" class="post-title2"><strong>Beginner/Intermediate Track Presentations</strong></h3>
                          <ul style="list-style:none;">
        <li>BI-1: <a href="sharkfest.12/presentations/BI-1_BI-15_Using_Wireshark_Software_as_an_Applications_Engineer.pdf" title="Download presentation PDF" target="_blank">Using Wireshark Software as an Applications Engineer</a> by Tim Poth</li>
        <li>BI-2: <a href="sharkfest.12/presentations/BI-2_IPv6_Addressing.pdf" title="Download presentation PDF" target="_blank">Introduction to IPv6 Addressing - (Part 1 of 4)</a> by Nalini Elkins</li>
        <li>BI-3: <a href="sharkfest.12/presentations/BI-3_Its_Not_the_Network-The_Value_of_Root_Cause_Analysis.pdf" title="Download presentation PDF" target="_blank">It's Not the Network!  The Value of Root Cause Analysis</a> by Graeme Bailey</li>
        <li>BI-4: <a href="sharkfest.12/presentations/BI-4_Using_Wireshark_Software_with_a_Cloudshark_Plug-in.pdf" title="Download presentation PDF" target="_blank">Using Wireshark Software with a Cloudshark Plug-in</a> by Joe McEachern and Zach Chadwick</li>
        <li>BI-5: <a href="sharkfest.12/presentations/BI-5_ICMPv6.pdf" title="Download presentation PDF" target="_blank">ICMPv6 - (Part 2 of 4)</a> by Nalini Elkins</li>
        <li>BI-6: <a href="sharkfest.12/presentations/BI-6_Wireshark_Software_and_802.11ac_Wireless_Evolution.pdf" title="Download presentation PDF" target="_blank">Wireshark Software and 802.11ac Wireless Evolution</a> by Joe Bardwell</li>
        <li>BI-7: <a href="sharkfest.12/presentations/BI-7_VoIP_Analysis_Fundamentals.pdf" title="Download presentation PDF" target="_blank">VoIP Analysis Fundamentals</a> by Phill Shade</li>
        <li>BI-8a: <a href="sharkfest.12/presentations/BI-8a_Wireshark_Software_Case_Studies-Phill_Shade.pdf" title="Download presentation PDF" target="_blank">Wireshark Software Case Studies</a> by Phill Shade</li>
        <li>BI-8b: <a href="sharkfest.12/presentations/BI-8b_Wireshark_Software_Case_Studies-Tim_Poth.pdf" title="Download presentation PDF" target="_blank">Wireshark Software Case Studies</a> by Tim Poth</li>
        <li>BI-8c: <a href="sharkfest.12/presentations/BI-8c_Wireshark_Software_Case_Studies-Megumi_Takeshita.pdf" title="Download presentation PDF" target="_blank">Wireshark Software Case Studies</a> by Megumi Takeshita</li>
        <li>BI-8d: <a href="sharkfest.12/presentations/BI-8d_Wireshark_Software_Case_Studies-Graeme_Bailey.pdf" title="Download presentation PDF" target="_blank">Wireshark Software Case Studies</a> by Graeme Bailey</li>
        <li>BI-9: <a href="sharkfest.12/presentations/BI-9_Application_Performance_Analysis.pdf" title="Download presentation PDF" target="_blank">Application Performance Analysis using Wireshark Software and Riverbed Technology Cascade® Software</a> by Mike Canney</li>
        <li>BI-10: <a href="sharkfest.12/presentations/BI-10_Build_a_HOT_Security_Profile_Using_Cool_New_Features.pdf" title="Download presentation PDF" target="_blank">Build a HOT Security Profile Using Cool New Features!</a> by Laura Chappell</li>
        <li>BI-11: <a href="sharkfest.12/presentations/BI-11_Inside_the_TCP_Handshake.pdf" title="Download presentation PDF" target="_blank">Inside the TCP Handshake</a> by Betty DuBois, BI-11 <a href="sharkfest.12/presentations/Inside_TCP_handshake_traces.zip" title="Download trace files">Trace Files</a></li>
        <li>BI-12 <a href="sharkfest.12/presentations/BI-12_Wireshark_1.8–16_New_Wireshark_Features_to_Drool_Over.pdf" title="Download presentation PDF" target="_blank">Wireshark 1.8 – 16 New Wireshark Features to Drool Over!</a> by Laura Chappell</li>
        <li>BI-13:<a href="sharkfest.12/presentations/BI-13_IPv6_Transition_Techniques.pdf" title="Download presentation PDF" target="_blank"> IPv6 Transition Techniques (Part 3 of 4)</a> by Nalini Elkins</li>
        <li>BI-14 <a href="sharkfest.12/presentations/BI-14_IPv6_Address_Planning.pdf" title="IPv6 Address Planning" target="_blank">IPv6 Address Planning (Part 4 of 4)</a> by Nalini Elkins</li>
        <li>BI-15: <a href="sharkfest.12/presentations/BI-1_BI-15_Using_Wireshark_Software_as_an_Applications_Engineer.pdf" title="Download presentation PDF" target="_blank">Using Wireshark Software as an Applications Engineer</a> by Tim Poth</li>
        <li>BI-16: <a href="sharkfest.12/presentations/BI-16_Using_Lua_to_Implement_the_CloudShark_Plug-in.pdf" title="Download presentation PDF" target="_blank">Using Lua to Implement the Cloudshark Plug-in</a> by Joe McEachern and Zach Chadwick</li>
        <li>BI-17: <a href="sharkfest.12/presentations/BI-3_Its_Not_the_Network-The_Value_of_Root_Cause_Analysis.pdf" title="Download presentation PDF" target="_blank">It's Not the Network!  The Value of Root Cause Analysis</a> by Graeme Bailey</li>
        <li>BI-18: <a href="sharkfest.12/presentations/BI-18_Understanding_Encryption_Services_Using_Wireshark.pdf" title="Download presentation PDF" target="_blank">Understanding Encryption Services Using Wireshark Software</a> by Larry Greenblatt</li>
      </ul>
            
        
                <h3 class="post-title2"><strong>Advanced Track Presentations</strong></h3>
                
          <ul style="list-style:none;"><li>A-1: <a href="sharkfest.12/presentations/A-1_Deep_Dive_Packet_Analysis.pdf" title="Download presentation PDF" target="_blank">Deep Dive Packet Analysis</a> by Hansang Bae</li>
        <li>A-2: <a href="sharkfest.12/presentations/A-2_Understanding_Encryption_Services_Using_Wireshark.pdf" title="Download presentation PDF" target="_blank">Understanding Encryption Services Using Wireshark Software</a> by Larry Greenblatt</li>
        <li>A-3: <a href="sharkfest.12/presentations/A-3_A-10_Tuning_Win7_Using_Wireshark_TCP_Stream_Graph_a_Case_Study.pdf" title="Download presentation PDF" target="_blank">Tuning Win7 Using Wireshark's TCP Stream Graph (case study)</a> by Rolf Leutert</li>
        <li>A-4: <a href="sharkfest.12/presentations/A-4_Leveraging_Openflow_to_create_a_Large_Scale_and_Cost_Effective_Packet_Capture_Network.pdf" title="Download presentation PDF" target="_blank">Leveraging Openflow to create a Large Scale and Cost Effective, Packet Capture Network</a> by Rich Groves</li>
        <li>A-5: <a href="sharkfest.12/presentations/A-5_Analyzing_WLAN_Roaming_Problems.pdf" title="Download presentation PDF" target="_blank">Analyzing WLAN Roaming Problems (case study)</a> by Rolf Leutert</li>
        <li>A-6: <a href="sharkfest.12/presentations/A-6_Open_WIPS-ng.pdf" title="Download presentation PDF" target="_blank">Open WIPS-ng</a> by Thomas D'Otreppe</li>
        <li>A-7: <a href="sharkfest.12/presentations/A-7_Wireshark_Software_in_the_Large_Enterprise.pdf" title="Download presentation PDF" target="_blank">Wireshark Software in the Large Enterprise</a> by Hansang Bae</li>
        <li>A-8: <a href="sharkfest.12/presentations/A-8_SMB_CIFS_Analysis.pdf" title="Download presentation PDF" target="_blank">SMB/CIFS Analysis</a> by Betty DuBois, A-8 <a href="sharkfest.12/presentations/A-8_CifsTraces.zip" title="Download Trace Files">Traces</a> and <a href="sharkfest.12/presentations/A-8_SharkfestCIFSprofile.zip" title="Download Profile Files">Profile</a> Files</li>
        <li>A-9: <a href="sharkfest.12/presentations/A-9_Visualizing_802.11_WireShark_Data.pdf" title="Download presentation PDF" target="_blank">Spectrum Analysis &amp; Visual Packet Analysis</a> by Ryan Woodings</li>
        <li>A-10: <a href="sharkfest.12/presentations/A-3_A-10_Tuning_Win7_Using_Wireshark_TCP_Stream_Graph_a_Case_Study.pdf" title="Download presentation PDF" target="_blank">Tuning Win7 Using Wireshark's TCP Stream Graph (case study)</a> by Rolf Leutert</li>
        <li>A-11: <a href="sharkfest.12/presentations/A-11_Pervasive_Visibility_in_the_Clouded_Data_Center.pdf" title="Download presentation PDF" target="_blank">Pervasive Visibility in the Clouded Data Center - Distributed Real-Time Monitoring and Wireshark Software Drill Down on-Demand</a> by Rony Kay</li>
        <li>A-12: <a href="sharkfest.12/presentations/A-12_Effects_of_Receiver-Side_Window_Scaling_on_Enterprise_Networks.pdf" title="Download presentation PDF" target="_blank">Effects of Receiver-Side Window Scaling on Enterprise Networks</a> by Christian Landstroem</li>
        <li>A-13: <a href="sharkfest.12/presentations/A-13_A-17_Secrets_of_Vulnerability_Scanning_Nessus_Nmap_and_More.pdf" title="Download presentation PDF" target="_blank">Secrets of Vulnerability Scanning: Nessus, Nmap and More</a> by Ron Bowes</li>
        <li>A-14: <a href="sharkfest.12/presentations/MB-1_SSL_Troubleshooting_with _Wireshark_Software.pdf" title="Download presentation PDF" target="_blank">SSL Troubleshooting with Wireshark Software</a> by Sake Blok, Additional <a href="sharkfest.12/presentations13/MB-2_SSL_Troubleshooting_Hands-on_Lab_Files.tgz">Lab Files</a></li>
        <li>A-15: <a href="sharkfest.12/presentations/A-8_SMB_CIFS_Analysis.pdf" title="Download presentation PDF" target="_blank">SMB/CIFS Analysis</a> by Betty DuBois, A-15 <a href="sharkfest.12/presentations/A-8_CifsTraces.zip" title="Download Trace Files">Traces</a> and <a href="sharkfest.12/presentations/A-8_SharkfestCIFSprofile.zip" title="Download Profile Files">Profile</a> Files</li>
        <li>A-16: <a href="sharkfest.12/presentations/A-1_Deep_Dive_Packet_Analysis.pdf" title="Download presentation PDF" target="_blank">Deep Dive Packet Analysis</a> by Hansang Bae</li>
        <li>A-17: <a href="sharkfest.12/presentations/A-13_A-17_Secrets_of_Vulnerability_Scanning_Nessus_Nmap_and_More.pdf" title="Download presentation PDF" target="_blank">Secrets of Vulnerability Scanning: Nessus, Nmap and More</a> by Ron Bowes</li>
        <li>A-18: <a href="sharkfest.12/presentations/A-18_Effects_of_Receiver-Side_Window_Scaling_on_Enterprise_Networks.pdf" title="Download presentation PDF" target="_blank">Effects of Receiver-Side Window Scaling on Enterprise Networks</a> by Christian Landstroem</li>
      </ul>
                <h3 class="post-title2"><strong>Mixed Bag Track Presentations</strong></h3>
                
          <ul style="list-style:none;"><li>MB-1: <a href="sharkfest.12/presentations/MB-1_SSL_Troubleshooting_with _Wireshark_Software.pdf" title="Download presentation PDF" target="_blank">SSL Troubleshooting with Wireshark Software</a> by Sake Blok</li>
        <li>MB-2: <a href="sharkfest.12/presentations/MB-2_SSL_Troubleshooting_Hands-on_Lab_Files.tgz" title="Download Lab Files">SSL Troubleshooting Hands-on Lab Files</a> by Sake Blok</li>
        <li>MB-3: <a href="sharkfest.12/presentations/MB-3_Trace_File_Case_Files.pdf" title="Download presentation PDF" target="_blank">Trace File Case Files</a> by Jasper Bongertz</li>
        <li>MB-4: <a href="sharkfest.12/presentations/MB-4_More_Trace_Files_Case_Files_Hands-on_Lab.pdf" title="Download presentation PDF" target="_blank">More Trace Files Case Files - Hands-on Lab</a> by Jasper Bongertz</li>
        <li>MB-5: <a href="sharkfest.12/presentations/MB-5_Troubleshooting_from_the_Field.pdf" title="Download presentation PDF" target="_blank">Troubleshooting from the Field</a> by Herbert Grabmayer</li>
        <li>MB-6: <a href="sharkfest.12/presentations/MB-6_Introduction_to_WiFi_Security_and_Aircrack-ng.pdf" title="Download presentation PDF" target="_blank">Introduction to WiFi Security and Aircrack-ng</a> by Thomas D'Otreppe, MB-6 <a href="sharkfest.12/presentations/MB-6_Network_Interaction.pcap.zip" title="Download the presentation trace files">Trace Files</a></li>
        <li>MB-7: <a href="sharkfest.12/presentations/MB-7_Network_Forensics_Analysis-A_Hands-on_Look.pdf" title="Download presentation PDF" target="_blank">Network Forensics Analysis: A Hands-On Look at the New Paradigm in Network Security</a> by Phill Shade</li>
        <li>MB-8: <a href="sharkfest.12/presentations/MB-8_Powershell-The_New_Command_Shell_for_Windows_in_Combination_with_T-Shark.pdf" title="Download presentation PDF" target="_blank">Powershell: The New Command Shell for Windows in Combination with T-Shark</a> by Graham Bloice, MB-8 <a href="sharkfest.12/presentations/MB-8-Captures.zip" title="Download MB-8 sample capture files" target="_blank">Presentation Capture Files</a></li>
        <li>MB-11: <a href="sharkfest.12/presentations/MB-11_Trace_File_Case_Files.pdf" title="Download presentation PDF" target="_blank">Trace File Case Files</a> by Jasper Bongertz</li>
        <li>MB-12: <a href="sharkfest.12/presentations/MB-12_Trace_File_Case_Files-Hands_On_Lab.pdf" title="Download presentation PDF" target="_blank">More Trace File Case Files</a> by Jasper Bongertz</li>
        <li>MB-13: <a href="https://kismetwireless.net/sharkfest/" title="Presentations, videos and sample files at the Kismet Wireless website" target="_blank">Building Your Own Wireless Packet Capture Platform</a> by Mike Kershaw</li>
        <li>MB-14: <a href="sharkfest.12/presentations/MB-14_Whats_Old_is_New_Again–Evolving_Network_Security_Threats.pdf" title="IPv6 Address Planning" target="_blank">What's Old is New Again: Evolving Network Security Threats</a> by Phill Shade</li>
      </ul>
                          

                      </div>
                  </div>
                  
              </article>

              <!-- About the author -->
              
              <!-- /About the author -->

              <!-- Comments -->
              
              <!-- /Comments -->

              <!-- Leave a Comment -->
              
              <!-- /Leave a Comment -->

          </section>
          <!-- Content -->

          <hr class="page-divider transparent visible-xs"/>

          <!-- Sidebar -->
      <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
        <div class="background-blue front-column1">
          <div class="grid-cell">
            <h4 class="widget-title">A Word of Thanks</h4>
            <p>Another Sharkfest has come and gone, and we thank each participant, sponsor, presenter, keynote speaker, coordinator, caterer, staff and general support person for making this a successful, richly educational event once again. All session presentations are available from this page, and recordings of many sessions can be found at www.lovemytool.com.</p>
          </div>
        </div>
                <div class="widget categories">
                    <!--
                    <h4 class="widget-title">Past Years</h4>
                    <ul>
                        <li><a href="sf14.html">SharkFest'14</a></li>
                        <li><a href="sf13.html">SharkFest'13</a></li>
                        <li><a href="sf12.html">SharkFest'12</a></li>
                        <li><a href="sf11.html">Sharkfest'11</a></li>
                        <li><a href="sf10.html">Sharkfest'10</a></li>
                        <li><a href="sf09.html">Sharkfest'09</a></li>
                        <li><a href="sf08.html">Sharkfest'08</a></li>
                    </ul> -->
                </div>
          <div class="background-blue front-column1">
            <div class="grid-cell">
              <div class="sponsor-list">
                <div class="sponsor-imgs">
                  <h4><strong>Platinum Level</strong></h4>
                  <a href="http://www.cpacket.com/index.php" target="_blank"><img class="sponsors" src="img/sponsors/cpacket-white.png"></a>
                  <a href="http://www.metageek.net/" target="_blank"><img src="img/sponsors/metageek_logo.png">

                  <h4><strong>Silver Level</strong></h4>
                  
                  <a href="http://www.gigamon.com/" target="_blank"><img class="sponsors" src="img/sponsors/gigamon_logo.png"></a>
                  <a href="http://appliance.cloudshark.org/" target="_blank"><img class="sponsors" width="129" src="img/sponsors/cloudshark_logo.png"></a>
                  <a href="http://www.dual-comm.com/" target="_blank"><img class="sponsors" src="img/sponsors/dualcomm-white.png"></a>
                 
                  <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                  <a href="http://www.napatech.com/" target="_blank"><img class="sponsors" src="img/sponsors/napatech-white.png"></a>
                   <a href="http://www.insidethestack.com/" target="_blank"><img class="sponsors" src="img/sponsors/inside-white.png"></a>
                  <a href="http://www.wiresharktraining.com/" target="_blank"><img class="sponsors" src="img/sponsors/wiresharku-white.png"></a>
                 
                  <a href="http://www.bigswitch.com/" target="_blank"><img class="sponsors" width="130" src="img/sponsors/bigswitch-white.png"></a>
                 
                  

                  <h4><strong>Hosting Sponsor</strong></h4>
                  <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>
                </a>
              </div>
            </div>
          </div>
        </div>


  <!--
              <div class="widget tag-cloud">
                  <h4 class="widget-title">Tags</h4>
                  <ul>
                      <li><a href="#">Creative</a></li>
                      <li><a href="#">Design</a></li>
                      <li><a href="#">Art</a></li>
                      <li><a href="#">Autpor Name</a></li>
                      <li><a href="#">Coorperate</a></li>
                      <li><a href="#">Wordpress</a></li>
                      <li><a href="#">3D Animations</a></li>
                  </ul>
              </div>

              <div class="widget">
                  <h4 class="widget-title">Join The Newsletter</h4>
                  <form method="post" action="#">
                      <div class="form-group">
                          <input type="text" placeholder="Type and hit enter... " class="form-control" name="search" title="search">
                          <input type="submit" hidden="hidden"/>
                      </div>
                  </form>
              </div>

          </aside>
          <!-- Sidebar -->

          </div>
          </div>
          </section>
          <!-- /PAGE BLOG -->

      </div>
      <!-- /Content area -->
</a>
</div>
</aside>
</div>
</div>
</section>
</div>
</section>
</div>
    <!-- FOOTER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
    