<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<section id="health-and-safety">
    <div class="container grid-container">
        <div class="row">
            <h1>Health and Safety at SharkFest</h1>
            <p><i>Please note that we will update our COVID policy based on current circumstances, as well as venue restrictions and requirements, closer to the conference dates.</i></p>
            <p>The health and safety of the Wireshark community is our top priority.<br /> Please review the steps we are taking to ensure a safe environment for attendees who choose to join us in-person.</p>

            <h4>Healthy Measures at SharkFest'22 US</h4>
            <ul>
                <li>We require all those attending in-person to be fully vaccinated and up to date on their COVID-19 vaccinations which includes receiving a COVID-19 Booster. Exceptions will be made for people unable to be vaccinated for medical reasons.</li>
                <li>We will also require all in-person attendees, vendors, and staff to wear a <strong>well-fitting</strong> mask while indoors</li>
                <li>We will comply fully with the Kansas City Marriott Downtown commitment to Clean & Safe Events: <a href="https://whattoexpect.marriott.com/mcidt" target="_blank" rel="noreferrer" ref="_">https://whattoexpect.marriott.com/mcidt</a></li>
                <li>Anyone experiencing cold or flu-like symptoms should stay home and get well</li>
                <li>Hand sanitizing stations will be available throughout the venue</li>
            </ul>
            <p>We will be monitoring the situation carefully and will add updates as it evolves. If you have any questions or concerns, please do not hesitate to contact us at <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a></p>
        </div>
    </div>
</section>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>