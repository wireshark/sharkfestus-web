<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>
<div class="text-centered pwd-protected-page">
    <section id="main-content">
        <div class="about-container">
            <h3>Full Playlist of SF21VUS Sessions on YouTube:</h3>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqPcQWL3uRIq81ONbO28Pbb3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </section>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>