<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf13-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'13 Retrospective</h1>
            <p>June 16th - 19th, 2013<br>UC Berkeley, Clark Kerr Campus | Berkeley, California</p>
        </div>
    </div>
    <section id="main-content">
      <!-- Content area -->
      <div class="content-area retrospective-page">

          <!-- PAGE BLOG -->
          <section class="page-section with-sidebar sidebar-right">
          <div class="container">
          <div class="row">

          <!-- Content -->
          <section id="content" class="content col-sm-7 col-md-8">

              <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                  <div class="post-media">
                  </div>
                  <div class="post-header">
                      <h3 class="post-title"></h3>
                      <div class="post-meta"> 
                      </div>
                  </div>
                  <div class="post-body">
                      <div class="post-excerpt">
                          <h3 class="post-title2"><strong>Blogs</strong></h3>
                          <p><a href="http://blog.riverbed.com/2013/06/gerald-combs-your-network-is-not-a-black-box-.html">Your Network is Not a Black Box</a> by Gerald Combs<br>
                          <a href="http://www.riverbed.com/blogs/12-ways-to-go-deep-with-wireshark-at-sharkfest-2013.html">12 Ways to Go Deep with Wireshark at SharkFest 2013</a> by Dormain Drewitz<br>
                          <a href="http://blog.riverbed.com/2013/02/wireshark-at-15-how-to-start-a-project-that-lasts.html">Wireshark at 15: How to Start a Project That Lasts</a> by Liz Padula<br>
                         <a href="http://blog.riverbed.com/2012/10/wireshark-tutorial-series.html">Wireshark Tips and Tricks Used by Insiders and Veterans</a> by Hansang Bae</p>

                          <h3 class="post-title2"><strong>Press Release</strong></h3>
                          <p><a href="http://www.riverbed.com/about/news-articles/press-releases/sharkfest-2013-marks-15th-anniversary-of-wireshark.html">SharkFest 2013 Marks 15th Anniversary of Wireshark</a></p>

                          <h3 class="post-title2"><strong>Packet Challenge</strong></h3>
                          <p>The SharkFest 2013 <a href="http://www.wiresharktraining.com/sharkfest2013challenge.html">Packet Challenge answer key </a>is online at Wireshark University.</p>
                          <p>CONGRATULATIONS to the winners of the Packet Challenge (and an AirPcap Nx and Cascade Pilot PE license):
                          Clay Maddox, University of North Florida (100%, but missed the bonus Challenge #7)
                          Gareth Sydie, G-Research (Missed 1 Challenge but nailed the bonus one)</p>

                          <h3 class="post-title2"><strong>Keynote Presentations</strong></h3>
                          <div class="responsive-iframe">
                            <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/xwZ_9bLC2ns" frameborder="0" allowfullscreen></iframe>
                          </div>
                          <p><strong>History of the Wireshark Project - Reminiscing on the 15-Year-Old Project</strong><br/>
                          Gerald Combs, Ethereal/Wireshark Project Founder + Special Guests</p>
                          <div class="responsive-iframe">
                            <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/YhqrtGlmfhQ" frameborder="0" allowfullscreen></iframe>
                          </div>
                          <p><strong>Ode to Gerald: "Gerald Combs, Man...Myth...Mystery"</strong><br/>
                          A Wireshark University Production</p>
                          <div class="responsive-iframe">
                            <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/rzHGrCCY8ow" frameborder="0" allowfullscreen></iframe>
                          </div>
                          <p><strong>"The Nice Thing About Standards is That There are so Many of Them!” Musings of an Early Networker</strong><br/>
                          Rich Seifert, M.S.E.E., M.B.A., J.D., President of Networks and Communications Consulting</p>
                          <div class="responsive-iframe">
                            <iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/St8RR87F1MU" frameborder="0" allowfullscreen></iframe>
                          </div>
                          <p><strong>To Engineer is Human…So is Being Lazy: A Conversation on Network Vulnerability</strong><br/>
                          Charles Kaplan, Senior Technical Director, Office of the CTO, Riverbed</p>
                          
                          <h3 style="" class="post-title2"><strong>Network &amp; Application Performance Track Presentations</strong></h3>
                          <ul style="list-style:none;">
            <li>NAP-01: <a href="sharkfest.13/presentations/NAP-01_Application-Performance-Analysis_Mike-Canney.pdf" title="Presentation slides" target="_blank">Application Performance Analysis</a> by Mike Canney</li>
            <li>NAP-02: <a href="sharkfest.13/presentations/NAP-02_Its-Not-the-Network_The-Value-of-Root-Cause-Analysis_Graeme-Bailey.pdf" title="Presentation slides" target="_blank">It's Not the Network! The Value of Root Cause Analysis</a> by Graeme Bailey</li>
                    <li>NAP-03: <a href="sharkfest.13/presentations/NAP-03_Microsoft-SMB-Troubleshooting_Rolf-Leutert.pdf" title="Presentation slides" target="_blank">Microsoft SMB Troubleshooting</a> by Rolf Leutert</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/XbvFXSPig-w" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:59)</li>
            </ul>
                    <li>NAP-04: <a href="sharkfest.13/presentations/NAP-04_Wireless-Network-Optimization_Trent-Cutler.pdf" title="Presentation slides" target="_blank">Wireless Network Optimization</a> by Trent Cutler</li>
                    <li>NAP-05: <a href="sharkfest.13/presentations/NAP-05_Correlating-Traces-From-Multiple-Tiers_Paul-Offord.pdf" title="Presentation slides" target="_blank">Correlating Traces From Multiple Tiers</a> by Paul Offord</li><ul>
              <li class="presVideo"><a href="http://youtu.be/MYler-T9yGk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:06:33)</li>
            </ul>
            <li>NAP-06: Why Pilot? by Janice Spampinato and Martin Lewald</li>
            <ul>
              <li class="presVideo"><a href="http://youtu.be/YUqjPEUyzbk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:04:04)</li>
            </ul>
                    <li>NAP-08: <a href="sharkfest.13/presentations/NAP-08_Using-Wireshark-as-an-Application-Engineer_Tim-Poth.pdf" title="Presentation slides" target="_blank">Using Wireshark as an Application Engineer</a> by Tim Poth</li>
                    <ul>
              <li class="presVideo"><a href="sharkfest.13/presentations/NAP-08_Using-Wireshark-as-an-Application-Engineer_Poth_Trace-files.7z" title="Presentation trace files" target="_blank">Presentation Trace Files</a></li>
            </ul>
            <li>NAP-09: <a href="sharkfest.13/presentations/NAP-01_Application-Performance-Analysis_Mike-Canney.pdf" title="Presentation slides" target="_blank">Application Performance Analysis</a> by Mike Canney</li>
            
            <li>NAP-10: <a href="sharkfest.13/presentations/NAP-10_Enabling-Visibility-for-Wireshark-Across-Physical,-Virtual-and-SDN_Patrick-Leong.pdf" title="Presentation slides" target="_blank">Visibility for Wireshark Across Physical, Virtual and SDN</a> by Patrick Leong</li>
            <ul style="list-style:none;">
              <li class="presVideo"><a href="http://youtu.be/k7JtruMSPjk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:07)</li>
            </ul>
                    <li>NAP-11: <a href="sharkfest.13/presentations/NAP-11_Expanding-Wireshark-Beyond-Ethernet-and-Network-Interfaces_Kershaw-Ryan.pdf" title="Presentation slides" target="_blank">Expanding Wireshark Beyond Ethernet and Network Interfaces</a> <br />
                    by Mike Kershaw and Mike Ryan</li>
                    <ul style="list-style:none;">
              <li class="presVideo"><a href="http://youtu.be/Nn84T506SwU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (44:16)</li>
            </ul>
                    <li>NAP-12: <a href="sharkfest.13/presentations/NAP-12_Packet-Optimization-and-Visibility-using-Wireshark-and-pcaps_Gordon-Beith.pdf" title="Presentation slides" target="_blank">Packet Optimization and Visibility using Wireshark and pcaps</a> by Gordon Beith</li>
                    <li>NAP-13: Wireshark Users Ask the Experts! Moderated by Chris Bidwell</li>
                    <ul style="list-style:none;">
              <li class="presVideo"><a href="http://youtu.be/G7Fg6cwNCaA" title="Presentation audio on YouTube" target="_blank">Presentation Audio</a> (58:10)</li>
            </ul>
                    <li>NAP-14: <a href="sharkfest.13/presentations/NAP-14_Accessing-Packet-Traces-from-Multiple-Locations_Bill-Eastman.pdf" title="Presentation slides" target="_blank">Accessing Packet Traces from Multiple Locations</a> by Bill Eastman</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/5Iw_66UbD6A" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (52:53)</li>
            </ul>
                    <li>NAP-15: <a href="sharkfest.13/presentations/NAP-15_Understanding-Wireshark's-Reassembly-Features_Christian-Landstrom.pdf" title="Presentation slides" target="_blank">Understanding Wireshark's Reassembly Features</a> by Christian Landström</li>
                    <li>NAP-16: <a href="sharkfest.13/presentations/PA-05_Capture-Limitations-of-a-Laptop--When-Does-It-Start-Dropping-Packets_Chris-Greer.pdf" title="Presentation slides" target="_blank">Limitations of a Laptop: When Does It Start Dropping Packets?</a> by Chris Greer</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/_H7PjWqKV0Q" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (45:02)</li>
            </ul>
            <li>NAP-17: <a href="sharkfest.13/presentations/NAP-17_Network-Virtualization--the-SDN-You-REALLY-Want_Steve-Riley.pdf" title="Presentation slides" target="_blank">Network Virtualization: the SDN You REALLY Want</a> by Steve Riley</li>
                  </ul>
        
                <h3 class="post-title2"><strong>Packet Analysis Presentations</strong></h3>
                
          <ul style="list-style:none;">
                    <li>PA-01: <a href="sharkfest.13/presentations/PA-01_Deep-Dive-Packet-Analysis_Hansang-Bae.pdf" title="Presentation slides" target="_blank">Deep-Dive-Packet-Analysis</a> by Hansang Bae</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/w-hTNGZRaSQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:37)</li>
            </ul>
                    <li>PA-02: <a href="sharkfest.13/presentations/PA-02_Introduction-to-IPv6-Addressing_Nalini-Elkins.pdf" title="Presentation slides" target="_blank">Introduction to IPv6 Addressing</a> by Nalini Elkins</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/cd4tu_kVQt0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:54)</li>
            </ul>
            <li>PA-03: <a href="presentations/PA-03_Debugging-Wireless-with-Wireshark-Including-Large-Trace-Files-AirPcap-and-Cascade-Pilot_Megumi-Takeshita.pdf" title="Presentation slides" target="_blank">Debugging Wireless with Wireshark Including Large Trace Files, AirPcap & Cascade Pilot</a> by Megumi Takeshita</li>
                    <li>PA-04:<a href="sharkfest.13/presentations/PA-04_Inside-the-TCP-Handshake_Betty-DuBois.pdf" title="Presentation slides" target="_blank"> Inside the TCP Handshake</a> by Betty DuBois</li>
                    <ul>
              <li class="presVideo" style="margin-bottom:0;"><a href="http://youtu.be/HGcbhCVZ8MU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:28)</li>
              
              <li class="presVideo"><a href="http://tinyurl.com/tcptraces" title="Presentation trace files" target="_blank">Presentation Trace Files</a></li>
            </ul>
             <li>PA-05: <a href="sharkfest.13/presentations/PA-05_Capture-Limitations-of-a-Laptop--When-Does-It-Start-Dropping-Packets_Chris-Greer.pdf" title="Presentation slides" target="_blank">Limitations of a Laptop: When Does It Start Dropping Packets?</a> by Chris Greer</li>
             <ul>
              <li class="presVideo"><a href="http://youtu.be/_H7PjWqKV0Q" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (45:02)</li>
            </ul>
                    <li>PA-07: <a href="sharkfest.13/presentations/PA-07_Troubleshooting-from-the-field_Herbert-Grabmayer.pdf" title="Presentation slides" target="_blank">Troubleshooting From the Field</a> by Herbert Grabmayer</li>
                    
                    <li>PA-08: <a href="sharkfest.13/presentations/PA-08_IPv6-Address-Planning_Nalini-Elkins.pdf" title="Presentation slides" target="_blank">IPv6 Address Planning</a> by Nalini Elkins</li>
                    <li>PA-10: <a href="sharkfest.13/presentations/PA-10_Writing-a-Wireshark-Dissector_Graham-Bloice.zip" title="Presentation slides and trace files" target="_blank">Writing a Wireshark Dissector</a> by Graham Bloice
              (Zip archive)</li>
              <li>PA-11: <a href="sharkfest.13/presentations/PA-11_How-to-Use-Wireshark-to-Analyze-Video_Betty-DuBois.pdf" title="Presentation slides" target="_blank">How to Use Wireshark to Analyze Video</a> by Betty DuBois</li>
              <ul>
              <li class="presVideo"><a href="http://tinyurl.com/tcptraces" title="Presentation trace files" target="_blank">Presentation Trace Files</a></li>
            </ul>
            <li>PA-12: <a href="sharkfest.13/presentations/PA-12_WLAN-Troubleshooting-with-Wireshark-and-AirPcap_Rolf-Leutert.pdf" title="Presentation slides" target="_blank">WLAN Troubleshooting with Wireshark and AirPcap</a> by Rolf Leutert</li>
            <li>PA-13: <a href="sharkfest.13/presentations/PA-13_IPv6-Trace-Analysis-Using-Wireshark_Nalini-Elkins.pdf" title="Presentation slides" target="_blank">Trace Analysis Using Wireshark</a> by Nalini Elkins</li>
                    <li>PA-14: <a href="sharkfest.13/presentations/PA-14_Top-5-False-Positives-when-Analyzing-Networks_Jasper-Bongertz.pdf" title="Presentation slides" target="_blank">Top 5 False Positives when Analyzing Networks</a> by Jasper Bongertz</li>
                    <li>PA-15: So You've Found the Suspect Traffic, But What's Causing It? by Graeme Bailey</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/X9xGs7sQpPY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:29)</li>
            </ul>
                    <li>PA-16: <a href="sharkfest.13/presentations/PA-16_Wireshark-in-the-Large-Enterprise_Hansang-Bae.pdf" title="Presentation slides" target="_blank">Wireshark in the Large Enterprise</a> by Hansang Bae</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/0TTgVmzKUqo" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:19:04)</li>
            </ul>
            <li>PA-17:<a href="sharkfest.13/presentations/PA-17_TCP-Performance-Problem-Analysis-Through-Multiple-Network-Segments_Bongertz-and-Landstrom.pdf" title="Presentation slides" target="_blank"> TCP Performance Problem Analysis Through Multiple Network Segments</a> by Jasper Bongoertz and Christian Landström</li>
                  </ul>
        
                <h3 class="post-title2"><strong>Security Presentations</strong></h3>
                
          <ul style="list-style:none;">
          <li>SEC-01: <a href="sharkfest.13/presentations/SEC-01_Understanding-Encryption-Services-using-Wireshark_Larry-Greenblatt.pdf" title="Presentation slides" target="_blank">Understanding Encryption Services using Wireshark</a> by Larry Greenblatt</li>
          <ul>
              <li class="presVideo"><a href="http://youtu.be/0i192lYNxJs" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (44:50)</li>
            </ul>
          <li>SEC-02: <a href="sharkfest.13/presentations/SEC-02_VoIP-Fundamentals_Phill-Shade.pdf" title="Presentation slides" target="_blank">VoIP Fundamentals</a> by Phill Shade</li>
                    <li>SEC-03: <a href="sharkfest.13/presentations/SEC-03_IPv6-Security-Assessment-Tools-and-Infrastructure-Mitigation_Jeff-Carrell.pdf" title="Presentation slides" target="_blank">IPv6 Security Assessment Tools & Infrastructure Mitigation</a> by Jeff-Carrell</li>
                    <li>SEC-04: <a href="sharkfest.13/presentations/SEC-04_Trace-File-Sanitization-NG_Jasper-Bongertz.pdf" title="Presentation slides" target="_blank">Trace File Sanitization NG</a> by Jasper Bongertz</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/WZLz8meMNgI" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:21:21)</li>
            </ul>
                    <li>SEC-05: <a href="sharkfest.13/presentations/SEC-05_Using-Wireshark-to-Gather-Forensic-Evidence-on-Malware-Outbreaks-in-Enterprise-Networks_Christian-Landstrom.pdf" title="Presentation slides" target="_blank">Using Wireshark to Gather Forensic Evidence on Malware Outbreaks in Enterprise Networks</a> by Christian Landström</li>
                    <li>SEC-06: <a href="sharkfest.13/presentations/SEC-06_I-Can-Hear-You-Tunneling_Alex-Weber.pdf" title="Presentation slides" target="_blank">I Can Hear You Tunneling…</a> by Alex Weber</li>
                    <ul>
              <li class="presVideo"><a href="sharkfest.13/presentations/I-Can-Hear-You-Tunneling_Alex-Weber.mp4" title="Presentation video" target="_blank">Presentation Video</a> (51:23)</li>
            </ul>
            <li>SEC-07: <a href="sharkfest.13/presentations/SEC-07_Wireshark-Network-Forensics_Laura-Chappell.zip" title="Presentation slides and trace files" target="_blank">Wireshark Network Forensics</a> by Laura Chappell (Zip archive)</li>
             <ul>
              <li class="presVideo"><a href="http://youtu.be/UXAHvwouk6Q" title="Presentation video" target="_blank">Presentation Video</a> (1:16:39)</li>
            </ul>
                    <li>SEC-08: <a href="sharkfest.13/presentations/SEC-08_Why-is-Cryptography-So-Hard-to-Get-Right_Ron-Bowes.pdf" title="Presentation slides" target="_blank">Why is Cryptography So Hard to Get Right</a> by Ron Bowes</li>
                    <li>SEC-09: <a href="sharkfest.13/presentations/SEC-09_Attack-Trends-and-Techniques_Steve-Riley.pdf" title="Presentation slides" target="_blank">Attack Trends & Techniques</a> by Steve Riley</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/6RGk-Q-S6Zs" title="Presentation video" target="_blank">Presentation Video</a> (1:09:15)</li>
            </ul>
                    <li>SEC-10: Wireshark Network Forensics by Laura Chappell</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/UXAHvwouk6Q" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:38)</li>
            </ul>
                    <li>SEC-11: <a href="sharkfest.13/presentations/SEC-11_IPv6-Security_Nalini-Elkins.pdf" title="Presentation slides and trace files" target="_blank">IPv6 Security</a> by Nalini Elkins</li>
                    <li>SEC-12: <a href="sharkfest.13/presentations/SEC-04_Trace-File-Sanitization-NG_Jasper-Bongertz.pdf" title="Presentation slides" target="_blank">Trace File Sanitization NG</a> by Jasper Bongertz</li>
                    <ul>
              <li class="presVideo"><a href="http://youtu.be/WZLz8meMNgI" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:21:21)</li>
            </ul>
                    <li>SEC-13: <a href="sharkfest.13/presentations/SEC-13_How-802.11ac-Will-Hide-Problems-From-Wireshark_Joe-Bardwell.pdf" title="Presentation slides" target="_blank">How 802.11ac Will Hide Problems From Wireshark</a> by Joe Bardwell</li>
                    <li>SEC-14: <a href="sharkfest.13/presentations/SEC-01_Understanding-Encryption-Services-using-Wireshark_Larry-Greenblatt.pdf" title="Presentation slides" target="_blank">Understanding Encryption Services using Wireshark</a> by Larry Greenblatt</li>
          <ul>
              <li class="presVideo"><a href="http://youtu.be/0i192lYNxJs" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (44:50)</li>
            </ul>
                    <li>SEC-15: <a href="sharkfest.13/presentations/SEC-15_Why-is-Cryptography-So-Hard-to-Get-Right_Ron-Bowes.pdf" title="Presentation slides" target="_blank">Why is Cryptography So Hard to Get Right</a> by Ron Bowes</li>
                    <li>SEC-16: <a href="sharkfest.13/presentations/SEC-16_I-Can-Hear-You-Tunneling_Alex-Weber.pdf" title="Presentation slides" target="_blank">I Can Hear You Tunneling…</a> by Alex Weber</li>
                    <ul>
              <li class="presVideo"><a href="sharkfest.13/presentations/I-Can-Hear-You-Tunneling_Alex-Weber.mp4" title="Presentation video" target="_blank">Presentation Video</a> (51:23)</li>
            </ul>
                    <li>SEC-17: <a href="sharkfest.13/presentations/SEC-17_Wireless-Intrusion-Detection_Mike-Kershaw.pdf" title="Presentation slides" target="_blank">Wireless Intrusion Detection</a> by Mike Kershaw</li>
                  </ul>
                          

                      </div>
                  </div>
                  
              </article>

              <!-- About the author -->
              
              <!-- /About the author -->

              <!-- Comments -->
              
              <!-- /Comments -->

              <!-- Leave a Comment -->
              
              <!-- /Leave a Comment -->

          </section>
          <!-- Content -->

          <hr class="page-divider transparent visible-xs"/>

          <!-- Sidebar -->
      <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
        <div class="background-blue front-column1">
          <div class="grid-cell">
            <h4 class="widget-title">A Word of Thanks</h4>
            <p>SharkFest’13 has been a smashing event, if post-conference feedback from attendees is to be believed. For this we must thank all presenters for sharing their expertise and knowledge, thank the Wireshark core developers for making our virgin HACKATHON attempt so worthwhile, thank Laura Chappell for holding her inaugural WCNA Bootcamp in conjunction with our conference, thank the attendees for their generous participation, fellowship and camaraderie, thank the staff for working tirelessly to make this a truly wonderful experience for attendees, thank the UC Berkeley Conference Services group for helping ease the process of event planning, and thank Gerald Combs for giving us such a wonderful reason to get together and lift each other up every year.</p>
          </div>
        </div>  
          <div class="widget flickr-feed">
                <div class="post-media">
            <a style="font-size: 25px;" data-lightbox="sf13" alt="Click Here to View Pictures from Sharkfest'14!" href='img/gallery13/6.jpg'><img src="img/sharkfest14gallery/retrogallerylink.jpg" ></a></div>
                      <a data-lightbox="sf13" href='img/gallery13/1.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/6.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/7.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/8.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/9.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/10.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/11.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/12.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/23.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/14.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/15.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/16.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/17.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/18.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/19.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/20.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/25.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/26.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/22.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/28.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/29.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/30.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/31.jpg'></a>
                      <!--<a data-lightbox="sf13" href='img/sharkfest14gallery/sharkfest32.jpg'><img src="img/shark</a>-->
                      <a data-lightbox="sf13" href='img/gallery13/33.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/34.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/2.jpg'></a>
                      <!--<a data-lightbox="sf13" href='img/sharkfest14gallery/sharkfest36.jpg'><img src="img/shark</a>
                      <!--<a data-lightbox="sf13" href='img/sharkfest14gallery/sharkfest40.jpg'><img src="img/shark</a>-->
                      
                      <a data-lightbox="sf13" href='img/gallery13/3.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/4.jpg'></a>
                      <a data-lightbox="sf13" href='img/gallery13/5.jpg'></a>

                      <!--<li><a data-lightbox="sf13" href='img/sharkfest14gallery/sharkfest45.jpg'><img src="img/sharkfest14gallery/sharkfest45.jpg"></a></li>
                      <!--<li><a data-lightbox="sf13" href='img/sharkfest14gallery/sharkfest46.jpg'><img src="img/sharkfest14gallery/sharkfest46.jpg"></a></li>-->
                  </ul>
          <div class="background-blue front-column1">
            <div class="grid-cell">
              <div class="sponsor-list">
                <div class="sponsor-imgs">
                  <h4><strong>Platinum Level</strong></h4>
                  <a href="http://www.cpacket.com/index.php" target="_blank"><img class="sponsors" src="img/sponsors/cpacket-white.png"></a>

                  <h4><strong>Gold Level</strong></h4>
                  <a href="http://www.vssmonitoring.com/" target="_blank"><img class="sponsors" width="125px;" src="img/sponsors/vss_logo.png"></a>
                  <a href="http://www.gigamon.com/" target="_blank"><img class="sponsors" src="img/sponsors/gigamon_logo.png"></a>

                  <h4><strong>Silver Level</strong></h4>
                  <a href="http://www.dual-comm.com/" target="_blank"><img class="sponsors" src="img/sponsors/dualcomm-white.png"></a>
                  <a href="http://www.apposite-tech.com/" target="_blank"><img class="sponsors" src="img/sponsors/apposite_logo.png"></a>
                  <a href="http://www.fiberblaze.com/" target="_blank"><img class="sponsors" src="img/sponsors/fiberblaze_logo.png"></a>
                  <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                  <a href="http://www.napatech.com/" target="_blank"><img class="sponsors" src="img/sponsors/napatech-white.png"></a>
                  <a href="http://www.aristanetworks.com/" target="_blank"><img class="sponsors" src="img/sponsors/arista-bright.png"></a>
                  <a href="http://www.wiresharktraining.com/" target="_blank"><img class="sponsors" src="img/sponsors/wiresharku-white.png"></a>
                  <a href="http://www.ixiacom.com/" target="_blank"><img class="sponsors" src="img/sponsors/ixia-white.png"></a>
                  <a href="http://www.endace.com/" target="_blank"><img class="sponsors" src="img/sponsors/endace_big.png"></a>
                  <a href="http://www.insidethestack.com/" target="_blank"><img class="sponsors" src="img/sponsors/inside-white.png"></a>
                  <a href="http://www.metageek.net/" target="_blank"><img width="130" src="img/sponsors/metageek_logo.png">

                  <h4><strong>Hosting Sponsor</strong></h4>
                  <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>
              </div>
            </a>
          </div>
        </div>
      </div>


  <!--
              <div class="widget tag-cloud">
                  <h4 class="widget-title">Tags</h4>
                  <ul>
                      <li><a href="#">Creative</a></li>
                      <li><a href="#">Design</a></li>
                      <li><a href="#">Art</a></li>
                      <li><a href="#">Autpor Name</a></li>
                      <li><a href="#">Coorperate</a></li>
                      <li><a href="#">Wordpress</a></li>
                      <li><a href="#">3D Animations</a></li>
                  </ul>
              </div>

              <div class="widget">
                  <h4 class="widget-title">Join The Newsletter</h4>
                  <form method="post" action="#">
                      <div class="form-group">
                          <input type="text" placeholder="Type and hit enter... " class="form-control" name="search" title="search">
                          <input type="submit" hidden="hidden"/>
                      </div>
                  </form>
              </div>

          </aside>
          <!-- Sidebar -->

          </div>
          </div>
          </section>
          <!-- /PAGE BLOG -->

      </div>
      <!-- /Content area -->
</a>
</div>
</aside>
</div>
</div>
</h2>
</div>
</div>
</article>
</section>
</div>
</div>
</section>
</div>
</section>
</div>

    <!-- FOOTER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>