<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf19-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'19 Retrospective</h1>
            <p>June 8th - 13th, 2019<br>UC Berkeley | Berkeley, California</p>
        </div>
    </div>
    <section id="main-content">
        <div class="content-area retrospective-page post-excerpt">

            <!-- PAGE BLOG -->
            <section class="page-section with-sidebar sidebar-right">
            <div class="container">
            <div class="row">

            <!-- Content -->
            <section id="content" class="content col-sm-7 col-md-8">

                <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                    <div class="post-header">
                        
                        <div class="post-meta">
      
                        </div>
                    </div>
                    <div class="post-body">
                        <div class="post-excerpt">
                            <h3 class="post-title2"><strong>Keynote Presentations</strong></h3>
                                <div class="retro-vid-wrapper">
                                     <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/lAc6DCxdF2o" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <h4><strong><a href="assets/presentations19/gerald-keynote.pptx">Latest Wireshark Developments & Road Map</a></strong><br/>
                                    Gerald Combs</h4>
                                </div>
                                <div class="retro-vid-wrapper">
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/bafqVHBT2sA" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    
                                    <h4><strong>Mental Models for Using Network Evidence</strong><br/>
                                    Chris Sanders</h4>
                                </div>
                                <div class="retro-vid-wrapper">
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/KfA8jW4HQcQ" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    
                                    <h4><strong>A Brief Annotated History of Capture</strong><br/>
                                    Loris Degioanni</h4>
                                </div>    

                            

                            
                            <h3 class="post-title2"><strong>Tuesday Classes</strong></h3>
                            <ul style="list-style:none;">
                            <li>01: <a href="assets/presentations19/01.pdf">War story: troubleshooting issues on encrypted links</a> by Christian Landström</li>
                            <li>02: <a href="assets/presentations19/02-26.pptx"> TLS encryption & decryption: What every IT engineer should know about TLS</a> by Ross Bagurdes</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=qbPOXoNXtGY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:22:25)</li>
                            </ul>
                            <li>03: <a href="assets/presentations19/03.zip"> Writing a Wireshark Dissector: 3 ways to eat bytes</a> by Graham Bloice</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=Fp_7g5as1VY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:07)</li>
                            </ul>
                            <li>04: Solving (SharkFest) packet capture challenges with only tshark by Sake Blok</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=gMg8ttIzry0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:11)</li>
                            </ul> 
              
                            <li>05: <a href="assets/presentations19/05.pptx">How long is a packet? And does it really matter?</a> by Stephen Donnelly</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=2GFCzAYSJx0&list=PLz_ZpPUgiXqMeN9ly6-lbr6Gdf3mknGIe&index=8&t=0s" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:54)</li>
                            </ul>
                            <li>06: Creating dissectors like a pro by generating dissectors by Richard Sharpe</li>
                            <ul>
                                <li class="presVideo"><a href="https://www.youtube.com/watch?v=XFFkC4PdCbI&list=PLz_ZpPUgiXqMeN9ly6-lbr6Gdf3mknGIe&index=9&t=0s" target="_blank">Presentation Video</a> (1:20:38)</li>
                            </ul>
                            <li>07: <a href="assets/presentations19/07-23.pdf"> To Send or not to Send? How TCP congestion control algorithms work</a> by Vladimir Gerasimov</li>
                            
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=EO8BaUwQHNI" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:30:56)</li>
                            </ul> 
                            <li>08: Taking a bite out of 100GB files by Betty DuBois</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=olhr1V5J-1c" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:11:33)</li>
                            </ul> 
                            <li>09: <a href="https://lekensteyn.nl/files/wireshark-tls-debugging-sharkfest19us.pdf">Debugging TLS issues with Wireshark</a> by Peter Wu</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=Ha4SLHceF6w" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:44)</li>
                            </ul>
                            <li>10: IPv6 troubleshooting with Wireshark  by Jeff Carrell</li>

                            <li>11: <a href="assets/presentations19/11.key">When TCP reassembly gets complicated</a> by Tom Peterson</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=D9GHb4QQI6Q" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (41:47)</li>
                            </ul>

                            <li>12: Jumbo frames & how to catch them  by Patrick Kinnison</li>
  
                            <li> 13: Kismet & wireless security 101 by Mike Kershaw</li>
                             <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=Kk4sImFR4z4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:20:16)</li>
                            </ul> 
                            <li>14: Tracing the untraceable with Wireshark: a view under the hood </a> by Roland Knall</li> 

                            </ul>

                            <h3 class="post-title2"><strong>Wednesday Classes</strong></h3>
                            <ul style="list-style:none;">
                            <li>15: <a href="assets/presentations19/15.pdf">Automating cloud infrastructure for analysis of large network captures</a> by Brad Palm & Brian Greunke</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=MdjlHy2aFJc" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:30:15)</li>
                            </ul>
                            <li>16:<a href="assets/presentations19/16.pdf"> My TCP ain't your TCP - ain't no TCP? </a> by Simon Lindermann</li> 
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=2coqZ07-Yhw" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:26:14)</li>
                            </ul> 
                            <li>17: <a href="assets/presentations19/17.pdf">TLS1.3, DNS over HTTPs, DNS over TLS, QUIC, IPv6 PDM & more!</a>by Nalini Elkins</li>
                           
                            <li>18: Practical Tracewrangling: Exploring capture file manipulation/extraction</a> by Jasper Bongertz</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=n1IKMoQE7yY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:24:32)</li>
                            </ul>
                            <li>19: <a href="assets/presentations19/19.zip" title="Presentation slides" target="_blank">TCP SACK overview & impact on performance </a> by John Pittle</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=6cNVEc2nQUk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:11:54)</li>
                            </ul> 
                            <li>20: IPv6 security assessment tools (aka IPv6 hacking tools) by Jeff Carrell</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=G9coloYylBc" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:35:25)</li>
                            </ul> 
                            <li>21: Troubleshooting slow networks</a> by Chris Greer </li>
                            <ul>
                                <li class="presVideo"><a href="https://www.youtube.com/watch?v=h9stVIfug5Y" target="_blank">Presentation Video</a>(1:10:57)</li>
                            </ul>
                            <li>22: <a href="https://malware-traffic-analysis.net/2019/sharkfest/" target="_blank">Analyzing Windows malware traffic with Wireshark (Part 1)</a> by Brad Duncan</li> 
                            <ul>
                                <li class="presVideo"><a href="https://www.youtube.com/watch?v=eQItiKZpuSc" target="_blank">Presentation Video</a>(1:10:57)</li>
                            </ul>
                            <li>23: <a href="assets/presentations19/07-23.pdf">To send or not to send? How TCP congestion control algorithms work</a> by Vladimir Gerasimov</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=EO8BaUwQHNI" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:30:56)</li>
                            </ul> 

                            <li>24: The packet doctors are in! Packet trace examinations with the experts</a> by Drs. Blok, Bongertz, and Landström</li>
                            
                            <li>25: <a href="https://malware-traffic-analysis.net/2019/sharkfest/" target="_blank">Analyzing Windows malware traffic with Wireshark (Part 2)</a> by Brad Duncan</li> 
                            <ul>
                                <li class="presVideo"><a href="https://youtu.be/ibSqqWZq9sk" target="_blank">Presentation Video</a> (1:04:26)</li>
                            </ul>
                            <li>26: <a href="assets/presentations19/02-26.pptx">TLS encryption & decryption: what every IT engineer should know about TLS</a> by Ross Bagurdes</li>

                            <li>27: Developer bytes lightning talks </a>  by Wireshark Core Developers</li>

                            <li>28: <a href="assets/presentations19/28-37.pdf">Wireshark visualization TIPS & tricks</a> by Megumi Takeshita</li>
                            <!--
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/OvV17GQlw-I" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:49)</li>
                            </ul> -->
                            <li>29: <a href="assets/presentations19/32.pdf"> Kismet & wireless security 101</a> by Mike Kershaw</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=Kk4sImFR4z4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:20:16)</li>
                            </ul>

                            </ul>
                            <h3 class="post-title2"><strong>Thursday Classes</strong></h3>
                            <ul style="list-style:none;">

                            <li>30: <a href="assets/presentations19/30.pptx">Using Wireshark to solve real problems for real people: step-by-step case studies in packet analysis</a> by Kary Rogers</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=ClqlK7OEFCc" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:20:01)</li>
                            </ul> 
                            
                            <li>31: <a href="assets/presentations19/31.zip"> TCP split brain: compare/contrast TCP effects on client & server with Wireshark (Part 1)</a> by John Pittle</li>

                            <ul>
                                <li class="presVideo"><a href="https://www.youtube.com/watch?v=7uC1CLq8BaI" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:24:11)</li>
                            </ul>

                            <li>32: <a href="assets/presentations19/32.pdf"> Kismet & wireless security 101</a> by Mike Kershaw</li>
                            <ul>
                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=Kk4sImFR4z4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:20:16)</li>
                            </ul>
                            <li>33: Capture file format deep dive by Jasper Bongertz</li>
                            <ul>
                                <li class="presVideo"><a href="https://youtu.be/IZ439VNvJqo" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:11:14)</li>
                            </ul>
                            <li>34: <a href="assets/presentations19/34.zip">TCP split brain: compare/contrast TCP effects on client & server with Wireshark (Part 2)</a> by John Pittle</li> 

                            <ul>
                                <li class="presVideo"><a href="https://www.youtube.com/watch?v=YpiORadeiy0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:27:25)</li>
                            </ul>
                            
                            <li>35: Solving the impossible by Mike Canney</li>
                             <ul>
                            <li class="presVideo"><a href="https://youtu.be/YLg91sAcQdw" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:02:20)</li>
                            </ul> 
                            <li>36: A deep dive into LDAP: Everything you need to know to debug and troubleshoot LDAP packets by Betty DuBois</li>

                            <li>37: <a href="assets/presentations19/28-37.pdf">Wireshark visualization TIPS & tricks</a> by Megumi Takeshita</li>
                            <li>38: Enrich your network visibility & analysis with Wireshark & ELK</a> by Tajul Ariffin</li>
                            <ul>
                            <li class="presVideo"><a href="https://youtu.be/IXdR3HWDB7A" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:05:55)</li>
                            </ul>
                            <li>39: A walkthrough of the SharkFest Group & Individual Packet Challenges by Sake Blok, Christian Landström, and Jasper Bongertz</li>
                        </div>
                    </div>
                    
                </article>

            </section>
            <!-- Content -->

            <hr class="page-divider transparent visible-xs"/>

            <!-- Sidebar -->
            <aside id="sidebar" class="sidebar col-sm-5 col-md-4">
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <h4 class="widget-title">A Word of Thanks</h4>
                        <p>SharkFest'19 US proved to be a blazing success thanks to the generous, giving community in attendance. Particular thanks to Gerald Combs and his merry band of core developers for inspiring the many first-time participants by opening with a keynote that illuminated the 20-year history of the project, to instructors who selflessly donated time and wisdom to educate and mentor participants, to sponsors who so generously provided the resources to make the conference possible, to the UC Berkeley Events team for their expert guidance, to tireless caterers who served up varied and delicious daily fare, and to a staff and volunteer crew who once again went overboard in making the conference as smooth and pleasant an experience as possible for attendees!</p>
                    </div>
                </div>
                <div class="widget flickr-feed">
                    <div class="post-media">
                        <a style="font-size: 25px;" alt="Click Here to View Pictures from Sharkfest'19!" target="_blank" href='https://photos.app.goo.gl/jxpxDZ5miMXH5WFW6'><img src="img/sharkfest19gallery/1.jpg" ></a>
                    </div>
                </div>
                    
                <div class="background-blue front-column1">
                    <div class="grid-cell">
                        <div class="sponsor-list">
                            <div class="sponsor-imgs">
                                    <h4>Sponsor Videos</h4>
                                    <div class="retro-vid-wrapper">
                                        <div class="responsive-iframe">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqOTlBn_W0x3tFstX6A0BY_6" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <h4>Host Sponsor</h4>
                                    <a href="http://www.riverbed.com/" target="_blank"><img class="sponsors" src="img/sponsors/riverbed.png"></a>

                                    <h4>Angel Shark Sponsors</h4>
                                    <a href="https://accedian.com/" target="_blank"><img class="sponsors" src="img/sponsors/accedian-new.png"></a>
                                    <a href="http://www.comworth.com.sg/" target="_blank"><img class="sponsors" src="img/sponsors/comworth.png"></a>
                                     
                                    <a href="https://www.dualcomm.com/" target="_blank"><img class="sponsors" src="img/sponsors/dualcomm-white.png"></a>
                                    <a href="https://www.endace.com/" target="_blank"><img class="sponsors" src="img/sponsors/endace_big.png"></a>
                                    <a href="https://www.gigamon.com/" target="_blank"><img class="sponsors" src="img/sponsors/gigamon-new-big.jpg"></a>
                                    <a href="https://www.influxdata.com/" target="_blank"><img class="sponsors" src="img/sponsors/influx-white.png"></a>

                                    <h4>Tiger Shark Group Packet Competition Sponsor</h4>
                                    
                                    <a href="https://www.endace.com/" target="_blank"><img class="sponsors" src="img/sponsors/endace_big.png"></a>
                                    
                                    <h4>Silky Shark Welcome Gift Sponsors</h4>

                                    <a href="https://counterflow.ai/" target="_blank"><img class="sponsors" src="img/sponsors/counterflow.png"></a>
                                    <a href="https://www.profitap.com/" target="_blank"><img class="sponsors" src="img/sponsors/profitap-white.png"></a>

                                    <h4>Copper Shark Welcome Dinner Sponsor</h4>
                                    <a href="http://go.bigswitch.com/meet-big-switch-at-sharkfest-us-19.html" target="_blank"><img class="sponsors" src="img/sponsors/bigswitch-white.png"></a>
                                    <a href="https://sysdig.com/" target="_blank"><img class="sponsors" src="img/sponsors/sysdig-white.png"></a>
                                
                                    <h4>Honorary Sponsors</h4>
                                    <a href="http://www.lovemytool.com/" target="_blank"><img class="sponsors" src="img/sponsors/lovemtytool_Logo.png"></a>
                            </div>
                                                        
                        </div>
                    </div>
                </div>
            </aside>
            </div>
            </section>
            <!-- /PAGE BLOG -->

        </div>
        <!-- /Content area -->
    </section>
</div>
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>



